<?php
/* @var $this OffersController */

$this->breadcrumbs=array(
    'Special Offers',
);
?>
<div id="special-offers" class="l-container container">
    <div class="main-special-offers">
        <div class="wrapper-special-offers">
            <div class="header-special-offers-page">
                <h3><?php echo Yii::t('_yii','Special Offers');?></h3>
                <div class="info-search-filter">
                    <form name="search-special-offers" id="search-special-offers" action="" method="get">
                        <div class="filter-sort">
                            <label><?php echo Yii::t('_yii','Sort by:');?></label>
                            <select name="sort" id="sortbook">
                                <option value="title-asc"><?php echo Yii::t('_yii','Title(A-Z)');?></option>
                                <option value="title-desc"><?php echo Yii::t('_yii','Title(Z-A)');?></option>
                            </select>
                        </div>
                        <div class="filter-search">
                            <div>
                                <label><?php echo Yii::t('_yii','Search:');?></label>
                                <input type="text" name="key" class="key-search-input" value="<?php echo isset($_GET['key'])?$_GET['key']:'';?>">
                                <button type="submit" class="submit-search" id="submit-search"><?php echo Yii::t('_yii','Search');?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="content-special-offers-page">
                <ul>
                    <?php $this->renderPartial('_item');?>
                </ul>
            </div>
            <div class="special-offers-footer">
                <a href="#" class="book-load-more"><?php echo Yii::t('_yii','View more');?></a>
            </div>
        </div>
    </div>
</div>