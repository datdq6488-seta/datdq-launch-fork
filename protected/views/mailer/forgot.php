    <div style=" display: block;">
		<p>A request to reset the password for your account has been made at Launch</p>
		<p>You will be redirected to your profile to change your password. If you are not redirected, please follow this link:</p>
		<p><?php echo $activation_url;?></p>
		<p>This is a one-time login, so it can be used only once.</p>	
    </div>