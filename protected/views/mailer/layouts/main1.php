<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title><?php echo Yii::app() ->name;?></title>
</head>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
<center>
    <h1 style="font-size: 30px; color:#00FFF5" class="h1"><?php echo Yii::app() ->name;?></h1>
    <div style="margin-top: 50px;">
        <?php echo $content; ?>
    </div>
</center>
</body>
</html>