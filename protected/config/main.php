<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Launch',
    'theme' => 'launch',
    'language' => 'en',
    'sourceLanguage' => 'en',
    // preloading 'log' component
    'preload' => array('log'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.widgets.*',
        'application.components.helpers.*',
        'application.components.helpers.utils.*',
        'ext.phpthumb.*',
        'application.commands.*',
    ),
    'modules' => array(
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'launch',
        ),
        'service',
        'user' => array(
            # encrypting method (php hash function)
            'hash' => 'md5',
            # send activation email
            'sendActivationMail' => true,
            # allow access for non-activated users
            'loginNotActiv' => false,
            # activate user on registration (only sendActivationMail = false)
            'activeAfterRegister' => false,
            # automatically login from registration
            'autoLogin' => true,
            # registration path
            'registrationUrl' => array('/user/registration'),
            # recovery password path
            'recoveryUrl' => array('/user/recovery'),
            # login form path
            'loginUrl' => array('/user/login'),
            # page after login
            'returnUrl' => array('/user/profile'),
            # page after logout
            'returnLogoutUrl' => array('/user/login'),
        ),
    ),
    'behaviors' => array(
        'onBeginRequest' => array(
            'class' => 'application.components.AppStartup'
        ),
    ),
    // application components
    'components' => array(
        'session' => array(
            'class' => 'system.web.CDbHttpSession',
            'connectionID' => 'db',
        ),
        //email
        'mailer' => array(
            'class' => 'application.extensions.mailer.EMailer',
            'pathViews' => 'application.views.mailer',
            'pathLayouts' => 'application.views.mailer.layouts'
        ),
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(
                'gii' => 'gii',
                'gii/<controller:\w+>' => 'gii/<controller>',
                'gii/<controller:\w+>/<action:\w+>' => 'gii/<controller>/<action>',
                '<language:(en)>/' => 'site/index',
                '<language:(en)>/<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<language:(en)>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<language:(en)>/<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                '<language:(en)>/login' => 'user/login',
                '<language:(en)>/logout' => 'site/logout',
                '/login' => 'user/login',
                '/logout' => 'site/logout',
                '<language:(en|vi|de)>/<controller:\w+>/<action:\w+>/*' => '<controller>/<action>',
                '<language:(en|vi|de)>/<controller:\w+>' => '<controller>/index',
				'<controller:\w+>/detail/<id:\d+>'=>'<controller>/detail/<id:\d+>',
            ),
        ),
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=launch',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
            'tablePrefix' => 'tbl_',
        ),
        'user' => array(
            // enable cookie-based authentication
            'class' => 'WebUser',
            'allowAutoLogin' => true,
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            ),
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        'domain' => 'launch.com',
        // this is used in contact page
        'adminEmail' => 'webmaster@gmail.com',
        'adminName' => 'Launch Admin',
        'languages' => array('vi' => 'Vietnamese', 'en' => 'English', 'de' => 'Deutsch'),
        'uploadPath' => dirname(__FILE__) . '/../../images/files',
        'uploadUrl' => '/images/files',
        'oauth' => array(
            "base_url" => "http://" . $_SERVER['SERVER_NAME'] . "/oauth/index",
            "providers" => array(
                "Facebook" => array(
                    "enabled" => true,
                    "keys" => array("id" => "706255712765745", "secret" => "2e8bfff249cc223cc22392885c332501"),
                    "scope" => "email",
                    "display" => "popup"
                ),
                "Twitter" => array(
                    "enabled" => true,
                    "keys" => array("key" => "IngXJ4cF2QiXWQtMUzJLEsdT9", "secret" => "SqUbkhuMjpb00i2IYu0mkFI8m7E13kf48uD3ForjZaZpRMn277"),
                ),
                "Google" => array(
                    "enabled" => true,
                    "keys" => array("id" => "961151416683-tavbgdg4cs4pqbdhj61a3dt1sjrbtj11.apps.googleusercontent.com", "secret" => "lks2C_5ynPLkAVS9VCiGQDsr"),
                ),
            ),
            "debug_mode" => false,
            // to enable logging, set 'debug_mode' to true, then provide here a path of a writable file 
            "debug_file" => "",
        ),
        'recaptcha' => array(
            'publicKey' => '6Le66PQSAAAAANp6_SqnzCLRd7knmKjxob1kthFk',
            'privateKey' => '6Le66PQSAAAAAAa4hwS7AzzeldsqrKsIElY3Sj7g',
        ),
        'expried_token' => 1, //Token reset password, unit : Day
        'smtp' => array(
            "Host" => 'smtp.gmail.com',
            "Username" => 'dapworld.test@gmail.com',
            "Password" => 'dapworld1234567dapworld',
            "From" => 'vnlabcode@gmail.com',
            "SMTPDebug" => 0,
            "SMTPAuth" => true,
            "SMTPSecure" => 'ssl',
            "Port" => 465,
            "FromName" => 'launch.com',
        ),
    ),
);
