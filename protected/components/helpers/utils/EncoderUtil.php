<?php

/**
 * Created by PhpStorm.
 * User: Pham Quoc Hung
 * Date: 6/5/14
 * Time: 11:10 AM
 */
class EncoderUtil {

    protected static $charWin1252ToUtf8 = array(
        128 => "\xe2\x82\xac",

        130 => "\xe2\x80\x9a",
        131 => "\xc6\x92",
        132 => "\xe2\x80\x9e",
        133 => "\xe2\x80\xa6",
        134 => "\xe2\x80\xa0",
        135 => "\xe2\x80\xa1",
        136 => "\xcb\x86",
        137 => "\xe2\x80\xb0",
        138 => "\xc5\xa0",
        139 => "\xe2\x80\xb9",
        140 => "\xc5\x92",

        142 => "\xc5\xbd",


        145 => "\xe2\x80\x98",
        146 => "\xe2\x80\x99",
        147 => "\xe2\x80\x9c",
        148 => "\xe2\x80\x9d",
        149 => "\xe2\x80\xa2",
        150 => "\xe2\x80\x93",
        151 => "\xe2\x80\x94",
        152 => "\xcb\x9c",
        153 => "\xe2\x84\xa2",
        154 => "\xc5\xa1",
        155 => "\xe2\x80\xba",
        156 => "\xc5\x93",

        158 => "\xc5\xbe",
        159 => "\xc5\xb8"
    );

    protected static $charUtf8ToWin1252 = array(
        "\xe2\x82\xac" => "\x80",

        "\xe2\x80\x9a" => "\x82",
        "\xc6\x92"     => "\x83",
        "\xe2\x80\x9e" => "\x84",
        "\xe2\x80\xa6" => "\x85",
        "\xe2\x80\xa0" => "\x86",
        "\xe2\x80\xa1" => "\x87",
        "\xcb\x86"     => "\x88",
        "\xe2\x80\xb0" => "\x89",
        "\xc5\xa0"     => "\x8a",
        "\xe2\x80\xb9" => "\x8b",
        "\xc5\x92"     => "\x8c",

        "\xc5\xbd"     => "\x8e",

        "\xe2\x80\x98" => "\x91",
        "\xe2\x80\x99" => "\x92",
        "\xe2\x80\x9c" => "\x93",
        "\xe2\x80\x9d" => "\x94",
        "\xe2\x80\xa2" => "\x95",
        "\xe2\x80\x93" => "\x96",
        "\xe2\x80\x94" => "\x97",
        "\xcb\x9c"     => "\x98",
        "\xe2\x84\xa2" => "\x99",
        "\xc5\xa1"     => "\x9a",
        "\xe2\x80\xba" => "\x9b",
        "\xc5\x93"     => "\x9c",

        "\xc5\xbe"     => "\x9e",
        "\xc5\xb8"     => "\x9f"
    );
    protected static $charHtmlEntity = array(
        '"' => '&quot;',
        '&' => '&amp;',
        '<' => '&lt;',
        '>' => '&gt;',
        ' ' => '&nbsp;',
        '¡' => '&iexcl;',
        '¢' => '&cent;',
        '£' => '&pound;',
        '¤' => '&curren;',
        '¥' => '&yen;',
        '¦' => '&brvbar;',
        '§' => '&sect;',
        '¨' => '&uml;',
        '©' => '&copy;',
        'ª' => '&ordf;',
        '«' => '&laquo;',
        '¬' => '&not;',
        '­' => '&shy;',
        '®' => '&reg;',
        '¯' => '&macr;',
        '°' => '&deg;',
        '±' => '&plusmn;',
        '²' => '&sup2;',
        '³' => '&sup3;',
        '´' => '&acute;',
        'µ' => '&micro;',
        '¶' => '&para;',
        '·' => '&middot;',
        '¸' => '&cedil;',
        '¹' => '&sup1;',
        'º' => '&ordm;',
        '»' => '&raquo;',
        '¼' => '&frac14;',
        '½' => '&frac12;',
        '¾' => '&frac34;',
        '¿' => '&iquest;',
        'À' => '&Agrave;',
        '�?' => '&Aacute;',
        'Â' => '&Acirc;',
        'Ã' => '&Atilde;',
        'Ä' => '&Auml;',
        'Å' => '&Aring;',
        'Æ' => '&AElig;',
        'Ç' => '&Ccedil;',
        'È' => '&Egrave;',
        'É' => '&Eacute;',
        'Ê' => '&Ecirc;',
        'Ë' => '&Euml;',
        'Ì' => '&Igrave;',
        '�?' => '&Iacute;',
        'Î' => '&Icirc;',
        '�?' => '&Iuml;',
        '�?' => '&ETH;',
        'Ñ' => '&Ntilde;',
        'Ò' => '&Ograve;',
        'Ó' => '&Oacute;',
        'Ô' => '&Ocirc;',
        'Õ' => '&Otilde;',
        'Ö' => '&Ouml;',
        '×' => '&times;',
        'Ø' => '&Oslash;',
        'Ù' => '&Ugrave;',
        'Ú' => '&Uacute;',
        'Û' => '&Ucirc;',
        'Ü' => '&Uuml;',
        '�?' => '&Yacute;',
        'Þ' => '&THORN;',
        'ß' => '&szlig;',
        'à' => '&agrave;',
        'á' => '&aacute;',
        'â' => '&acirc;',
        'ã' => '&atilde;',
        'ä' => '&auml;',
        'å' => '&aring;',
        'æ' => '&aelig;',
        'ç' => '&ccedil;',
        'è' => '&egrave;',
        'é' => '&eacute;',
        'ê' => '&ecirc;',
        'ë' => '&euml;',
        'ì' => '&igrave;',
        'í' => '&iacute;',
        'î' => '&icirc;',
        'ï' => '&iuml;',
        'ð' => '&eth;',
        'ñ' => '&ntilde;',
        'ò' => '&ograve;',
        'ó' => '&oacute;',
        'ô' => '&ocirc;',
        'õ' => '&otilde;',
        'ö' => '&ouml;',
        '÷' => '&divide;',
        'ø' => '&oslash;',
        'ù' => '&ugrave;',
        'ú' => '&uacute;',
        'û' => '&ucirc;',
        'ü' => '&uuml;',
        'ý' => '&yacute;',
        'þ' => '&thorn;',
        'ÿ' => '&yuml;',
        'Œ' => '&OElig;',
        'œ' => '&oelig;',
        'Š' => '&Scaron;',
        'š' => '&scaron;',
        'Ÿ' => '&Yuml;',
        'ƒ' => '&fnof;',
        'ˆ' => '&circ;',
        '˜' => '&tilde;',
        'Α' => '&Alpha;',
        'Β' => '&Beta;',
        'Γ' => '&Gamma;',
        'Δ' => '&Delta;',
        'Ε' => '&Epsilon;',
        'Ζ' => '&Zeta;',
        'Η' => '&Eta;',
        'Θ' => '&Theta;',
        'Ι' => '&Iota;',
        'Κ' => '&Kappa;',
        'Λ' => '&Lambda;',
        'Μ' => '&Mu;',
        '�?' => '&Nu;',
        'Ξ' => '&Xi;',
        'Ο' => '&Omicron;',
        'Π' => '&Pi;',
        'Ρ' => '&Rho;',
        'Σ' => '&Sigma;',
        'Τ' => '&Tau;',
        'Υ' => '&Upsilon;',
        'Φ' => '&Phi;',
        'Χ' => '&Chi;',
        'Ψ' => '&Psi;',
        'Ω' => '&Omega;',
        'α' => '&alpha;',
        'β' => '&beta;',
        'γ' => '&gamma;',
        'δ' => '&delta;',
        'ε' => '&epsilon;',
        'ζ' => '&zeta;',
        'η' => '&eta;',
        'θ' => '&theta;',
        'ι' => '&iota;',
        'κ' => '&kappa;',
        'λ' => '&lambda;',
        'μ' => '&mu;',
        'ν' => '&nu;',
        'ξ' => '&xi;',
        'ο' => '&omicron;',
        'π' => '&pi;',
        '�?' => '&rho;',
        'ς' => '&sigmaf;',
        'σ' => '&sigma;',
        'τ' => '&tau;',
        'υ' => '&upsilon;',
        'φ' => '&phi;',
        'χ' => '&chi;',
        'ψ' => '&psi;',
        'ω' => '&omega;',
        'ϑ' => '&thetasym;',
        'ϒ' => '&upsih;',
        'ϖ' => '&piv;',
        ' ' => '&ensp;',
        ' ' => '&emsp;',
        ' ' => '&thinsp;',
        '‌' => '&zwnj;',
        '�?' => '&zwj;',
        '‎' => '&lrm;',
        '�?' => '&rlm;',
        '–' => '&ndash;',
        '—' => '&mdash;',
        '‘' => '&lsquo;',
        '’' => '&rsquo;',
        '‚' => '&sbquo;',
        '“' => '&ldquo;',
        '�?' => '&rdquo;',
        '„' => '&bdquo;',
        '†' => '&dagger;',
        '‡' => '&Dagger;',
        '•' => '&bull;',
        '…' => '&hellip;',
        '‰' => '&permil;',
        '′' => '&prime;',
        '″' => '&Prime;',
        '‹' => '&lsaquo;',
        '›' => '&rsaquo;',
        '‾' => '&oline;',
        '�?�' => '&frasl;',
        '€' => '&euro;',
        'ℑ' => '&image;',
        '℘' => '&weierp;',
        'ℜ' => '&real;',
        '™' => '&trade;',
        'ℵ' => '&alefsym;',
        '�?' => '&larr;',
        '↑' => '&uarr;',
        '→' => '&rarr;',
        '↓' => '&darr;',
        '↔' => '&harr;',
        '↵' => '&crarr;',
        '�?' => '&lArr;',
        '⇑' => '&uArr;',
        '⇒' => '&rArr;',
        '⇓' => '&dArr;',
        '⇔' => '&hArr;',
        '∀' => '&forall;',
        '∂' => '&part;',
        '∃' => '&exist;',
        '∅' => '&empty;',
        '∇' => '&nabla;',
        '∈' => '&isin;',
        '∉' => '&notin;',
        '∋' => '&ni;',
        '�?' => '&prod;',
        '∑' => '&sum;',
        '−' => '&minus;',
        '∗' => '&lowast;',
        '√' => '&radic;',
        '�?' => '&prop;',
        '∞' => '&infin;',
        '∠' => '&ang;',
        '∧' => '&and;',
        '∨' => '&or;',
        '∩' => '&cap;',
        '∪' => '&cup;',
        '∫' => '&int;',
        '∴' => '&there4;',
        '∼' => '&sim;',
        '≅' => '&cong;',
        '≈' => '&asymp;',
        '≠' => '&ne;',
        '≡' => '&equiv;',
        '≤' => '&le;',
        '≥' => '&ge;',
        '⊂' => '&sub;',
        '⊃' => '&sup;',
        '⊄' => '&nsub;',
        '⊆' => '&sube;',
        '⊇' => '&supe;',
        '⊕' => '&oplus;',
        '⊗' => '&otimes;',
        '⊥' => '&perp;',
        '⋅' => '&sdot;',
        '⌈' => '&lceil;',
        '⌉' => '&rceil;',
        '⌊' => '&lfloor;',
        '⌋' => '&rfloor;',
        '〈' => '&lang;',
        '〉' => '&rang;',
        '◊' => '&loz;',
        '♠' => '&spades;',
        '♣' => '&clubs;',
        '♥' => '&hearts;',
        '♦' => '&diams;',
    ) ;

    protected static $charHtmlEntityScript = array(
        '<script' => '&lt;script',
        'script>' => 'script&gt;'
    ) ;

    protected static $vowels_1 = array(	'a'=>array('a', 'á', 'à', 'ã', 'ạ', 'ả', 'ă', 'ắ', 'ằ', 'ẵ', 'ặ', 'ẳ', 'â', 'ấ', 'ầ', 'ẫ', 'ậ', 'ẩ'),
        'á'=>array('a', 'á', 'à', 'ã', 'ạ', 'ả', 'ă', 'ắ', 'ằ', 'ẵ', 'ặ', 'ẳ', 'â', 'ấ', 'ầ', 'ẫ', 'ậ', 'ẩ'),
        'à'=>array('a', 'á', 'à', 'ã', 'ạ', 'ả', 'ă', 'ắ', 'ằ', 'ẵ', 'ặ', 'ẳ', 'â', 'ấ', 'ầ', 'ẫ', 'ậ', 'ẩ'),
        'ã'=>array('a', 'á', 'à', 'ã', 'ạ', 'ả', 'ă', 'ắ', 'ằ', 'ẵ', 'ặ', 'ẳ', 'â', 'ấ', 'ầ', 'ẫ', 'ậ', 'ẩ'),
        'ạ'=>array('a', 'á', 'à', 'ã', 'ạ', 'ả', 'ă', 'ắ', 'ằ', 'ẵ', 'ặ', 'ẳ', 'â', 'ấ', 'ầ', 'ẫ', 'ậ', 'ẩ'),
        'ả'=>array('a', 'á', 'à', 'ã', 'ạ', 'ả', 'ă', 'ắ', 'ằ', 'ẵ', 'ặ', 'ẳ', 'â', 'ấ', 'ầ', 'ẫ', 'ậ', 'ẩ'),
        'ă'=>array('a', 'á', 'à', 'ã', 'ạ', 'ả', 'ă', 'ắ', 'ằ', 'ẵ', 'ặ', 'ẳ', 'â', 'ấ', 'ầ', 'ẫ', 'ậ', 'ẩ'),
        'ắ'=>array('a', 'á', 'à', 'ã', 'ạ', 'ả', 'ă', 'ắ', 'ằ', 'ẵ', 'ặ', 'ẳ', 'â', 'ấ', 'ầ', 'ẫ', 'ậ', 'ẩ'),
        'ằ'=>array('a', 'á', 'à', 'ã', 'ạ', 'ả', 'ă', 'ắ', 'ằ', 'ẵ', 'ặ', 'ẳ', 'â', 'ấ', 'ầ', 'ẫ', 'ậ', 'ẩ'),
        'ẵ'=>array('a', 'á', 'à', 'ã', 'ạ', 'ả', 'ă', 'ắ', 'ằ', 'ẵ', 'ặ', 'ẳ', 'â', 'ấ', 'ầ', 'ẫ', 'ậ', 'ẩ'),
        'ặ'=>array('a', 'á', 'à', 'ã', 'ạ', 'ả', 'ă', 'ắ', 'ằ', 'ẵ', 'ặ', 'ẳ', 'â', 'ấ', 'ầ', 'ẫ', 'ậ', 'ẩ'),
        'ẳ'=>array('a', 'á', 'à', 'ã', 'ạ', 'ả', 'ă', 'ắ', 'ằ', 'ẵ', 'ặ', 'ẳ', 'â', 'ấ', 'ầ', 'ẫ', 'ậ', 'ẩ'),
        'â'=>array('a', 'á', 'à', 'ã', 'ạ', 'ả', 'ă', 'ắ', 'ằ', 'ẵ', 'ặ', 'ẳ', 'â', 'ấ', 'ầ', 'ẫ', 'ậ', 'ẩ'),
        'ấ'=>array('a', 'á', 'à', 'ã', 'ạ', 'ả', 'ă', 'ắ', 'ằ', 'ẵ', 'ặ', 'ẳ', 'â', 'ấ', 'ầ', 'ẫ', 'ậ', 'ẩ'),
        'ầ'=>array('a', 'á', 'à', 'ã', 'ạ', 'ả', 'ă', 'ắ', 'ằ', 'ẵ', 'ặ', 'ẳ', 'â', 'ấ', 'ầ', 'ẫ', 'ậ', 'ẩ'),
        'ẫ'=>array('a', 'á', 'à', 'ã', 'ạ', 'ả', 'ă', 'ắ', 'ằ', 'ẵ', 'ặ', 'ẳ', 'â', 'ấ', 'ầ', 'ẫ', 'ậ', 'ẩ'),
        'ậ'=>array('a', 'á', 'à', 'ã', 'ạ', 'ả', 'ă', 'ắ', 'ằ', 'ẵ', 'ặ', 'ẳ', 'â', 'ấ', 'ầ', 'ẫ', 'ậ', 'ẩ'),
        'ẩ'=>array('a', 'á', 'à', 'ã', 'ạ', 'ả', 'ă', 'ắ', 'ằ', 'ẵ', 'ặ', 'ẳ', 'â', 'ấ', 'ầ', 'ẫ', 'ậ', 'ẩ'),
        'i'=>array('i', 'í', 'ì', 'ĩ', 'ị', 'ỉ'),
        'í'=>array('i', 'í', 'ì', 'ĩ', 'ị', 'ỉ'),
        'ì'=>array('i', 'í', 'ì', 'ĩ', 'ị', 'ỉ'),
        'ĩ'=>array('i', 'í', 'ì', 'ĩ', 'ị', 'ỉ'),
        'ị'=>array('i', 'í', 'ì', 'ĩ', 'ị', 'ỉ'),
        'ỉ'=>array('i', 'í', 'ì', 'ĩ', 'ị', 'ỉ'),
        'y'=>array('y', 'ý', 'ỳ', 'ỹ', 'ỵ', 'ỷ'),
        'ý'=>array('y', 'ý', 'ỳ', 'ỹ', 'ỵ', 'ỷ'),
        'ỳ'=>array('y', 'ý', 'ỳ', 'ỹ', 'ỵ', 'ỷ'),
        'ỹ'=>array('y', 'ý', 'ỳ', 'ỹ', 'ỵ', 'ỷ'),
        'ỵ'=>array('y', 'ý', 'ỳ', 'ỹ', 'ỵ', 'ỷ'),
        'ỷ'=>array('y', 'ý', 'ỳ', 'ỹ', 'ỵ', 'ỷ'),
        'u'=>array('u', 'ú', 'ù', 'ũ', 'ụ', 'ủ', 'ư', 'ứ', 'ừ', 'ữ', 'ự', 'ử'),
        'ú'=>array('u', 'ú', 'ù', 'ũ', 'ụ', 'ủ', 'ư', 'ứ', 'ừ', 'ữ', 'ự', 'ử'),
        'ù'=>array('u', 'ú', 'ù', 'ũ', 'ụ', 'ủ', 'ư', 'ứ', 'ừ', 'ữ', 'ự', 'ử'),
        'ũ'=>array('u', 'ú', 'ù', 'ũ', 'ụ', 'ủ', 'ư', 'ứ', 'ừ', 'ữ', 'ự', 'ử'),
        'ụ'=>array('u', 'ú', 'ù', 'ũ', 'ụ', 'ủ', 'ư', 'ứ', 'ừ', 'ữ', 'ự', 'ử'),
        'ủ'=>array('u', 'ú', 'ù', 'ũ', 'ụ', 'ủ', 'ư', 'ứ', 'ừ', 'ữ', 'ự', 'ử'),
        'ư'=>array('u', 'ú', 'ù', 'ũ', 'ụ', 'ủ', 'ư', 'ứ', 'ừ', 'ữ', 'ự', 'ử'),
        'ứ'=>array('u', 'ú', 'ù', 'ũ', 'ụ', 'ủ', 'ư', 'ứ', 'ừ', 'ữ', 'ự', 'ử'),
        'ừ'=>array('u', 'ú', 'ù', 'ũ', 'ụ', 'ủ', 'ư', 'ứ', 'ừ', 'ữ', 'ự', 'ử'),
        'ữ'=>array('u', 'ú', 'ù', 'ũ', 'ụ', 'ủ', 'ư', 'ứ', 'ừ', 'ữ', 'ự', 'ử'),
        'ự'=>array('u', 'ú', 'ù', 'ũ', 'ụ', 'ủ', 'ư', 'ứ', 'ừ', 'ữ', 'ự', 'ử'),
        'ử'=>array('u', 'ú', 'ù', 'ũ', 'ụ', 'ủ', 'ư', 'ứ', 'ừ', 'ữ', 'ự', 'ử'),
        'e'=>array('e', 'é', 'è', 'ẽ', 'ẹ', 'ẻ', 'ê', 'ế', '�?', 'ễ', 'ệ', 'ể'),
        'é'=>array('e', 'é', 'è', 'ẽ', 'ẹ', 'ẻ', 'ê', 'ế', '�?', 'ễ', 'ệ', 'ể'),
        'è'=>array('e', 'é', 'è', 'ẽ', 'ẹ', 'ẻ', 'ê', 'ế', '�?', 'ễ', 'ệ', 'ể'),
        'ẽ'=>array('e', 'é', 'è', 'ẽ', 'ẹ', 'ẻ', 'ê', 'ế', '�?', 'ễ', 'ệ', 'ể'),
        'ẹ'=>array('e', 'é', 'è', 'ẽ', 'ẹ', 'ẻ', 'ê', 'ế', '�?', 'ễ', 'ệ', 'ể'),
        'ẻ'=>array('e', 'é', 'è', 'ẽ', 'ẹ', 'ẻ', 'ê', 'ế', '�?', 'ễ', 'ệ', 'ể'),
        'ê'=>array('e', 'é', 'è', 'ẽ', 'ẹ', 'ẻ', 'ê', 'ế', '�?', 'ễ', 'ệ', 'ể'),
        'ế'=>array('e', 'é', 'è', 'ẽ', 'ẹ', 'ẻ', 'ê', 'ế', '�?', 'ễ', 'ệ', 'ể'),
        '�?'=>array('e', 'é', 'è', 'ẽ', 'ẹ', 'ẻ', 'ê', 'ế', '�?', 'ễ', 'ệ', 'ể'),
        'ễ'=>array('e', 'é', 'è', 'ẽ', 'ẹ', 'ẻ', 'ê', 'ế', '�?', 'ễ', 'ệ', 'ể'),
        'ệ'=>array('e', 'é', 'è', 'ẽ', 'ẹ', 'ẻ', 'ê', 'ế', '�?', 'ễ', 'ệ', 'ể'),
        'ể'=>array('e', 'é', 'è', 'ẽ', 'ẹ', 'ẻ', 'ê', 'ế', '�?', 'ễ', 'ệ', 'ể'),
        'o'=>array('o', 'ó', 'ò', 'õ', '�?', '�?', 'ô', 'ố', 'ồ', 'ỗ', 'ộ', 'ổ', 'ơ', 'ớ', '�?', 'ỡ', 'ợ', 'ở'),
        'ó'=>array('o', 'ó', 'ò', 'õ', '�?', '�?', 'ô', 'ố', 'ồ', 'ỗ', 'ộ', 'ổ', 'ơ', 'ớ', '�?', 'ỡ', 'ợ', 'ở'),
        'ò'=>array('o', 'ó', 'ò', 'õ', '�?', '�?', 'ô', 'ố', 'ồ', 'ỗ', 'ộ', 'ổ', 'ơ', 'ớ', '�?', 'ỡ', 'ợ', 'ở'),
        'õ'=>array('o', 'ó', 'ò', 'õ', '�?', '�?', 'ô', 'ố', 'ồ', 'ỗ', 'ộ', 'ổ', 'ơ', 'ớ', '�?', 'ỡ', 'ợ', 'ở'),
        '�?'=>array('o', 'ó', 'ò', 'õ', '�?', '�?', 'ô', 'ố', 'ồ', 'ỗ', 'ộ', 'ổ', 'ơ', 'ớ', '�?', 'ỡ', 'ợ', 'ở'),
        '�?'=>array('o', 'ó', 'ò', 'õ', '�?', '�?', 'ô', 'ố', 'ồ', 'ỗ', 'ộ', 'ổ', 'ơ', 'ớ', '�?', 'ỡ', 'ợ', 'ở'),
        'ô'=>array('o', 'ó', 'ò', 'õ', '�?', '�?', 'ô', 'ố', 'ồ', 'ỗ', 'ộ', 'ổ', 'ơ', 'ớ', '�?', 'ỡ', 'ợ', 'ở'),
        'ố'=>array('o', 'ó', 'ò', 'õ', '�?', '�?', 'ô', 'ố', 'ồ', 'ỗ', 'ộ', 'ổ', 'ơ', 'ớ', '�?', 'ỡ', 'ợ', 'ở'),
        'ồ'=>array('o', 'ó', 'ò', 'õ', '�?', '�?', 'ô', 'ố', 'ồ', 'ỗ', 'ộ', 'ổ', 'ơ', 'ớ', '�?', 'ỡ', 'ợ', 'ở'),
        'ỗ'=>array('o', 'ó', 'ò', 'õ', '�?', '�?', 'ô', 'ố', 'ồ', 'ỗ', 'ộ', 'ổ', 'ơ', 'ớ', '�?', 'ỡ', 'ợ', 'ở'),
        'ộ'=>array('o', 'ó', 'ò', 'õ', '�?', '�?', 'ô', 'ố', 'ồ', 'ỗ', 'ộ', 'ổ', 'ơ', 'ớ', '�?', 'ỡ', 'ợ', 'ở'),
        'ổ'=>array('o', 'ó', 'ò', 'õ', '�?', '�?', 'ô', 'ố', 'ồ', 'ỗ', 'ộ', 'ổ', 'ơ', 'ớ', '�?', 'ỡ', 'ợ', 'ở'),
        'ơ'=>array('o', 'ó', 'ò', 'õ', '�?', '�?', 'ô', 'ố', 'ồ', 'ỗ', 'ộ', 'ổ', 'ơ', 'ớ', '�?', 'ỡ', 'ợ', 'ở'),
        'ớ'=>array('o', 'ó', 'ò', 'õ', '�?', '�?', 'ô', 'ố', 'ồ', 'ỗ', 'ộ', 'ổ', 'ơ', 'ớ', '�?', 'ỡ', 'ợ', 'ở'),
        '�?'=>array('o', 'ó', 'ò', 'õ', '�?', '�?', 'ô', 'ố', 'ồ', 'ỗ', 'ộ', 'ổ', 'ơ', 'ớ', '�?', 'ỡ', 'ợ', 'ở'),
        'ỡ'=>array('o', 'ó', 'ò', 'õ', '�?', '�?', 'ô', 'ố', 'ồ', 'ỗ', 'ộ', 'ổ', 'ơ', 'ớ', '�?', 'ỡ', 'ợ', 'ở'),
        'ợ'=>array('o', 'ó', 'ò', 'õ', '�?', '�?', 'ô', 'ố', 'ồ', 'ỗ', 'ộ', 'ổ', 'ơ', 'ớ', '�?', 'ỡ', 'ợ', 'ở'),
        'ở'=>array('o', 'ó', 'ò', 'õ', '�?', '�?', 'ô', 'ố', 'ồ', 'ỗ', 'ộ', 'ổ', 'ơ', 'ớ', '�?', 'ỡ', 'ợ', 'ở')
    );
    public static function mb_str_split( $string ) {
        return preg_split('/(?<!^)(?!$)/u', $string );
    }
    /**
     * covert string to regular exception for search mongodb
     */
    public static function fixForSearchRegex($str){

        $str = strtolower($str);
        $str_list = self::mb_str_split($str);
        $vowels_k = array_keys(self::$vowels_1);

        $str_list_regex = $str_list;
        foreach($str_list as $i=>$char){
            if(isset(self::$vowels_1[$char])){

                //tồn tại không nhất quán với các tổ hợp oa, oe, ua, ue, uy
                if(($char == 'o' || $char == 'u') && isset($str_list[$i+1]) && ($str_list[$i+1] == 'a' || $str_list[$i+1] == 'e' || $str_list[$i+1] == 'y')){
                    $str_list_regex[$i] = '['.implode('|', self::$vowels_1[$char]).']'; //nguyên âm này có thể mang dấu thanh
                }
                //nếu trước và sau đ�?u là nguyên âm.
                else if(isset($str_list[$i-1]) && isset($str_list[$i+1]) &&
                    in_array($str_list[$i-1], $vowels_k) && in_array($str_list[$i+1], $vowels_k)){
                    $str_list_regex[$i] = '['.implode('|', self::$vowels_1[$char]).']'; //nguyên âm này có thể mang dấu thanh
                }
                //nếu trước là nguyên âm, sau không phải là nguyên âm.
                else if(isset($str_list[$i-1]) && isset($str_list[$i+1]) &&
                    in_array($str_list[$i-1], $vowels_k) && !in_array($str_list[$i+1], $vowels_k)){
                    $str_list_regex[$i] = '['.implode('|', self::$vowels_1[$char]).']'; //nguyên âm này có thể mang dấu thanh
                }
                //nếu tồn tại 2 nguyên âm kế tiếp.
                else if(isset($str_list[$i+1]) && isset($str_list[$i+2]) &&
                    in_array($str_list[$i+1], $vowels_k) && in_array($str_list[$i+2], $vowels_k)){
                    $str_list_regex[$i] = $char; //nguyên âm này không thể mang dấu thanh
                }
                //nếu tồn tại 2 nguyên âm truoc do.
                else if(isset($str_list[$i-1]) && isset($str_list[$i-2]) &&
                    in_array($str_list[$i-1], $vowels_k) && in_array($str_list[$i-2], $vowels_k)){
                    $str_list_regex[$i] = $char; //nguyên âm này không thể mang dấu thanh
                }
                //nếu kế tiếp là nguyên âm, và kế tiếp nữa không phải là nguyên âm
                else if(isset($str_list[$i+1]) && in_array($str_list[$i+1], $vowels_k) &&
                    (!isset($str_list[$i+2]) || !in_array($str_list[$i+2], $vowels_k))){
                    $str_list_regex[$i] = $char; //nguyên âm này không thể mang dấu thanh
                }
                //nếu trước và sau đ�?u không là nguyên âm.
                else{
                    $str_list_regex[$i] = '['.implode('|', self::$vowels_1[$char]).']'; //nguyên âm này có thể mang dấu thanh
                }

            }else if($char == ' '){
                $str_list_regex[$i] = '.*';
            }
        }
        if(implode('', $str_list_regex) == $str){
            return implode('.*', $str_list_regex);
        }else{
            return implode('', $str_list_regex);
        }
    }

    public static function fixForPreg($str){
        return str_replace(array_keys(self::$vowels_1), array_map('array_shift', array_values(self::$vowels_1)), $str);
    }

    private static function covertToUTF8($str){
        if(is_array($str))
        {
            foreach($str as $k => $v)
            {
                $str[$k] = self::covertToUTF8($v);
            }
            return $str;
        } elseif(is_string($str)) {

            $max = strlen($str);
            $buffer = "";
            for($i = 0; $i < $max; $i++){
                $c1 = $str{$i};
                if($c1>="\xc0"){
                    $c2 = $i+1 >= $max? "\x00" : $str{$i+1};
                    $c3 = $i+2 >= $max? "\x00" : $str{$i+2};
                    $c4 = $i+3 >= $max? "\x00" : $str{$i+3};
                    if($c1 >= "\xc0" & $c1 <= "\xdf"){
                        if($c2 >= "\x80" && $c2 <= "\xbf"){
                            $buffer .= $c1 . $c2;
                            $i++;
                        } else {
                            $cc1 = (chr(ord($c1) / 64) | "\xc0");
                            $cc2 = ($c1 & "\x3f") | "\x80";
                            $buffer .= $cc1 . $cc2;
                        }
                    } elseif($c1 >= "\xe0" & $c1 <= "\xef"){
                        if($c2 >= "\x80" && $c2 <= "\xbf" && $c3 >= "\x80" && $c3 <= "\xbf"){
                            $buffer .= $c1 . $c2 . $c3;
                            $i = $i + 2;
                        } else {
                            $cc1 = (chr(ord($c1) / 64) | "\xc0");
                            $cc2 = ($c1 & "\x3f") | "\x80";
                            $buffer .= $cc1 . $cc2;
                        }
                    } elseif($c1 >= "\xf0" & $c1 <= "\xf7"){
                        if($c2 >= "\x80" && $c2 <= "\xbf" && $c3 >= "\x80" && $c3 <= "\xbf" && $c4 >= "\x80" && $c4 <= "\xbf"){ //yeah, almost sure it's UTF8 already
                            $buffer .= $c1 . $c2 . $c3;
                            $i = $i + 2;
                        } else {
                            $cc1 = (chr(ord($c1) / 64) | "\xc0");
                            $cc2 = ($c1 & "\x3f") | "\x80";
                            $buffer .= $cc1 . $cc2;
                        }
                    } else {
                        $cc1 = (chr(ord($c1) / 64) | "\xc0");
                        $cc2 = (($c1 & "\x3f") | "\x80");
                        $buffer .= $cc1 . $cc2;
                    }
                } elseif(($c1 & "\xc0") == "\x80"){
                    if(isset(self::$charWin1252ToUtf8[ord($c1)])) {
                        $buffer .= self::$charWin1252ToUtf8[ord($c1)];
                    } else {
                        $cc1 = (chr(ord($c1) / 64) | "\xc0");
                        $cc2 = (($c1 & "\x3f") | "\x80");
                        $buffer .= $cc1 . $cc2;
                    }
                } else {
                    $buffer .= $c1;
                }
            }
            return $buffer;
        } else {
            return $str;
        }
    }
    static function fixCharUTF8($str){
        return self::convertToWin1252($str);
    }

    private static function convertToWin1252($str) {
        if(is_array($str)) {
            foreach($str as $k => $v) {
                $encode_before_convert = mb_detect_encoding($v);
                $str_convert = self::convertToWin1252($v);
                $encode_after_convert = mb_detect_encoding($str_convert);
                if(mb_check_encoding($str_convert) && $encode_before_convert == $encode_after_convert){
                    $str[$k] = $str_convert;
                }
            }
            return $str;
        } elseif(is_string($str)) {
            $encode_before_convert = mb_detect_encoding($str);
            $str_convert = utf8_decode(str_replace(array_keys(self::$charUtf8ToWin1252), array_values(self::$charUtf8ToWin1252), self::covertToUTF8($str)));
            $encode_after_convert = mb_detect_encoding($str_convert);
            if(mb_check_encoding($str_convert) && $encode_before_convert == $encode_after_convert){
                $str = $str_convert;
            }
            return $str;
        } else {
            return $str;
        }
    }

    public static function fixHtmlEntity ($string)  {
        $trans_tbl = array_flip (self::$charHtmlEntity);
        $ret = strtr ($string, $trans_tbl);
        return preg_replace('/&#(\d+);/me', "chr('\\1')",$ret);
    }

    public static function fixHtmlEntityUtf8($string){

        $patterns = array_keys(self::$charHtmlEntityScript);
        $replaces = array_values(self::$charHtmlEntityScript);

        return str_replace($patterns, $replaces, $string);
    }

    public static function preg_match($subject, $patterns, $options=array()){
        $result = false;
        $subject = self::fixForPreg($subject);
        if(is_object($patterns)){
            $patterns = (array)$patterns;
        }
        else if(is_string($patterns)){
            $patterns = array($patterns);
        }
        foreach($patterns as $pattern){
            $pattern = trim($pattern);
            $pattern = trim($pattern, '/');

            $regs = array();
            if(substr_count($pattern, '*') > 0){
                $regs[] = str_replace(array('*'), array('.*'), $pattern);
                $regs[] = str_replace(array('*'), array(' .* '), $pattern);
                $regs[] = str_replace(array('*'), array(''), $pattern).'*';
            }else{
                $regs[] = '^'.$pattern.'$';
            }
            foreach($regs as $reg){
                $reg = '/'.$reg.'/i';
                if(preg_match_all($reg, $subject, $matches)){
                    $result = true;
                    if(isset($options['debug']) && $options['debug']){
                        echo '<pre>';
                        print_r($matches);
                        echo '</pre>';
                        if(isset($options['die']) && $options['die']){
                            die;
                        }
                    }
                    break;
                }
            }
        }
        return $result;
    }

}

?>