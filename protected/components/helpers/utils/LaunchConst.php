<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LaunchConst
 *
 * @author An Dinh Luan <luanad6365@setacinq.com.vn>
 */
class LaunchConst {

	//Default limit
	const LIMIT_PER_PAGE = 5;
	//Default "View more" item;
	const LOAD_MORE_ITEM = 5;
	
	//Status of Item
	const STATUS_PUBLISHED = 1;
	const STATUS_UNPUBLISH = 0;
	
	//State of Item: Webinar,..
	const STATUS_APPROVED = 1;
	const STATUS_UNAPPROVE = 0;
	
	//Type of Books
	const BOOK_TYPE_SPONSORED = 1;
	//...
	
	//Type of Webninar
	const WEBINAR_TYPE_SPONSORED = 1;
	//...
	
	
}
