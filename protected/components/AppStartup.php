<?php

class AppStartup extends CBehavior {

	public function events() {
		return array_merge(parent::events(), array(
			'onBeginRequest' => 'beginRequest',
		));
	}

	public function beginRequest() {
		$domain = preg_replace('#^https?://#', '', yii::app()->getBaseUrl(true));
		$tenantSite = TTenantSites::model()->findByAttributes(array('dbu' => $domain));
		// get tenant sites
		if (is_object($tenantSite) && Common::checkMySQLUserExists($domain)) {
			//add session tenant
			yii::app()->session['tenantSite'] = $tenantSite->getAttributes();
			// switch db credentials for logged in users
			Yii::app()->db->setActive(false);
			Yii::app()->db->username = $tenantSite->dbu;
			Yii::app()->db->password = $tenantSite->e_dbpwd;
			Yii::app()->db->setActive(true);
			//initial theme
			Yii::app()->theme = $tenantSite->dbu;
		}

		//load files
		$loadFiles = new LoaderUtil();
		$files = array(
            'css/libs/bootstrap.min.css',
            'css/libs/bootstrap-theme.css',
            'css/styles.css',
            'css/libs/bootstrap-colorpicker.min.css',
            'js/libs/modernizr-2.5.3-respond-1.1.0.min.js',
            'js/libs/bootstrap.min.js',
            'js/libs/jquery.html5uploader.cus.js',
            'js/libs/bootstrap-colorpicker.min.js',
            'js/STAjax.js',
            'js/STQuery.js',
		);
		$loadFiles->load('root', $files);
		//Init constant url for PHP & JS
		$this->initConst();
	}

	
	//Init constant url for PHP & JS
	public function initConst() {
		define('BASE_URL', Yii::app()->request->getBaseUrl(true));
		define('THEME_URL', Yii::app()->theme->baseUrl);

		$bases = array(
			'baseUrl' => BASE_URL,
			'themeUrl' => THEME_URL
		);
		$params = 'PARAMS = ' . json_encode($bases);
		Yii::app()->clientScript->registerScript('BASE_DATA', $params, CClientScript::POS_END);
	}

}
