<?php

class BaseController extends CController {

    private $_assetsUrl;

    public function getAssetsUrl() {
        if ($this->getModule() != null) {
            return $this->getModule()->getAssetsUrl();
        }
        if ($this->_assetsUrl === null)
            $this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.assets'), FALSE, -1, YII_DEBUG);
        return $this->_assetsUrl;
    }
    
    public function getBaseUrl() {
        return Yii::app()->getBaseUrl(true);
    }

    public function getUploadUrl() {
        if (Yii::app()->params['extUpload']) {
            return Yii::app()->params['uploadUrl'];
        }
        return $this->getBaseUrl() . UploadedFile::UPLOADDIR;
    }

    /**
     * 
     * @return User
     */
    public function getViewer() {
        return Yii::app()->user->getUser();
    }

    public function init() {
        parent::init();
    }

    public function genUrl($addParams = array()) {
        $params = $_GET;
        if($addParams){
            foreach ($addParams as $key => $addParam) {
                $params[$key] = $addParam;
            }
        }

        return $this->createUrl($this->id . '/' . $this->action->id, $params);
    }

}