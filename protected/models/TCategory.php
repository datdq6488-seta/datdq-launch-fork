<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TCategory
 *
 * @author An Dinh Luan <luanad6365@setacinq.com.vn>
 */
class TCategory {

	public $data = array(
		1 => array(
			'id' => 1,
			'name' => 'Must watch'
		),
		2 => array(
			'id' => 2,
			'name' => 'Most popular'
		),
		3 => array(
			'id' => 3,
			'name' => 'Most Viewed'
		),
		4 => array(
			'id' => 4,
			'name' => 'Video Category 1'
		),
		5 => array(
			'id' => 5,
			'name' => 'Video Category 2'
		),
		6 => array(
			'id' => 6,
			'name' => 'Video Category 3'
		),
	);

}
