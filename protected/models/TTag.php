<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TTag
 *
 * @author An Dinh Luan <luanad6365@setacinq.com.vn>
 */
class TTag {

	public $data = array(
		1 => array(
			'id' => 1,
			'name' => 'Business'
		),
		2 => array(
			'id' => 2,
			'name' => 'Online'
		),
		3 => array(
			'id' => 3,
			'name' => 'Finance'
		),
		4 => array(
			'id' => 4,
			'name' => 'Relationships'
		),
		5 => array(
			'id' => 5,
			'name' => 'Banking'
		),
		6 => array(
			'id' => 6,
			'name' => 'Speaking'
		),
	);

}
