<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TVideo
 *
 * @author An Dinh Luan <luanad6365@setacinq.com.vn>
 */
class TVideo {

	public $data = array(
		1 => array(
			'id' => 1,
			'img' => 'godin-video.png',
			'name' => 'Building Relationships Cre...',
			'author' => 'Seth Godin',
			'length' => '12:32',
			'full_desc' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book",
			'link' =>'https://www.youtube.com/watch?v=PwL0ydaTBp8',
		),
		2 => array(
			'id' => 2,
			'img' => 'hsieh-video.png',
			'name' => 'Launching a Bussiness...',
			'author' => 'Tony Hsieh',
			'length' => '10:32',
			'full_desc' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy of type and scrambled it to make a type specimen book",
			'link' =>'https://www.youtube.com/watch?v=ptCHA4KcKDQ',
		),
		3 => array(
			'id' => 3,
			'img' => 'ries-video.png',
			'name' => 'What is a Lean Relationship',
			'author' => 'Eric Ries',
			'length' => '32:26',
			'full_desc' => "Lorem Ipsum is simply dummy text of the printing and typesetting industrry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book",
			'link' =>'https://www.youtube.com/watch?v=vTuRYonf7LA',
		),
		4 => array(
			'id' => 4,
			'img' => 'waynechuck-video.png',
			'name' => 'Why the Bussiness of...',
			'author' => 'Gary Waynerchuck',
			'length' => '23:55',
			'full_desc' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus a arcu egestas, varius turpis in, varius orci. Pellentesque viverra ultricies diam et tempor. Phasellus aliquet iaculis odio, ut dictum enim",
			'link' =>'https://www.youtube.com/watch?v=fI1_ZGVNmTc',
		),
	);

}
