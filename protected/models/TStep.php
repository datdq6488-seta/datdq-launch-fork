<?php

/**
 * Description of TStep
 *
 * @author An Dinh Luan <luanad6365@setacinq.com.vn>
 */
class TStep {

	public $data = array(
		1 => array(
			'id' => 1,
			'name' => 'Step 1',
			'full_name' => 'Pre-Launch Checklist',
			'link' => '#',
			'intro' => array(
				'name' => 'Step One - Introduction',
				'status' => 0,
				'video' => 'https://www.youtube.com/watch?v=PwL0ydaTBp8',
				'link' => '#',
			),
			'partner' => array(
				'img' => 'advertising.png',
				'link' => '#',
			),
			'feature_video' => array(
				1 => array(
					'id' => 1,
					'img' => 'godin-video.png',
					'name' => 'Building Relationships Cre...',
					'author' => 'Seth Godin'
				),
				2 => array(
					'id' => 2,
					'img' => 'hsieh-video.png',
					'name' => 'Launching a Bussiness...',
					'author' => 'Tony Hsieh'
				),
				3 => array(
					'id' => 3,
					'img' => 'ries-video.png',
					'name' => 'What is a Lean Relationship',
					'author' => 'Eric Ries'
				),
				4 => array(
					'id' => 4,
					'img' => 'waynechuck-video.png',
					'name' => 'Why the Bussiness of...',
					'author' => 'Gary Waynerchuck'
				),
			),
			'child_step' => array(
				1 => array(
					'id' => 1,
					'name' => 'Vestibulum sed lorem ut eros',
					'status' => 0,
					'parent_step' => 1,
					'sub_name' => 1,
					'full_desc' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc nec diam nec turpis blandit semper. Integer id iaculis erat, vitae congue orci. Donec sit amet egestas est. Vivamus aliquam in ipsum sit amet cursus. Mauris ultrices, urna nec auctor ornare, nibh lectus malesuada risus, nec gravida nibh nisi vel dolor. Aenean dapibus fermentum ipsum non rhoncus. Fusce consectetur porta sem a vulputate. Duis iaculis, est a pharetra rhoncus, lorem arcu tempor nisl, quis ornare nulla est vel sapien.'
				),
				2 => array(
					'id' => 2,
					'name' => 'Vivamus tristique velit at',
					'status' => 1,
					'parent_step' => 1,
					'sub_name' => 2,
					'full_desc' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc nec diam nec turpis blandit semper. Integer id iaculis erat, vitae congue orci. Donec sit amet egestas est. Vivamus aliquam in ipsum sit amet cursus. Mauris ultrices, urna nec auctor ornare, nibh lectus malesuada risus, nec gravida nibh nisi vel dolor. Aenean dapibus fermentum ipsum non rhoncus. Fusce consectetur porta sem a vulputate. Duis iaculis, est a pharetra rhoncus, lorem arcu tempor nisl, quis ornare nulla est vel sapien.'
				),
				3 => array(
					'id' => 3,
					'name' => 'Vestibulum ultricies ornare',
					'status' => 0,
					'parent_step' => 1,
					'sub_name' => 3,
					'full_desc' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc nec diam nec turpis blandit semper. Integer id iaculis erat, vitae congue orci. Donec sit amet egestas est. Vivamus aliquam in ipsum sit amet cursus. Mauris ultrices, urna nec auctor ornare, nibh lectus malesuada risus, nec gravida nibh nisi vel dolor. Aenean dapibus fermentum ipsum non rhoncus. Fusce consectetur porta sem a vulputate. Duis iaculis, est a pharetra rhoncus, lorem arcu tempor nisl, quis ornare nulla est vel sapien.'
				),
				4 => array(
					'id' => 4,
					'name' => 'Donec euismod orc faucibus',
					'status' => 1,
					'parent_step' => 1,
					'sub_name' => 4,
					'full_desc' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc nec diam nec turpis blandit semper. Integer id iaculis erat, vitae congue orci. Donec sit amet egestas est. Vivamus aliquam in ipsum sit amet cursus. Mauris ultrices, urna nec auctor ornare, nibh lectus malesuada risus, nec gravida nibh nisi vel dolor. Aenean dapibus fermentum ipsum non rhoncus. Fusce consectetur porta sem a vulputate. Duis iaculis, est a pharetra rhoncus, lorem arcu tempor nisl, quis ornare nulla est vel sapien.'
				),
				5 => array(
					'id' => 5,
					'name' => 'Aenean luctus varius',
					'status' => 0,
					'parent_step' => 1,
					'sub_name' => 5,
					'full_desc' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc nec diam nec turpis blandit semper. Integer id iaculis erat, vitae congue orci. Donec sit amet egestas est. Vivamus aliquam in ipsum sit amet cursus. Mauris ultrices, urna nec auctor ornare, nibh lectus malesuada risus, nec gravida nibh nisi vel dolor. Aenean dapibus fermentum ipsum non rhoncus. Fusce consectetur porta sem a vulputate. Duis iaculis, est a pharetra rhoncus, lorem arcu tempor nisl, quis ornare nulla est vel sapien.'
				),
			),
			'class' => 'first',
		),
		2 => array(
			'id' => 2,
			'name' => 'Step 2',
			'full_name' => 'Assemble Your Resources',
			'link' => '#',
			'intro' => array(
				'name' => 'Step Two - Introduction',
				'status' => 1,
				'video' => 'https://www.youtube.com/watch?v=ptCHA4KcKDQ',
				'link' => '#',
			),
			'partner' => array(
				'img' => 'advertising.png',
				'link' => '#',
			),
			'feature_video' => array(
				1 => array(
					'id' => 5,
					'img' => 'godin-video.png',
					'name' => 'Building Relationships Cre...',
					'author' => 'Seth Godin'
				),
				2 => array(
					'id' => 6,
					'img' => 'hsieh-video.png',
					'name' => 'Launching a Bussiness...',
					'author' => 'Tony Hsieh'
				),
				3 => array(
					'id' => 7,
					'img' => 'ries-video.png',
					'name' => 'What is a Lean Relationship',
					'author' => 'Eric Ries'
				),
				4 => array(
					'id' => 8,
					'img' => 'waynechuck-video.png',
					'name' => 'Why the Bussiness of...',
					'author' => 'Gary Waynerchuck'
				),
			),
			'child_step' => array(
				6 => array(
					'id' => 6,
					'name' => 'Pellentesque dignissim',
					'status' => 1,
					'parent_step' => 2,
					'sub_name' => 1,
					'full_desc' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc nec diam nec turpis blandit semper. Integer id iaculis erat, vitae congue orci. Donec sit amet egestas est. Vivamus aliquam in ipsum sit amet cursus. Mauris ultrices, urna nec auctor ornare, nibh lectus malesuada risus, nec gravida nibh nisi vel dolor. Aenean dapibus fermentum ipsum non rhoncus. Fusce consectetur porta sem a vulputate. Duis iaculis, est a pharetra rhoncus, lorem arcu tempor nisl, quis ornare nulla est vel sapien.'
				),
				7 => array(
					'id' => 7,
					'name' => 'Nunc non dui ac eros auctor accumsan',
					'status' => 0,
					'parent_step' => 2,
					'sub_name' => 2,
					'full_desc' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc nec diam nec turpis blandit semper. Integer id iaculis erat, vitae congue orci. Donec sit amet egestas est. Vivamus aliquam in ipsum sit amet cursus. Mauris ultrices, urna nec auctor ornare, nibh lectus malesuada risus, nec gravida nibh nisi vel dolor. Aenean dapibus fermentum ipsum non rhoncus. Fusce consectetur porta sem a vulputate. Duis iaculis, est a pharetra rhoncus, lorem arcu tempor nisl, quis ornare nulla est vel sapien.'
				),
				8 => array(
					'id' => 8,
					'name' => 'Ut porta mauris at varius ornare',
					'status' => 1,
					'parent_step' => 2,
					'sub_name' => 3,
					'full_desc' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc nec diam nec turpis blandit semper. Integer id iaculis erat, vitae congue orci. Donec sit amet egestas est. Vivamus aliquam in ipsum sit amet cursus. Mauris ultrices, urna nec auctor ornare, nibh lectus malesuada risus, nec gravida nibh nisi vel dolor. Aenean dapibus fermentum ipsum non rhoncus. Fusce consectetur porta sem a vulputate. Duis iaculis, est a pharetra rhoncus, lorem arcu tempor nisl, quis ornare nulla est vel sapien.'
				),
				9 => array(
					'id' => 9,
					'name' => 'Quisque et nisl posuere ero',
					'status' => 0,
					'parent_step' => 2,
					'sub_name' => 4,
					'full_desc' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc nec diam nec turpis blandit semper. Integer id iaculis erat, vitae congue orci. Donec sit amet egestas est. Vivamus aliquam in ipsum sit amet cursus. Mauris ultrices, urna nec auctor ornare, nibh lectus malesuada risus, nec gravida nibh nisi vel dolor. Aenean dapibus fermentum ipsum non rhoncus. Fusce consectetur porta sem a vulputate. Duis iaculis, est a pharetra rhoncus, lorem arcu tempor nisl, quis ornare nulla est vel sapien.'
				),
				10 => array(
					'id' => 10,
					'name' => 'Etiam imperdiet lectus',
					'status' => 1,
					'parent_step' => 2,
					'sub_name' => 5,
					'full_desc' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc nec diam nec turpis blandit semper. Integer id iaculis erat, vitae congue orci. Donec sit amet egestas est. Vivamus aliquam in ipsum sit amet cursus. Mauris ultrices, urna nec auctor ornare, nibh lectus malesuada risus, nec gravida nibh nisi vel dolor. Aenean dapibus fermentum ipsum non rhoncus. Fusce consectetur porta sem a vulputate. Duis iaculis, est a pharetra rhoncus, lorem arcu tempor nisl, quis ornare nulla est vel sapien.'
				),
			),
			'class' => '',
		),
		3 => array(
			'id' => 3,
			'name' => 'Step 3',
			'full_name' => 'Fuel The Tank',
			'link' => '#',
			'intro' => array(
				'name' => 'Step Three - Introduction',
				'status' => 0,
				'video' => 'https://www.youtube.com/watch?v=vTuRYonf7LA',
				'link' => '#',
			),
			'partner' => array(
				'img' => 'advertising.png',
				'link' => '#',
			),
			'feature_video' => array(
				1 => array(
					'id' => 9,
					'img' => 'godin-video.png',
					'name' => 'Building Relationships Cre...',
					'author' => 'Seth Godin'
				),
				2 => array(
					'id' => 10,
					'img' => 'hsieh-video.png',
					'name' => 'Launching a Bussiness...',
					'author' => 'Tony Hsieh'
				),
				3 => array(
					'id' => 11,
					'img' => 'ries-video.png',
					'name' => 'What is a Lean Relationship',
					'author' => 'Eric Ries'
				),
				4 => array(
					'id' => 12,
					'img' => 'waynechuck-video.png',
					'name' => 'Why the Bussiness of...',
					'author' => 'Gary Waynerchuck'
				),
			),
			'child_step' => array(
				11 => array(
					'id' => 11,
					'name' => 'Integer tincidunt arcu nec',
					'status' => 0,
					'parent_step' => 3,
					'sub_name' => 1,
					'full_desc' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc nec diam nec turpis blandit semper. Integer id iaculis erat, vitae congue orci. Donec sit amet egestas est. Vivamus aliquam in ipsum sit amet cursus. Mauris ultrices, urna nec auctor ornare, nibh lectus malesuada risus, nec gravida nibh nisi vel dolor. Aenean dapibus fermentum ipsum non rhoncus. Fusce consectetur porta sem a vulputate. Duis iaculis, est a pharetra rhoncus, lorem arcu tempor nisl, quis ornare nulla est vel sapien.'
				),
				12 => array(
					'id' => 12,
					'name' => 'Vestibulum et orci at metus solli',
					'status' => 0,
					'parent_step' => 3,
					'sub_name' => 2,
					'full_desc' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc nec diam nec turpis blandit semper. Integer id iaculis erat, vitae congue orci. Donec sit amet egestas est. Vivamus aliquam in ipsum sit amet cursus. Mauris ultrices, urna nec auctor ornare, nibh lectus malesuada risus, nec gravida nibh nisi vel dolor. Aenean dapibus fermentum ipsum non rhoncus. Fusce consectetur porta sem a vulputate. Duis iaculis, est a pharetra rhoncus, lorem arcu tempor nisl, quis ornare nulla est vel sapien.'
				),
				13 => array(
					'id' => 13,
					'name' => 'In in nunc quis ante bl',
					'status' => 1,
					'parent_step' => 3,
					'sub_name' => 3,
					'full_desc' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc nec diam nec turpis blandit semper. Integer id iaculis erat, vitae congue orci. Donec sit amet egestas est. Vivamus aliquam in ipsum sit amet cursus. Mauris ultrices, urna nec auctor ornare, nibh lectus malesuada risus, nec gravida nibh nisi vel dolor. Aenean dapibus fermentum ipsum non rhoncus. Fusce consectetur porta sem a vulputate. Duis iaculis, est a pharetra rhoncus, lorem arcu tempor nisl, quis ornare nulla est vel sapien.'
				),
				14 => array(
					'id' => 14,
					'name' => 'Proin ac felis quis nunc eui',
					'status' => 1,
					'parent_step' => 3,
					'sub_name' => 4,
					'full_desc' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc nec diam nec turpis blandit semper. Integer id iaculis erat, vitae congue orci. Donec sit amet egestas est. Vivamus aliquam in ipsum sit amet cursus. Mauris ultrices, urna nec auctor ornare, nibh lectus malesuada risus, nec gravida nibh nisi vel dolor. Aenean dapibus fermentum ipsum non rhoncus. Fusce consectetur porta sem a vulputate. Duis iaculis, est a pharetra rhoncus, lorem arcu tempor nisl, quis ornare nulla est vel sapien.'
				),
				15 => array(
					'id' => 15,
					'name' => 'Ut a diam a quam malesuada',
					'status' => 1,
					'parent_step' => 3,
					'sub_name' => 5,
					'full_desc' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc nec diam nec turpis blandit semper. Integer id iaculis erat, vitae congue orci. Donec sit amet egestas est. Vivamus aliquam in ipsum sit amet cursus. Mauris ultrices, urna nec auctor ornare, nibh lectus malesuada risus, nec gravida nibh nisi vel dolor. Aenean dapibus fermentum ipsum non rhoncus. Fusce consectetur porta sem a vulputate. Duis iaculis, est a pharetra rhoncus, lorem arcu tempor nisl, quis ornare nulla est vel sapien.'
				),
			),
			'class' => '',
		),
		4 => array(
			'id' => 4,
			'name' => 'Step 4',
			'full_name' => 'Get Ready For Take Off',
			'link' => '#',
			'intro' => array(
				'name' => 'Step Four - Introduction',
				'status' => 1,
				'video' => 'https://www.youtube.com/watch?v=PwL0ydaTBp8',
				'link' => '#',
			),
			'partner' => array(
				'img' => 'advertising.png',
				'link' => '#',
			),
			'feature_video' => array(
				1 => array(
					'id' => 13,
					'img' => 'godin-video.png',
					'name' => 'Building Relationships Cre...',
					'author' => 'Seth Godin'
				),
				2 => array(
					'id' => 14,
					'img' => 'hsieh-video.png',
					'name' => 'Launching a Bussiness...',
					'author' => 'Tony Hsieh'
				),
				3 => array(
					'id' => 15,
					'img' => 'ries-video.png',
					'name' => 'What is a Lean Relationship',
					'author' => 'Eric Ries'
				),
				4 => array(
					'id' => 16,
					'img' => 'waynechuck-video.png',
					'name' => 'Why the Bussiness of...',
					'author' => 'Gary Waynerchuck'
				),
			),
			'child_step' => array(
				16 => array(
					'id' => 16,
					'name' => 'Identify Your Target',
					'status' => 1,
					'parent_step' => 4,
					'sub_name' => 1,
					'full_desc' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc nec diam nec turpis blandit semper. Integer id iaculis erat, vitae congue orci. Donec sit amet egestas est. Vivamus aliquam in ipsum sit amet cursus. Mauris ultrices, urna nec auctor ornare, nibh lectus malesuada risus, nec gravida nibh nisi vel dolor. Aenean dapibus fermentum ipsum non rhoncus. Fusce consectetur porta sem a vulputate. Duis iaculis, est a pharetra rhoncus, lorem arcu tempor nisl, quis ornare nulla est vel sapien.'
				),
				17 => array(
					'id' => 17,
					'name' => 'Build Your Minimum Viable Product',
					'status' => 1,
					'parent_step' => 4,
					'sub_name' => 2,
					'full_desc' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc nec diam nec turpis blandit semper. Integer id iaculis erat, vitae congue orci. Donec sit amet egestas est. Vivamus aliquam in ipsum sit amet cursus. Mauris ultrices, urna nec auctor ornare, nibh lectus malesuada risus, nec gravida nibh nisi vel dolor. Aenean dapibus fermentum ipsum non rhoncus. Fusce consectetur porta sem a vulputate. Duis iaculis, est a pharetra rhoncus, lorem arcu tempor nisl, quis ornare nulla est vel sapien.'
				),
				18 => array(
					'id' => 18,
					'name' => 'Develop Partnerships & Distribution',
					'status' => 1,
					'parent_step' => 4,
					'sub_name' => 3,
					'full_desc' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc nec diam nec turpis blandit semper. Integer id iaculis erat, vitae congue orci. Donec sit amet egestas est. Vivamus aliquam in ipsum sit amet cursus. Mauris ultrices, urna nec auctor ornare, nibh lectus malesuada risus, nec gravida nibh nisi vel dolor. Aenean dapibus fermentum ipsum non rhoncus. Fusce consectetur porta sem a vulputate. Duis iaculis, est a pharetra rhoncus, lorem arcu tempor nisl, quis ornare nulla est vel sapien.'
				),
				19 => array(
					'id' => 19,
					'name' => 'Engage With Customer, Build Relationships',
					'status' => 0,
					'parent_step' => 4,
					'sub_name' => 4,
					'full_desc' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc nec diam nec turpis blandit semper. Integer id iaculis erat, vitae congue orci. Donec sit amet egestas est. Vivamus aliquam in ipsum sit amet cursus. Mauris ultrices, urna nec auctor ornare, nibh lectus malesuada risus, nec gravida nibh nisi vel dolor. Aenean dapibus fermentum ipsum non rhoncus. Fusce consectetur porta sem a vulputate. Duis iaculis, est a pharetra rhoncus, lorem arcu tempor nisl, quis ornare nulla est vel sapien.'
				),
				20 => array(
					'id' => 20,
					'name' => 'Sell! Sell! Sell!',
					'status' => 0,
					'parent_step' => 4,
					'sub_name' => 5,
					'full_desc' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc nec diam nec turpis blandit semper. Integer id iaculis erat, vitae congue orci. Donec sit amet egestas est. Vivamus aliquam in ipsum sit amet cursus. Mauris ultrices, urna nec auctor ornare, nibh lectus malesuada risus, nec gravida nibh nisi vel dolor. Aenean dapibus fermentum ipsum non rhoncus. Fusce consectetur porta sem a vulputate. Duis iaculis, est a pharetra rhoncus, lorem arcu tempor nisl, quis ornare nulla est vel sapien.'
				),
			),
			'class' => '',
		),
		5 => array(
			'id' => 5,
			'name' => 'Step 5',
			'full_name' => 'Launch!',
			'link' => '#',
			'intro' => array(
				'name' => 'Step Five - Introduction',
				'status' => 1,
				'video' => 'https://www.youtube.com/watch?v=fI1_ZGVNmTc',
				'link' => '#',
			),
			'partner' => array(
				'img' => 'advertising.png',
				'link' => '#',
			),
			'feature_video' => array(
				1 => array(
					'id' => 17,
					'img' => 'godin-video.png',
					'name' => 'Building Relationships Cre...',
					'author' => 'Seth Godin'
				),
				2 => array(
					'id' => 18,
					'img' => 'hsieh-video.png',
					'name' => 'Launching a Bussiness...',
					'author' => 'Tony Hsieh'
				),
				3 => array(
					'id' => 19,
					'img' => 'ries-video.png',
					'name' => 'What is a Lean Relationship',
					'author' => 'Eric Ries'
				),
				4 => array(
					'id' => 20,
					'img' => 'waynechuck-video.png',
					'name' => 'Why the Bussiness of...',
					'author' => 'Gary Waynerchuck'
				),
			),
			'child_step' => array(
				21 => array(
					'id' => 21,
					'name' => 'Phasellus bibendum libero ut felis dign',
					'status' => 0,
					'parent_step' => 5,
					'sub_name' => 1,
					'full_desc' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc nec diam nec turpis blandit semper. Integer id iaculis erat, vitae congue orci. Donec sit amet egestas est. Vivamus aliquam in ipsum sit amet cursus. Mauris ultrices, urna nec auctor ornare, nibh lectus malesuada risus, nec gravida nibh nisi vel dolor. Aenean dapibus fermentum ipsum non rhoncus. Fusce consectetur porta sem a vulputate. Duis iaculis, est a pharetra rhoncus, lorem arcu tempor nisl, quis ornare nulla est vel sapien.'
				),
				22 => array(
					'id' => 22,
					'name' => 'Integer fringilla nisi id bla',
					'status' => 1,
					'parent_step' => 5,
					'sub_name' => 2,
					'full_desc' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc nec diam nec turpis blandit semper. Integer id iaculis erat, vitae congue orci. Donec sit amet egestas est. Vivamus aliquam in ipsum sit amet cursus. Mauris ultrices, urna nec auctor ornare, nibh lectus malesuada risus, nec gravida nibh nisi vel dolor. Aenean dapibus fermentum ipsum non rhoncus. Fusce consectetur porta sem a vulputate. Duis iaculis, est a pharetra rhoncus, lorem arcu tempor nisl, quis ornare nulla est vel sapien.'
				),
				23 => array(
					'id' => 23,
					'name' => 'Aliquam mollis augue et mass',
					'status' => 1,
					'parent_step' => 5,
					'sub_name' => 3,
					'full_desc' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc nec diam nec turpis blandit semper. Integer id iaculis erat, vitae congue orci. Donec sit amet egestas est. Vivamus aliquam in ipsum sit amet cursus. Mauris ultrices, urna nec auctor ornare, nibh lectus malesuada risus, nec gravida nibh nisi vel dolor. Aenean dapibus fermentum ipsum non rhoncus. Fusce consectetur porta sem a vulputate. Duis iaculis, est a pharetra rhoncus, lorem arcu tempor nisl, quis ornare nulla est vel sapien.'
				),
				24 => array(
					'id' => 24,
					'name' => 'Ut in tellus eget velit volutpat va',
					'status' => 1,
					'parent_step' => 5,
					'sub_name' => 4,
					'full_desc' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc nec diam nec turpis blandit semper. Integer id iaculis erat, vitae congue orci. Donec sit amet egestas est. Vivamus aliquam in ipsum sit amet cursus. Mauris ultrices, urna nec auctor ornare, nibh lectus malesuada risus, nec gravida nibh nisi vel dolor. Aenean dapibus fermentum ipsum non rhoncus. Fusce consectetur porta sem a vulputate. Duis iaculis, est a pharetra rhoncus, lorem arcu tempor nisl, quis ornare nulla est vel sapien.'
				),
				25 => array(
					'id' => 25,
					'name' => 'Sed quis felis ut leo dapib',
					'status' => 0,
					'parent_step' => 5,
					'sub_name' => 5,
					'full_desc' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc nec diam nec turpis blandit semper. Integer id iaculis erat, vitae congue orci. Donec sit amet egestas est. Vivamus aliquam in ipsum sit amet cursus. Mauris ultrices, urna nec auctor ornare, nibh lectus malesuada risus, nec gravida nibh nisi vel dolor. Aenean dapibus fermentum ipsum non rhoncus. Fusce consectetur porta sem a vulputate. Duis iaculis, est a pharetra rhoncus, lorem arcu tempor nisl, quis ornare nulla est vel sapien.'
				),
			),
			'class' => 'last',
		),
	);

	public function findChild($id) {
		foreach ($this->data as $parent) {
			foreach ($parent['child_step'] as $k => $child) {
				if ((int) $id == (int) $k)
					return $parent['child_step'][$id];
			}
		}
	}

}
