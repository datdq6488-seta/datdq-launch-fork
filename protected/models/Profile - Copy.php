<?php

/**
 * This is the model class for table "tbl_profiles".
 *
 * The followings are the available columns in table 'tbl_profiles':
 * @property integer $id
 * @property string $full_name
 * @property string $email
 * @property string $new_email
 * @property integer $tenant_id
 * @property integer $tenant_owner
 * @property string $password
 * @property integer $role_id
 * @property integer $user_type
 * @property string $auth_key
 * @property integer $status
 * @property integer $created
 */
class Profile extends CActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_BANNED = 2;        //TODO
    const STATUS_REMOVED = 3;    //TODO

    public $oldAvatar;
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'tbl_profiles';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('company_type, keep_private', 'numerical', 'integerOnly'=>true),
            array('full_name, company', 'length', 'max'=>255),
            array('bio_user', 'length', 'max'=>1500),
            array('full_name', 'length', 'min'=>2),
            array('id, company, full_name', 'safe', 'on'=>'search'),
            array('company, full_name','required'),
        );
    }

    /*public function relations()
    {
        return array(
            'tenant' => array(self::BELONGS_TO, 'TTenant', 'tenant_id'), // tenant table
        );
    }*/

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'full_name' => Yii::t('_yii','Public Name'),
            'company' => Yii::t('_yii','Company Name<span>(optional)</span>'),
            'email' => Yii::t('_yii','Email Address'),
            'company_type' => Yii::t('_yii','Company Type'),
            'bio_user' => Yii::t('_yii','Bio<span>(optional)</span>'),
            'keep_private' => Yii::t('_yii','Keep Private'),
            'avatar' => Yii::t('_yii','Profile Photo'),
            'created' => Yii::t('_yii','Created'),
            'validation'=>Yii::t('_yii', 'Enter both words separated by a space: '),
        );
    }
    public function afterFind() {
        parent::afterFind();
        $this->oldAvatar = $this->avatar;
    }

    public function afterDelete() {
        $this->deleteImagem();
        return parent::afterDelete();
    }
    public function avatarUpdate($imageName) {
        $uploadPath = Yii::app()->params['uploadPath'];
        if(is_object($this->avatar)) {
            $pathMove = $uploadPath.'/'.$imageName;
            $this->avatar->saveAs($pathMove);
            if(!empty($this->oldAvatar)) {
                $delete = Yii::app()->params['uploadPath'].'/'.$this->oldAvatar;
                if(file_exists($delete)) unlink($delete);
            }
        }
        if(empty($this->avatar) && !empty($this->oldAvatar)) $this->avatar = $this->oldAvatar;
    }
    public function deleteImagem() {
        $imagem = $this->avatar;
        return unlink(Yii::app()->params['uploadPath'].'/'.$imagem);
    }
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TUser the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
