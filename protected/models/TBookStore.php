<?php

/**
 * This is the model class for table "{{book_store}}".
 *
 * The followings are the available columns in table '{{book_store}}':
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $author
 * @property string $keyword
 * @property string $publisher
 * @property string $published_date
 * @property integer $published
 * @property integer $type
 * @property integer $book_category_id
 * @property integer $tenant_id
 * @property integer $state
 * @property string $link
 * @property string $created
 * @property string $modified
 * @property string $alias
 */
class TBookStore extends CActiveRecord
{
	public $category_name;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{book_store}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, publisher, book_category_id, tenant_id', 'required'),
			array('published, type, book_category_id, tenant_id, state', 'numerical', 'integerOnly'=>true),
			array('title, keyword, link, alias', 'length', 'max'=>255),
			array('author, publisher', 'length', 'max'=>45),
			array('description, published_date, created, modified', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, description, author, keyword, publisher, published_date, published, type, book_category_id, tenant_id, state, link, created, modified, alias', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'description' => 'Description',
			'author' => 'Author',
			'keyword' => 'Keyword',
			'publisher' => 'Publisher',
			'published_date' => 'Published Date',
			'published' => 'Published',
			'type' => 'Type',
			'book_category_id' => 'Book Category',
			'tenant_id' => 'Tenant',
			'state' => 'State',
			'link' => 'Link',
			'created' => 'Created',
			'modified' => 'Modified',
			'alias' => 'Alias',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('author',$this->author,true);
		$criteria->compare('keyword',$this->keyword,true);
		$criteria->compare('publisher',$this->publisher,true);
		$criteria->compare('published_date',$this->published_date,true);
		$criteria->compare('published',$this->published);
		$criteria->compare('type',$this->type);
		$criteria->compare('book_category_id',$this->book_category_id);
		$criteria->compare('tenant_id',$this->tenant_id);
		$criteria->compare('state',$this->state);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('modified',$this->modified,true);
		$criteria->compare('alias',$this->alias,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TBookStore the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
