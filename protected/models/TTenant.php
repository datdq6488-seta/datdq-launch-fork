<?php

/**
 * This is the model class for table "tbl_tenant".
 *
 * The followings are the available columns in table 'tbl_tenant':
 * @property integer $id
 * @property string $domain
 * @property string $dbu
 * @property string $e_dbpwd
 * @property string $full_name
 * @property string $website
 * @property string $email
 * @property string $theme
 * @property string $layout
 * @property string $language
 * @property string $business_name
 * @property integer $status
 * @property integer $created
 */
class TTenant extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_tenant';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('domain, dbu, e_dbpwd, full_name, website, email, business_name', 'required'),
			array('status, created', 'numerical', 'integerOnly'=>true),
			array('domain', 'length', 'max'=>32),
			array('dbu, full_name, website, email, business_name', 'length', 'max'=>255),
			array('e_dbpwd', 'length', 'max'=>1024),
			array('theme', 'length', 'max'=>100),
			array('layout', 'length', 'max'=>50),
			array('language', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, domain, dbu, e_dbpwd, full_name, website, email, theme, layout, language, business_name, status, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'domain' => 'Domain',
			'dbu' => 'Dbu',
			'e_dbpwd' => 'E Dbpwd',
			'full_name' => 'Full Name',
			'website' => 'Website',
			'email' => 'Email',
			'theme' => 'Theme',
			'layout' => 'Layout',
			'language' => 'Language',
			'business_name' => 'Business Name',
			'status' => 'Status',
			'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('domain',$this->domain,true);
		$criteria->compare('dbu',$this->dbu,true);
		$criteria->compare('e_dbpwd',$this->e_dbpwd,true);
		$criteria->compare('full_name',$this->full_name,true);
		$criteria->compare('website',$this->website,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('theme',$this->theme,true);
		$criteria->compare('layout',$this->layout,true);
		$criteria->compare('language',$this->language,true);
		$criteria->compare('business_name',$this->business_name,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created',$this->created);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TTenant the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
