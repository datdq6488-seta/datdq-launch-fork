<?php

/**
 * This is the model class for table "tbl_user_key".
 *
 * The followings are the available columns in table 'tbl_user_key':
 * @property integer $id
 * @property integer $user_id
 * @property integer $type
 * @property string $key
 * @property integer $created
 * @property integer $consume_time
 * @property integer $expire_time
 */
class TUserKey extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_user_key';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'type' => 'Type',
			'key' => 'Key',
			'created' => 'Created',
			'consume_time' => 'Consume Time',
			'expire_time' => 'Expire Time',
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TUserKey the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function generateToken($email, $tenantDbu = '') {
		if(!$tenantDbu){
			global $tenant;
			$tenantDbu = $tenant['dbu'];
		}
		return md5($email.'-'.$tenantDbu.'-'.time());
	}
}
