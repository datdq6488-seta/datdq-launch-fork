<?php

/**
 * This is the model class for table "{{webinar}}".
 *
 * The followings are the available columns in table '{{webinar}}':
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $approved
 * @property string $author
 * @property string $approved_date
 * @property integer $published
 * @property integer $type
 * @property string $created
 * @property string $modified
 * @property string $alias
 */
class TWebinar extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{webinar}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, description, approved, created', 'required'),
			array('approved, published, type', 'numerical', 'integerOnly'=>true),
			array('title, author, alias', 'length', 'max'=>255),
			array('approved_date, modified', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, description, approved, author, approved_date, published, type, created, modified, alias', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'description' => 'Description',
			'approved' => 'Approved',
			'author' => 'Author',
			'approved_date' => 'Approved Date',
			'published' => 'Published',
			'type' => 'Type',
			'created' => 'Created',
			'modified' => 'Modified',
			'alias' => 'Alias',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('approved',$this->approved);
		$criteria->compare('author',$this->author,true);
		$criteria->compare('approved_date',$this->approved_date,true);
		$criteria->compare('published',$this->published);
		$criteria->compare('type',$this->type);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('modified',$this->modified,true);
		$criteria->compare('alias',$this->alias,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TWebinar the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
