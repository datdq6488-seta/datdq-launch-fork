<div id="preneurs-widget">
	<div class="widget-title">
		 <span>Launch-o-preneurs</span>
		 <a href="<?php echo Yii::app()->request->baseUrl;?>/preneurs">See all</a>
	</div><!--.widget-title-->

	<div class="widget-list">
		<ul>
			<?php foreach ($data as $user) :?>
			<li>
				<div class="avatar">
					<a href="<?php echo Yii::app()->request->baseUrl;?>/preneurs/detail"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/files/avatar/<?php echo $user['avatar'];?>"/></a>
				</div><!--.avatar-->
				<div class="detail">
					<span><?php echo $user['name'];?></span>
					<p><?php echo $user['description'];?></p>
					<a href="<?php echo Yii::app()->request->baseUrl;?>/preneurs/detail">Meet Mark</a>
				</div><!--.detail-->
			</li>
			<?php endforeach;?>
		</ul>
	</div><!--.widget-list-->
</div>