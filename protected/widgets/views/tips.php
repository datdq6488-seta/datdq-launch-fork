<div id="tips-widget">
	<div class="widget-title">
		 <span>Tip of the day</span>
		 <a href="<?php echo Yii::app()->request->baseUrl;?>/tips">View more</a>
	</div><!--.widget-title-->

	<div class="widget-list">
		<div class="quote">
			<?php echo $data['quote'];?>
		</div><!--.quote-->
		<span class="quote-arrow"></span>
		<div class="avatar">
			<a href="<?php echo Yii::app()->request->baseUrl;?>/preneurs/detail"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/files/avatar/<?php echo $data['avatar'];?>"/></a>
		</div><!--.avatar-->
		<div class="detail">									
			<span><?php echo $data['name'];?></span>
			<p><?php echo $data['description'];?></p>
			<a href="<?php echo Yii::app()->request->baseUrl;?>/preneurs/detail">Meet Scott</a>
		</div><!--.detail-->
	</div><!--.widget-list-->
</div>