<div id="wrapper-nav">
	<div class="container">
		<div class="row">
			<div class="span12">
				<div id="nav">
					<ul class="parent-ul">
						<?php foreach ($data as $parent): ?>
							<li class="<?php echo $parent['class'];?><?php echo ($parent['id'] == $currentStep) ? ' active' : '';?>">
								<a href="<?php echo Yii::app()->getBaseUrl(true).'/landing/parent/step/'.$parent['id'];?>">
									<span><?php echo $parent['name'];?></span>
									<span><?php echo $parent['full_name'];?></span>
								</a>
								<span class="parent-li-hover"></span>
								<div class="step-content">
									<div class="list-child-wrap">
										<div class="top-title">
											Things You Need To Do
										</div>
										<div class="list-child">
											<ul class="child-ul">
												<li>
													<span><?php echo $parent['intro']['name'];?></span>
													<a class="<?php echo $parent['intro']['status'] == 0 ? 'uncompleted' : 'completed' ;?>" href="<?php echo Yii::app()->getBaseUrl(true).'/landing/parent/step/'.$parent['id'];?>"> <?php echo $parent['intro']['status'] == 0 ? 'Get Started' : 'Completed' ;?> </a>
												</li>
												<?php $i = 1;?>
												<?php foreach ($parent['child_step'] as $child): ?>
													<li>
														<span><?php echo $i++;?>.&nbsp;<?php echo $child['name'];?></span>
														<a class="<?php echo $child['status'] == 0 ? 'uncompleted' : 'completed' ;?>" href="<?php echo Yii::app()->getBaseUrl(true).'/landing/child/step/'.$child['id'];?>"> <?php echo $child['status'] == 0 ? 'Get Started' : 'Completed' ;?> </a>
													</li>
												<?php endforeach; ?>
											</ul><!--.ul-child-->

										</div><!--.list-child-->
									</div><!--.list-child-wrap-->
									<div class="partner">
										<div class="top-title">
											Partner
										</div>
										<div class="advertising">
											<a href="<?php echo $parent['partner']['link'];?>"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo $parent['partner']['img'];?>"/></a>
										</div>
									</div><!--.partner-->
									<div class="newsletter">
										<div class="top-title">
											Newsletter
										</div>
										<div class="newsletter-detail">
											<span>Sign up for our newsletter to receive exclusive content right to your email!</span>
											<input type="text" placeholder="youremail@example.com">
											<input type="submit" value="Sign Up Now" class="btn btn-primary">
										</div>
									</div><!--.newsletter-->
								</div><!--.step-content-->
							</li>
						<?php endforeach;?>
					</ul>
				</div><!--#nav-->
			</div>
		</div>
	 </div>
</div><!--#wrapper-nav-->