<?php

/**
 * Description of StepMenu
 *
 * @author An Dinh Luan <luanad6365@setacinq.com.vn>
 */
class StepMenu extends CWidget {

	public function run() {
		$step = Yii::app()->request->getParam('step', '');
		if (!$step || $step >= 6) {
			$step = 1;
		}
		$controller = $this->getController();
		$loadFile = new LoaderUtil();
		$loadFile->load('root', array('css/widget/stepMenu.css'));
		$view = 'stepMenu';
		$stepModel = new TStep();
		$data = $stepModel->data;
		$this->render($view, array('data' => $data, 'currentStep'=>$controller->active_parent));
	}

}
