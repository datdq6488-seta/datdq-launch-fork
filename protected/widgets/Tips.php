<?php

/**
 * Description of Tips
 *
 * @author An Dinh Luan <luanad6365@setacinq.com.vn>
 */
class Tips extends CWidget {

	public function run() {
		$data = array(
			'id' => 1,
			'name' => 'Scott Duffy',
			'avatar' => 'scott-avatar.png',
			'description' => 'Scott Duffy twin tornadoes that spun through on Monday night wiped out the town\'s business district.',
			'link' => '#',
			'quote' => '&ldquo;Every single relationship matters - in bussiness and in life.&rdquo;'
		);

		$loadFile = new LoaderUtil();
		$loadFile->load('root', array('css/widget/tips.css'));
		$view = 'tips';
		$this->render($view, array('data' => $data));
	}

}
