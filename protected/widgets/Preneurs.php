<?php

/**
 * Description of Preneurs
 *
 * @author An Dinh Luan <luanad6365@setacinq.com.vn>
 */
class Preneurs extends CWidget {

	public function run() {
		$data = array(
			1 => array(
				'id' => 1,
				'name' => 'Mark Suster',
				'avatar' => 'mark-avatar.png',
				'description' => 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec pellentesque dui dolor, nec ultricies justo ornare.',
				'link' => '#'
			),
			2 => array(
				'id' => 2,
				'name' => 'Ron Klein',
				'avatar' => 'ron-avatar.png',
				'description' => 'Curabitur consequat lectus ut est dictum, mattis accumsan justo pellentesque. Nulla id vehicula metus, ullamcorper.',
				'link' => '#'
			)
		);

		$loadFile = new LoaderUtil();
		$loadFile->load('root', array('css/widget/preneurs.css'));
		$view = 'preneurs';
		$this->render($view, array('data' => $data));
	}

}
