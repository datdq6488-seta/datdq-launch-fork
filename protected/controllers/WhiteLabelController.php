<?php

class WhiteLabelController extends Controller {

    public function actionCreate() {
        $this->layout = '//layouts/user';
        $model = new TUser();
        $model->scenario = 'registerwcaptcha';

        $loadFile = new LoaderUtil();
        $files = array(
            'css/whitelabel.css'
        );
        $loadFile->load('root', $files);

        $this->render('create', array(
            'model' => $model
        ));
    }

    public function actionDashboard() {
        $this->layout = '//layouts/user';

        $loadFile = new LoaderUtil();
        $files = array(
            'css/dashboard.css',
            'css/whitelabel.css'
        );
        $loadFile->load('root', $files);
        $this->render('dashboard', array());
    }

    public function actionConfiguaration() {
        $this->layout = '//layouts/user';

        $loadFile = new LoaderUtil();
        $files = array(
            'css/dashboard.css',
            'css/whitelabel.css',
            'css/dropdown.css',
            'css/configAccount.css',
            'js/STDrop.js',
            'js/configAccount.js',
            'js/whitelabel.configuration.js'
        );
        $loadFile->load('root', $files);
        $this->render('configuaration', array());
    }

}
