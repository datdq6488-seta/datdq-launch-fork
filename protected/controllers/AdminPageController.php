<?php

class AdminPageController extends Controller{
    public function actionPages() {
        $this->layout = '//layouts/user';
        
        $loadFile = new LoaderUtil();        
        $files = array(
            'css/dashboard.css',
            'css/page.css'
        );
        $loadFile->load('root', $files);                        
        $this->render('pages', array());
    }
    
    public function actionPageDetail() {
        $this->layout = '//layouts/user';
        
        $loadFile = new LoaderUtil();        
        $files = array(
            'css/dashboard.css',
            'css/page.css',
            'css/libs/jquery.mCustomScrollbar.css',
            'js/libs/jquery-1.11.1.min.js',
            'js/libs/jquery.mCustomScrollbar.concat.min.js'
        );
        $loadFile->load('root', $files);                        
        $this->render('pageDetail', array());
    }
    
    public function actionPageBuilder() {
        $this->layout = '//layouts/user';
        
        $loadFile = new LoaderUtil();        
        $files = array(
            'css/dashboard.css',
            'css/page.css'            
        );
        $loadFile->load('root', $files);                        
        $this->render('pageBuilder', array());
    }
    
    public function actionTemplateDesigner() {
        $this->layout = '//layouts/user';
        
        $loadFile = new LoaderUtil();        
        $files = array(
            'css/dashboard.css',
            'css/whitelabel.css',
            'css/page.css',
            'css/configAccount.css',
            'js/configAccount.js',
            'js/adminpage.teamplatedesigner.js'
        );
        $loadFile->load('root', $files);                        
        $this->render('templateDesigner', array());
    }
    
    public function actionDashboard() {
        $this->layout = '//layouts/user';
        
        $loadFile = new LoaderUtil();        
        $files = array(
            'css/dashboard.css',            
            'css/whitelabel.css'
        );
        $loadFile->load('root', $files);                        
        $this->render('dashboard', array());
    }
}

