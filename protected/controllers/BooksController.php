<?php

class BooksController extends Controller {

	public function init() {
		parent::init();
		$this->layout = '//layouts/main';
	}

	public function actionIndex() {
		//Load script, css for this page
		$loadFile = new LoaderUtil();
		$files = array(
            'css/libs/dd.css',
            'css/ask.css',
			'css/bookstore.css',
            'js/libs/jquery.dd.js',
			'js/bookstore.js',
		);
		$loadFile->load('root', $files);


		//Default value, first init
		$currentPage = 0;
		$limitPerPage = LaunchConst::LIMIT_PER_PAGE;
		$loadMoreItem = LaunchConst::LOAD_MORE_ITEM;

		//sort option
		$sortBy = array(
			'title' => Yii::t('_yii', 'Title (A-Z)'),
			'published_date' => Yii::t('_yii', 'Published Date'),
			'author' => Yii::t('_yii', 'Author'),
		);
		//Validate sort value
		$selectedSort = Yii::app()->request->getParam('sort', 'title');
		if (!array_key_exists($selectedSort, $sortBy)) {
			$selectedSort = 'title';
		}
		//Get key search
		$keySearch = Yii::app()->request->getParam('key', '');
        //Remove utf-8
        $keySearch = EncoderUtil::fixForPreg($keySearch);
		//Data for render
		$data = array();
		//Calc total page
		$criteria = new CDbCriteria();
		$criteria->addSearchCondition('title', trim($keySearch), true);
        $criteria->addSearchCondition('description', trim($keySearch),true, 'OR');
        $criteria->addSearchCondition('author', trim($keySearch),true, 'OR');
		$criteria->addCondition('published = '.LaunchConst::STATUS_PUBLISHED);
		$criteria->addCondition('type != '.LaunchConst::BOOK_TYPE_SPONSORED);
		$totalItem = TBookStore::model()->count($criteria);
		$data['totalPage'] = ceil($totalItem / $limitPerPage);

		//Init data for render
		$data['currentPage'] = $currentPage;
		$data['loadMoreItem'] = $loadMoreItem;
		$data['sortBy'] = $sortBy;
		$data['selectedSort'] = $selectedSort;
		$data['keySearch'] = $keySearch;

		//Get Top 3 Sponsored item
		$criteria = new CDbCriteria(
			array(
				'select' => 'id, title, description, author, publisher, published_date',
				'order' => 'id DESC',
				'limit' => 3,
			)
		);
		$criteria->addCondition('published = '.LaunchConst::STATUS_PUBLISHED);
		$criteria->addCondition('type = '.LaunchConst::BOOK_TYPE_SPONSORED);
		$modelSponsored = TBookStore::model()->findAll($criteria);
		$data['modelSponsored'] = $modelSponsored;
		
		//Render data to view
		$this->render('index', $data);
	}

	//View more ajax function
	public function actionLoadMore() {
		//Get current page
		$currentPage = Yii::app()->request->getParam('page', 1);
		//sort option
		$sortBy = array(
			'title' => Yii::t('_yii', 'Title (A-Z)'),
			'published_date' => Yii::t('_yii', 'Published Date'),
			'author' => Yii::t('_yii', 'Author'),
		);
		$selectedSort = Yii::app()->request->getParam('sort', 'title');
		//Validate sort value
		if (!array_key_exists($selectedSort, $sortBy)) {
			$selectedSort = 'title';
		}
        if($selectedSort == 'published_date'){
            $selectedSort = 'published_date DESC';
        }
		//Get key search
		$keySearch = Yii::app()->request->getParam('key', '');
        //Remove utf-8
        $keySearch = EncoderUtil::fixForPreg($keySearch);
		$limitPerPage = LaunchConst::LIMIT_PER_PAGE;
		$loadMoreItem = LaunchConst::LOAD_MORE_ITEM;

		//Add criterial for query
		$criteria = new CDbCriteria(
			array(
				'select' => 'id, title, description, author, publisher, published_date',
				'order' => $selectedSort,
				'limit' => $limitPerPage,
				'offset' => ($currentPage - 1) * $limitPerPage,
			)
		);
		$criteria->addSearchCondition('title', trim($keySearch), true);
        $criteria->addSearchCondition('description', trim($keySearch),true, 'OR');
        $criteria->addSearchCondition('author', trim($keySearch),true, 'OR');
		$criteria->addCondition('published = '.LaunchConst::STATUS_PUBLISHED);
		$criteria->addCondition('type != '.LaunchConst::BOOK_TYPE_SPONSORED);
        
		//Find next item to display
		$model = TBookStore::model()->findAll($criteria);
		//Data for render
		$data = array();
		$data['model'] = $model;
		$data['currentPage'] = $currentPage;
		$data['loadMoreItem'] = $loadMoreItem;
		//Handle show or hide "view more" button
		/*
		  $data['displayViewMore'] = true;
		  if (count($model) < $limitPerPage) {
		  $data['displayViewMore'] = false;
		  } else if (count($model) == $limitPerPage) {
		  $totalItem = TBookstore::model()->count();
		  $remain = $totalItem - $limitPerPage * $currentPage;
		  if ($remain <= 0) {
		  $data['displayViewMore'] = false;
		  }
		  }
		 */
		//Render data for ajax
		$this->renderPartial('_item', $data);
		die;
	}

	public function actionDetail($id) {
		if (!$id) {
			$this->redirect(array('index'));
		}
		
		//Load script, css for this page
		$loadFile = new LoaderUtil();
		$files = array(
            'css/ask.css',
            'css/bookstore.css',
            'js/bookstore.js',
            'js/SocialSharer.js'
		);
		$loadFile->load('root', $files);
		
		//Load item data
		$model = TBookStore::model()->findByPk((int) $id);
		//If data doesnt exist, redirect
		if(!$model){
			$this->redirect(array('index'));
		}
		
		//Parse time
		$model->published_date = Yii::app()->dateFormatter->format("MMM dd, yyyy", $model->published_date);
		
		//Get category name
		$model->category_name = '';
		$bookCategory = TBookCategory::model()->findAll();
		if($bookCategory){
			foreach ($bookCategory as $cat ) {
				if( $model->book_category_id === $cat['id']){
					$model->category_name = $cat['title'];
					break;
				}
			}
		}
		
		//Add meta resources
		Yii::app()->clientScript->registerScriptFile('http://w.sharethis.com/button/buttons.js', CClientScript::POS_END);
		$desc = $model->description;
		$image = BASE_URL . "/images/files/book/Book_Item_Details.png";
		$link = BASE_URL . "/books/detail/" . $id;
		$title = $model->title;
		$type = 'article';
		$loadSeo = new SEOUtil();
		$loadSeo->metaSocial($link, $title, $desc, $image, $type);
		
		//Render data
		$data = array();
		$data['model'] = $model;
		$this->render('detail', $data);
	}

	public function actions() {
		// return external action classes, e.g.:
		return array(
			'action1' => 'path.to.ActionClass',
			'action2' => array(
				'class' => 'path.to.AnotherActionClass',
				'propertyName' => 'propertyValue',
			),
		);
	}
}