<?php

class WebinarsController extends Controller {

    public function init() {
        parent::init();
        $this->layout = '//layouts/main';
    }

    public function actionIndex() {
        $loadFile = new LoaderUtil();
        $files = array(
            'css/libs/dd.css',
            'css/ask.css',
            'css/bookstore.css',
            'css/webinar.css',
            'js/libs/jquery.dd.js',
            'js/webinar.js',
        );
        $loadFile->load('root', $files);


        //Default value, first init
        $currentPage = 0;
        $limitPerPage = LaunchConst::LIMIT_PER_PAGE;
        $loadMoreItem = LaunchConst::LOAD_MORE_ITEM;

        //sort option
        $sortBy = array(
            'approved_date' => Yii::t('_yii', 'Date (newest first)'),
            'title' => Yii::t('_yii', 'Title'),
        );
        //Validate sort value
        $selectedSort = Yii::app()->request->getParam('sort', 'approved_date');
        if (!array_key_exists($selectedSort, $sortBy)) {
            $selectedSort = 'approved_date';
        }
        //Get key search
        $keySearch = Yii::app()->request->getParam('key', '');
        //Remove utf-8
        $keySearch = EncoderUtil::fixForPreg($keySearch);
        //Data for render
        $data = array();
        //Calc total page
        $criteria = new CDbCriteria();
        $criteria->addSearchCondition('title', trim($keySearch), true);
        $criteria->addSearchCondition('description', trim($keySearch), true, 'OR');
        $criteria->addCondition('published = ' . LaunchConst::STATUS_PUBLISHED);
        $criteria->addCondition('approved = ' . LaunchConst::STATUS_APPROVED);
        $criteria->addCondition('type != ' . LaunchConst::WEBINAR_TYPE_SPONSORED);
        $totalItem = TWebinar::model()->count($criteria);
        $data['totalPage'] = ceil($totalItem / $limitPerPage);

        //Init data for render
        $data['currentPage'] = $currentPage;
        $data['loadMoreItem'] = $loadMoreItem;
        $data['sortBy'] = $sortBy;
        $data['selectedSort'] = $selectedSort;
        $data['keySearch'] = $keySearch;

        //Get Top 3 Sponsored item
        $criteria = new CDbCriteria(
            array(
            'select' => 'id, title, description, alias, approved_date',
            'order' => 'id DESC',
            'limit' => 3,
            )
        );
        $criteria->addCondition('published = ' . LaunchConst::STATUS_PUBLISHED);
        $criteria->addCondition('approved = ' . LaunchConst::STATUS_APPROVED);
        $criteria->addCondition('type = ' . LaunchConst::WEBINAR_TYPE_SPONSORED);
        $modelSponsored = TWebinar::model()->findAll($criteria);
        $data['modelSponsored'] = $modelSponsored;

        //Render data to view
        $this->render('index', $data);
    }

    //View more ajax function
    public function actionLoadMore() {
        //Get current page
        $currentPage = Yii::app()->request->getParam('page', 1);
        //sort option
        $sortBy = array(
            'approved_date' => Yii::t('_yii', 'Date (newest first)'),
            'title' => Yii::t('_yii', 'Title'),
        );
        $selectedSort = Yii::app()->request->getParam('sort', 'approved_date');
        //Validate sort value
        if (!array_key_exists($selectedSort, $sortBy)) {
            $selectedSort = 'approved_date';
        }
        if ($selectedSort == 'approved_date') {
            $selectedSort = 'approved_date DESC';
        }
        //Get key search
        $keySearch = Yii::app()->request->getParam('key', '');
        //Remove utf-8
        $keySearch = EncoderUtil::fixForPreg($keySearch);
        $limitPerPage = LaunchConst::LIMIT_PER_PAGE;
        $loadMoreItem = LaunchConst::LOAD_MORE_ITEM;

        //Add criterial for query
        $criteria = new CDbCriteria(
            array(
            'select' => 'id, title, description, alias, approved_date',
            'order' => $selectedSort,
            'limit' => $limitPerPage,
            'offset' => ($currentPage - 1) * $limitPerPage,
            )
        );
        $criteria->addSearchCondition('title', trim($keySearch), true);
        $criteria->addSearchCondition('description', trim($keySearch), true, 'OR');
        $criteria->addCondition('published = ' . LaunchConst::STATUS_PUBLISHED);
        $criteria->addCondition('approved = ' . LaunchConst::STATUS_APPROVED);
        $criteria->addCondition('type != ' . LaunchConst::WEBINAR_TYPE_SPONSORED);

        //Find next item to display
        $model = TWebinar::model()->findAll($criteria);
        //Data for render
        $data = array();
        $data['model'] = $model;
        $data['currentPage'] = $currentPage;
        $data['loadMoreItem'] = $loadMoreItem;

        //Render data for ajax
        $this->renderPartial('_item', $data);
        die;
    }

    public function actionDetail($id) {
        if (!$id) {
            $this->redirect(array('index'));
        }

        //Load script, css for this page
        $loadFile = new LoaderUtil();
        $files = array(
            'css/ask.css',
            'css/bookstore.css',
            'css/webinar.css',
            'js/webinar.js',
        );
        $loadFile->load('root', $files);

        //Load item data
        $model = TWebinar::model()->findByPk((int) $id);
        //If data doesnt exist, redirect
        if (!$model) {
            $this->redirect(array('index'));
        }

        //Parse time
        $model->approved_date = Yii::app()->dateFormatter->format("MMM dd, yyyy - h:m a", $model->approved_date);


        //Add meta resources
        $desc = $model->description;
        $image = BASE_URL . "/images/files/webinar/webinar-item.png";
        $link = BASE_URL . "/webinars/detail/" . $id;
        $title = $model->title;
        $type = 'article';
        $loadSeo = new SEOUtil();
        $loadSeo->metaSocial($link, $title, $desc, $image, $type);

        //Render data
        $data = array();
        $data['model'] = $model;
        $this->render('detail', $data);
    }

    public function actions() {
        // return external action classes, e.g.:
        return array(
            'action1' => 'path.to.ActionClass',
            'action2' => array(
                'class' => 'path.to.AnotherActionClass',
                'propertyName' => 'propertyValue',
            ),
        );
    }

}
