<?php

class EventsController extends Controller {

    public function init() {
        parent::init();
        $this->layout = '//layouts/main';
    }

    public function actionIndex() {
        $loadFile = new LoaderUtil();
        $files = array(
            'css/liveevent.css',
            'js/liveevent.js',
        );
        $loadFile->load('root', $files);

        $data = array();
        $this->render('index', $data);
    }

    public function actionLoadEvent() {
        $this->renderPartial('_item');
        die;
    }

    public function actionDetail() {
        Yii::app()->clientScript->registerScriptFile('http://maps.googleapis.com/maps/api/js?sensor=false', CClientScript::POS_END);
        $loadFile = new LoaderUtil();
        $files = array(
            'css/liveevent.css',
            'js/liveevent.js',
            'js/coremap.js',
            'js/eventdetail.js',
        );
        $loadFile->load('root', $files);
        $data = array();
        $this->render('detail', $data);
    }

    public function actions() {
        // return external action classes, e.g.:
        return array(
            'action1' => 'path.to.ActionClass',
            'action2' => array(
                'class' => 'path.to.AnotherActionClass',
                'propertyName' => 'propertyValue',
            ),
        );
    }

    public function actionMap() {
        //Highlight one location
        $mark = Yii::app()->request->getParam('mark', '');
        Yii::app()->clientScript->registerScriptFile('http://maps.googleapis.com/maps/api/js?sensor=false', CClientScript::POS_END);
        $loadFile = new LoaderUtil();
        $files = array(
            'css/liveevent.css',
            'js/liveevent.js',
            'js/coremap.js',
            'js/eventmap.js',
        );
        $loadFile->load('root', $files);
        $data = array();
        $data['mark'] = $mark;
        $this->render('map', $data);
    }

    public function actionGetEventLocation() {
        $type = Yii::app()->request->getParam('type', '');
        $events = array(
            1 => array(
                'name' => 'Live Event: Essentials For Starting A Business 1',
                'time' => 'June 25, 2014 - 10:00 AM EST',
                'location' => 'Los Angeles Convetion Center',
                'lat' => -33.890542,
                'lng' => 151.274856,
                'img' => Yii::app()->request->baseUrl . '/images/files/event/icon-map-event.png'
            ),
            2 => array(
                'name' => 'Live Event: Essentials For Starting A Business 2',
                'time' => 'June 25, 2014 - 10:00 AM EST',
                'location' => 'Los Angeles Convetion Center',
                'lat' => -33.923036,
                'lng' => 151.259052,
                'img' => Yii::app()->request->baseUrl . '/images/files/event/icon-map-event.png'
            ),
            3 => array(
                'name' => 'Live Event: Essentials For Starting A Business 3',
                'time' => 'June 25, 2014 - 10:00 AM EST',
                'location' => 'Los Angeles Convetion Center',
                'lat' => -34.028249,
                'lng' => 151.157507,
                'img' => Yii::app()->request->baseUrl . '/images/files/event/icon-map-event.png'
            ),
            4 => array(
                'name' => 'Live Event: Essentials For Starting A Business 4',
                'time' => 'June 25, 2014 - 10:00 AM EST',
                'location' => 'Los Angeles Convetion Center',
                'lat' => -33.80010128657071,
                'lng' => 151.28747820854187,
                'img' => Yii::app()->request->baseUrl . '/images/files/event/icon-map-event.png'
            ),
            5 => array(
                'name' => 'Live Event: Essentials For Starting A Business 5',
                'time' => 'June 25, 2014 - 10:00 AM EST',
                'location' => 'Los Angeles Convetion Center',
                'lat' => -33.950198,
                'lng' => 151.259302,
                'img' => Yii::app()->request->baseUrl . '/images/files/event/icon-map-event.png'
            )
        );
        $mark = Yii::app()->request->getParam('mark', '');
        if ($mark) {
            $events[$mark]['highlight_info'] = true;
        }
        if ($type == 'detail') {
            $event_detail = array();
            $event_detail[1] = $events[rand(1, 5)];
            $event_detail[1]['disable_info'] = true;
            echo json_encode($event_detail);
        } else {
            echo json_encode($events);
        }
        die;
    }

}
