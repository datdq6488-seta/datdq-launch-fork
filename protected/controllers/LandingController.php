<?php

class LandingController extends Controller {

	public function init() {
		parent::init();
		$this->layout = '//layouts/landing';
	}

	public function actions() {
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha' => array(
				'class' => 'CCaptchaAction',
				'backColor' => 0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page' => array(
				'class' => 'CViewAction',
			),
		);
	}

	public function actionIndex() {
		$this->redirect('landing/parent');
	}

	public function actionParent() {
//		if (Yii::app()->user->isGuest) {
//			$this->redirect(array('user/login'));
//		}
		$loadFile = new LoaderUtil();
		$loadFile->load('root', array('css/landingParent.css'));

		$step = Yii::app()->request->getParam('step', '');
		if (!$step) {
			$step = 1;
		}
		$stepModel = new TStep();
		$data = $stepModel->data[$step];
		if (empty($data)) {
			$step = 1;
			$data = $stepModel->data[$step];
		}
		$this->active_parent = $step;
		//Cut name child step
		$strCut = new StringUtil();
		foreach ($data['child_step'] as $k => $v) {
			$data['child_step'][$k]['name'] = $strCut->substring($v['name'], $length = 40, $replacer = '...', $isStriped = true);
		}
		//Render embed youtube link
		preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $data['intro']['video'], $matches);
		$idYoutube = $matches[1];
		$data['intro']['video'] = "https://www.youtube.com/embed/" . $idYoutube . "?rel=0";
		$this->render('parent', array('data' => $data, 'currentStep' => $step));
	}

	public function actionChild() {
//		if (Yii::app()->user->isGuest) {
//			$this->redirect(array('user/login'));
//		}
		$loadFile = new LoaderUtil();
		$loadFile->load('root', array('css/landingChild.css'));

		$step = Yii::app()->request->getParam('step', '');
		if (!$step) {
			$step = 1;
		}
		$stepModel = new TStep();
		$data = $stepModel->findChild($step);
		if (empty($data)) {
			$step = 1;
			$data = $stepModel->findChild($step);
		}

		$dataParent = $stepModel->data[$data['parent_step']];
		//Render embed youtube link
		preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $dataParent['intro']['video'], $matches);
		$idYoutube = $matches[1];
		$dataParent['intro']['video'] = "https://www.youtube.com/embed/" . $idYoutube . "?rel=0";

		//Render embed youtube link
//		preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $data['intro']['video'], $matches);
//		$idYoutube = $matches[1];
//		$data['intro']['video'] = "https://www.youtube.com/embed/" . $idYoutube . "?rel=0";
		$this->active_parent = $data['parent_step'];
		global $user;
		$this->render('child', array('data' => $data, 'dataParent' => $dataParent, 'user' => $user));
	}

}
