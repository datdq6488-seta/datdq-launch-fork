<?php

/**
 * Description of VideoController
 *
 * @author An Dinh Luan <luanad6365@setacinq.com.vn>
 */
class VideosController extends Controller {
	public function init() {
		parent::init();
		$loadFile = new LoaderUtil();
		$files = array(
			'css/video.css',
			'js/video.js',
		);
		$loadFile->load('root', $files);
        $this->layout = '//layouts/main';
	}

	public function actionIndex() {

		$data = array();
		$catModel = new TCategory();
		$categories = $catModel->data;
		$data['categories'] = $categories;

		$tagModel = new TTag();
		$tags = $tagModel->data;
		$data['tags'] = $tags;

		$videoModel = new TVideo();
		$videos = $videoModel->data;
		$data['videos'] = $videos;

		$this->render('index', $data);
	}

	public function actionDetail() {
		
		$videoId = Yii::app()->request->getParam('id', 1);
		
		$data = array();
		
		$videoModel = new TVideo();
		$videos = $videoModel->data;
		$videoData = $videoModel->data[$videoId];
		
		//Render embed youtube link
		preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $videoData['link'], $matches);
		$idYoutube = $matches[1];
		$videoData['link'] = "https://www.youtube.com/embed/" . $idYoutube . "?rel=0";
		
		$data['videos'] = $videos;
		$data['videoData'] = $videoData;
		$this->render('detail', $data);
	}

	public function actions() {
		// return external action classes, e.g.:
		return array(
			'action1' => 'path.to.ActionClass',
			'action2' => array(
				'class' => 'path.to.AnotherActionClass',
				'propertyName' => 'propertyValue',
			),
		);
	}

}
