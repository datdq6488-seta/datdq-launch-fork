<?php

class ExpertsController extends Controller {
    public function init() {
        parent::init();
        $this->layout = '//layouts/main';
    }
    
    public function actionIndex() {
        $loadFile = new LoaderUtil();        
        $files = array(
            'css/libs/dd.css',
            'css/ask.css',
            'css/bookstore.css',            
            'js/libs/jquery.dd.js',
            'js/experts.js',
        );
        $loadFile->load('root', $files);
        $data = array();
        $this->render('index', $data);
    }    

    public function actionDetail() {
        $loadFile = new LoaderUtil();        
        $files = array(
            'css/ask.css'
        );
        $loadFile->load('root', $files);
        $data = array();
        $this->render('detail', $data);
    }

}
