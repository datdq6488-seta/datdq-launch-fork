<?php

class TenantController extends Controller
{
    // as tenant maintenance is mostly handled by the app’s staff and not the tenants
    // themselves, it only uses the TTenant model (tbl_tenant).
    // if a tenant_owner is allowed to change something (e.g. business_name),
    // create a separate AccountController where she can interact with the
    // VTenant model (vw_tenant)
 
    public function actionCreate()
    {
        $model=new TTenant;
        if(isset($_POST['TTenant'])) {
            $model->attributes=$_POST['TTenant'];
            // search for an available MySQL username
            $tntdbu = bin2hex(Yii::app()->getSecurityManager()->generateRandomBytes('4'));
            // Yii 1.1.14 only, there are other ways to generate a random 8 character hex number
            while (Common::checkMySQLUserExists($tntdbu)) {
               $tntdbu = bin2hex(Yii::app()->getSecurityManager()->generateRandomBytes('4'));
            }
            $model->dbu = $tntdbu;
            $model->e_dbpwd = bin2hex(Yii::app()->getSecurityManager()->generateRandomBytes('4'));
            // or some other clever way to assign a random password
            if($model->save()) {
                $this->redirect(array('view','id'=>$model->id));
            }
        }
        $this->render('create',array(
            'model'=>$model,
        ));
    }
}