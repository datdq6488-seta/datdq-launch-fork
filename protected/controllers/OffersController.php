<?php

class OffersController extends Controller {

    public function init() {
        parent::init();
        $this->layout = '//layouts/main';
    }

    public function actionIndex() {
        $loadFile = new LoaderUtil();
        //$model = new TOffers();
        $files = array(
            'css/offers.css',
            'js/offers.js',
        );
        $loadFile->load('root', $files);
        $data = array();
        $this->render('index', $data);
    }
}
