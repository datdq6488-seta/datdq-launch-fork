<?php

class UserController extends Controller
{
    // Load ajax check email exits;
    public function actionCheckobj()
    {
        $objVal = isset($_POST['value']) ? $_POST['value'] : null;
        if ($objVal) {
            if (strpos($objVal, '@') !== false) {
                $cond = array('email' => $objVal);
            } else {
                $cond = array('username' => $objVal);
            }

            $users = User::model()->findByAttributes($cond);
            if ($users) {
                $this->response(new Result(true, null, $users));
            }else{
                throw new Exception('User is not exists.');
            }
        }else
            throw new Exception('Invalide data.');
    }
}
