<?php

class ServiceModule extends CWebModule {

    public function init() {
        $this->setImport(array(
            'user.models.*',
            'service.models.*',
            'service.components.*',
        ));
    }

    public function beforeControllerAction($controller, $action) {
        if (parent::beforeControllerAction($controller, $action)) {
            return true;
        }
        else
            return false;
    }

}
