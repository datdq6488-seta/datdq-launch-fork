<?php

class Controller extends BaseController {

    private $_result;

    public function runAction($action) {
        header('Content-type: application/json');
        try {
            parent::runAction($action);
            echo CJSON::encode($this->_result);
        } catch (Exception $ex) {
            echo CJSON::encode(new Result(false, $ex->getMessage(), $ex));
        }
    }

    /**
     * Trả về kết quả cho service
     * @param Result $result kết quả của service
     */
    public function response($result) {
        $this->_result = $result;
    }

    public function filters() {
        $actionId = $this->getId();
        if (Yii::app()->params['outputCache'] && in_array($actionId, array('default'))) {
            $params = array();
            foreach ($_GET as $key => $val) {
                $params[] = $key;
            }
            return array(
                array(
                    'COutputCache',
                    'duration' => Yii::app()->params['serviceTimeCache'],
                    'varyByParam' => $params,
                    'varyByRoute' => true,
                    'requestTypes' => array('GET'),
                ),
            );
        }
    }

}