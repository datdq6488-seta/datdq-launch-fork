<?php
/* @var $this UserController */

$this->pageTitle = Yii::app()->name . ' - ' . UserModule::t('Create a New Account');
$this->breadcrumbs = array(
    UserModule::t('Create a New Account'),
);
?>
<div id="create-user-account">
    <h3><?php echo UserModule::t('Create a New Account'); ?></h3>
    <?php if (Yii::app()->user->hasFlash('registration')) { ?>
        <div class="success">
            <?php echo Yii::app()->user->getFlash('registration'); ?>
        </div>
    <?php } else { ?>
        <div class="form">
            <div class="well">
                <hr class="hr">

                <?php
                $form = $this->beginWidget('UActiveForm', array(
                    'id' => 'create-user-form',
//                    'enableAjaxValidation' => true,
                    'disableAjaxValidationAttributes' => array('RegistrationForm_verifyCode'),
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                    ),
                    'htmlOptions' => array('enctype' => 'multipart/form-data'),
                ));
                ?>

                <div class="field-row">
                    <?php echo $form->labelEx($model, 'name'); ?>
                    <?php echo $form->textField($model, 'name'); ?>
                    <!--                --><?php //echo $form->error($model, 'name'); ?>
                </div>

                <div class="field-row">
                    <?php echo $form->labelEx($model, 'company'); ?>
                    <?php echo $form->textField($model, 'company'); ?>
                    <!--                --><?php //echo $form->error($model, 'company'); ?>
                </div>

                <div class="field-row">
                    <?php echo $form->labelEx($model, 'email'); ?>
                    <?php echo $form->textField($model, 'email'); ?>
                    <!--                --><?php //echo $form->error($model, 'email'); ?>
                </div>

                <div class="field-row">
                    <?php echo $form->labelEx($model, 'username'); ?>
                    <?php echo $form->textField($model, 'username'); ?>
                    <!--                --><?php //echo $form->error($model, 'username'); ?>
                </div>

                <div class="field-row">
                    <?php echo $form->labelEx($model, 'password'); ?>
                    <?php echo $form->passwordField($model, 'password'); ?>
                    <!--                --><?php //echo $form->error($model, 'password'); ?>
                </div>

                <div class="field-row">
                    <?php echo $form->labelEx($model, 'verifyPassword'); ?>
                    <?php echo $form->passwordField($model, 'verifyPassword'); ?>
                    <!--                --><?php //echo $form->error($model, 'verifyPassword'); ?>
                </div>

                <div class="field-row account-type">
                    <?php
                    $accountType = array('5' => UserModule::t('Entrepreneur'), '2' => UserModule::t('Content Provider'));
                    if (isset($_POST['Usergroups']['id']))
                        $usergroup->id = $_POST['Usergroups']['id'];
                    else
                        $usergroup->id = 5;

                    ?>
                    <?php echo $form->labelEx($model, 'accountType'); ?>
                    <?php echo $form->radioButtonList($usergroup, 'id', $accountType, array('separator' => ' ')); ?>
                </div>
                <hr class="hr">
                <div class="user-field-captcha">
                    <?php
                    $this->widget('application.extensions.recaptcha.EReCaptcha',
                        array('attribute' => 'validateCapt',
                            'theme' => 'red', 'language' => 'en_US',
                            'publicKey' => Yii::app()->params['recaptcha']['publicKey']));
                    echo $form->error($model, 'validateCapt');
                    ?>

                </div>
                <div class="user-field-terms">
                    <?php echo $form->checkBox($model, 'user_terms'); ?>
                    <?php echo CHtml::label('I agree to the <a href="#">terms and conditions</a>', 'RegistrationForm_user_terms'); ?>
                    <div class="clearfix"></div>
                    <!--                --><?php //echo $form->error($model, 'user_terms'); ?>
                </div>
                <p class="user-read-terms">
                    <?php echo UserModule::t('By clicking on \'I Agree\' you are confirming that you have read, understood, and accept the Terms of Service Privacy Policy'); ?>
                </p>

                <div class="form-actions">
                    <?php echo CHtml::submitButton(UserModule::t('Sign Up'), array('class' => 'btn btn-primary', 'id' => 'registerId')); ?>
                </div>

                <label class="bold"><?php echo UserModule::t('Sign Up with social'); ?></label>

                <div class="field-row social">
                    <a class="js-openpopup facebook"
                       href="<?php echo Yii::app()->getBaseUrl(true) ?>/oauth/oauth?provider=facebook"><span><?php echo UserModule::t('Sign up with Facebook') ?></span></a>
                    <a class="js-openpopup twitter"
                       href="<?php echo Yii::app()->getBaseUrl(true) ?>/oauth/oauth?provider=twitter"><span><?php echo UserModule::t('Sign up with Twitter') ?></span></a>
                    <a class="js-openpopup google"
                       href="<?php echo Yii::app()->getBaseUrl(true) ?>/oauth/oauth/index?provider=google"><span><?php echo UserModule::t('Sign up with Google') ?></span></a>
                </div>

                <?php $this->endWidget(); ?>
            </div>
        </div>
    <?php } ?>

</div>