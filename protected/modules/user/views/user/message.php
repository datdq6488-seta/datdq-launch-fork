<?php
/* @var $this UserController */

$this->pageTitle = Yii::app()->name . ' - ' . UserModule::t('Create a New Account');
$this->breadcrumbs = array(
    UserModule::t('Create a New Account'),
);
?>
<div id="create-user-account">
    <h3><?php echo $title; ?></h3>
    <div class="success">
        <?php echo $content; ?>
    </div>
</div>