<?php
/* @var $this UserController */
/* @var $model LoginForm */
/* @var $form CActiveForm */

$this->pageTitle = Yii::app()->name . ' - ' . UserModule::t('Login');
$this->breadcrumbs = array(
    UserModule::t('Login'),
);
?>
<div id="login-wrapper">
    <h3><?php echo UserModule::t('Log In to Launch!'); ?></h3>
    <?php if(Yii::app()->user->hasFlash('loginMessage')){ ?>
    <div class="message-success"><?php echo Yii::app()->user->getFlash('loginMessage'); ?></div>
    <?php } ?>
    <div class="form">
        <div class="hint-wrapper">
            <label class="bold"><?php echo UserModule::t('Don\'t have an account?'); ?></label>
            <label id="faint">
                <?php echo UserModule::t('Signing up is quick and easy!'); ?>
                <a href="<?php echo Yii::app()->getBaseUrl(true); ?>/user/registration"
                   class="create-link"><?php echo UserModule::t('Create an account'); ?></a>
            </label>
        </div>
        <div class="well">

            <?php echo CHtml::beginForm(); ?>

            <?php echo CHtml::error($model, 'username', array('class' => 'alert alert-error error-validate hight-light')); ?>
            <?php echo CHtml::error($model, 'password', array('class' => 'alert alert-error error-validate hight-light')); ?>
            <?php echo CHtml::error($model, 'status', array('class' => 'alert alert-error error-validate hight-light')); ?>

            <div class="field-row">
                <?php echo CHtml::activeLabelEx($model,'username', array('class' => 'bold')); ?>
                <?php echo CHtml::activeTextField($model,'username', array('tabindex' => 1)) ?>
            </div>

            <div class="field-row">
                <?php echo CHtml::activeLabelEx($model,'password', array('class' => 'password-label bold')); ?>
                <?php echo Chtml::link(UserModule::t('Forgot Password?'), Yii::app()->getBaseUrl(true) . '/user/forgot', array('class' => 'forgot-link')); ?>
                <?php echo CHtml::activePasswordField($model,'password', array('tabindex' => 2)) ?>
            </div>

            <div class="user-field-terms rememberMe">
                <?php echo CHtml::activeCheckBox($model,'rememberMe', array('checked' => true, 'tabindex' => 3)); ?>
                <?php echo CHtml::activeLabelEx($model,'rememberMe'); ?>
            </div>
            <p class="user-read-terms">
            </p>

            <div class="form-actions">
                <?php echo CHtml::submitButton(UserModule::t('Log In'), array('class' => 'btn btn-primary')); ?>
            </div>
            <?php echo CHtml::endForm(); ?>

            <label class="bold"><?php echo UserModule::t('Log In with social'); ?></label>

            <div class="field-row social">
                <a class="js-openpopup facebook"
                   href="<?php echo Yii::app()->getBaseUrl(true) ?>/oauth/oauth/index?provider=facebook"><span><?php echo UserModule::t('Log in with Facebook') ?></span></a>
                <a class="js-openpopup twitter"
                   href="<?php echo Yii::app()->getBaseUrl(true) ?>/oauth/oauth/index?provider=twitter"><span><?php echo UserModule::t('Log in with Twitter') ?></span></a>
                <a class="js-openpopup google"
                   href="<?php echo Yii::app()->getBaseUrl(true) ?>/oauth/oauth/index?provider=google"><span><?php echo UserModule::t('Log in with Google') ?></span></a>
            </div>
        </div>
        <!-- well -->
    </div>
    <!-- form -->
</div>