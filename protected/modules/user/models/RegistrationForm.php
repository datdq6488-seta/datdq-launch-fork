<?php
/**
 * RegistrationForm class.
 * RegistrationForm is the data structure for keeping
 * user registration form data. It is used by the 'registration' action of 'UserController'.
 */
class RegistrationForm extends User
{
    public $verifyPassword;
    public $verifyCode;
    public $company;
    public $user_terms;
    public $validateCapt;
    public $accountType;

    public function rules()
    {
        $rules = array(
            array('name, username, password, verifyPassword, email', 'required'),
            array('username', 'length', 'max' => 20, 'min' => 3, 'message' => UserModule::t("Incorrect username (length between 3 and 20 characters).")),
            array('password', 'length', 'max' => 128, 'min' => 4, 'message' => UserModule::t("Incorrect password (minimal length 4 symbols).")),
            array('email', 'email'),
            array('username', 'unique', 'message' => UserModule::t("This user's name already exists.")),
            array('email', 'unique', 'message' => UserModule::t("This user's email address already exists.")),
            //array('verifyPassword', 'compare', 'compareAttribute'=>'password', 'message' => UserModule::t("Retype Password is incorrect.")),
            array('username', 'match', 'pattern' => '/^[A-Za-z0-9_.,\-]+$/u', 'message' => UserModule::t("Incorrect symbols (A-z0-9).")),
            array('user_terms', 'checkTerms'),
        );

        if (!(isset($_POST['ajax']) && $_POST['ajax'] === 'create-user-form')) {
            array_push($rules, array('validateCapt',
                'application.extensions.recaptcha.EReCaptchaValidator',
                'privateKey' => Yii::app()->params['recaptcha']['privateKey'],
                'on' => 'registerwcaptcha',
                'message' => 'Captcha is incorrect.'
            ));
        }

        array_push($rules, array('verifyPassword', 'compare', 'compareAttribute' => 'password', 'message' => UserModule::t("Retype Password is incorrect.")));
        return $rules;
    }

    public function checkTerms(){
        if(!$this->user_terms){
            $this->addError('user_terms', 'You have to agree to the term and conditions.');
            return false;
        }
        return true;
    }

    public function attributeLabels()
    {
        return array(
            'name' => UserModule::t("Full Name"),
            'email' => UserModule::t("Email Address"),
            'verifyPassword' => UserModule::t("Confirm Password"),
            'accountType' => UserModule::t("Account Type"),
            'company' => UserModule::t('Company Name <span>(optional)</span>'),
        );
    }

}