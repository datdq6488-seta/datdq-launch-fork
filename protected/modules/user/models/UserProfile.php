<?php

/**
 * This is the model class for table "tbl_users".
 *
 * The followings are the available columns in table 'tbl_users'
 * */
class UserProfile extends CActiveRecord
{
    public $keep_private;
    public $oldAvatar;
    public $company_type;
    public $full_name;
    public $company;
    public $bio_user;
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'tbl_users';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('company_type, keep_private', 'numerical', 'integerOnly'=>true),
            array('full_name, company', 'length', 'max'=>255),
            array('bio_user', 'length', 'max'=>1500),
            array('full_name', 'length', 'min'=>2),
            array('id, company, full_name', 'safe', 'on'=>'search'),
            array('company, full_name','required'),
        );
    }

    public function relations()
    {
        return array(
//            'tenant' => array(self::BELONGS_TO, 'TTenant', 'tenant_id'), // tenant table
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'full_name' => UserModule::t('Public Name'),
            'company' => UserModule::t('Company Name <span>(optional)</span>'),
            'email' => UserModule::t('Email Address'),
            'company_type' => UserModule::t('Company Type'),
            'bio_user' => UserModule::t('Bio <span>(optional)</span>'),
            'keep_private' => UserModule::t('Keep Private'),
            'avatar' => UserModule::t('Profile Photo'),
            'created' => UserModule::t('Created'),
            'validation'=>UserModule::t( 'Enter both words separated by a space: '),
        );
    }
    public function afterFind() {
        parent::afterFind();
        $this->oldAvatar = $this->avatar;
    }

    public function afterDelete() {
        $this->deleteImagem();
        return parent::afterDelete();
    }
    public function avatarUpdate($imageName) {
        $uploadPath = Yii::app()->params['uploadPath'];
        if(is_object($this->avatar)) {
            $pathMove = $uploadPath.'/'.$imageName;
            $this->avatar->saveAs($pathMove);
            if(!empty($this->oldAvatar)) {
                $delete = Yii::app()->params['uploadPath'].'/'.$this->oldAvatar;
                if(file_exists($delete)) unlink($delete);
            }
        }
        if(empty($this->avatar) && !empty($this->oldAvatar)) $this->avatar = $this->oldAvatar;
    }
    public function deleteImagem() {
        $imagem = $this->avatar;
        return unlink(Yii::app()->params['uploadPath'].'/'.$imagem);
    }
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TUser the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
