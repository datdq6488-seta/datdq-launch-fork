<?php

class ResetPasswordController extends Controller
{
	public $defaultAction = 'resetpassword';

    public function init()
    {
        parent::init();
        $this->layout = '//layouts/main';
    }

	/**
	 * Displays the login page
	 */
    public function actionResetPassword() {
        $token = Yii::app()->request->getParam('token');
        $md5Email = Yii::app()->request->getParam('tmp');
        $valid = FALSE;
        //Find existed token
        $userKey = UserKey::model()->findByAttributes(array('key' => $token));
        if (is_object($userKey) && $userKey->user_id) {//If existed
            //Find existed user
            $user = User::model()->findByPk($userKey->user_id);
            //If existed user and matching email with param tmp in link
            if (is_object($user) && $user->email && md5($user->email) == $md5Email) {
                //Check expired time
                if (time() <= $userKey->expire_time) {
                    $valid = TRUE;
                } else {
                    $mes = UserModule::t('Token is expired.');
                }
            } else { // if not
                $mes = UserModule::t('Invalid Token.');
            }
        } else { //Token does not valid.
            $mes = UserModule::t('Invalid Token.');
        }

        if (!$valid) {
            //Yii::app()->user->setState('message', $mes);
            //$this->redirect(array('user/notice'));
            Yii::app()->user->setFlash('invalid', UserModule::t($mes));
        }

        global $tenant;
        $loadFile = new LoaderUtil();
        $loadFile->load('root', array('css/resetPw.css'));
        $model = new ResetPwForm;
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'reset-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['ResetPwForm'])) {
            $model->setAttributes($_POST['ResetPwForm']);
            if ($valid) {
                if ($model->validate()) {
                    //Update pw and Auto login
                    $user->password = Yii::app()->getModule('user')->encrypting($model->password);
                    $user->resetCount += 1;
                    $user->update();
                    $modelLogin = new LoginForm;
                    $modelLogin->username = $user->username;
                    $modelLogin->password = $model->password;
                    if ($modelLogin->login()) {
                        //Delete token for user
                        $userKey->delete();

                        /*$domain = TTenant::model()->find(array(
                            'select' => 'domain',
                            'condition' => 'id=:tenant_id',
                            'params' => array(':tenant_id' => $tenant['tenant_id']),
                        ));
                        $this->redirect('http://' . $domain->domain);*/
                        $this->redirect($this->getBaseUrl());
                    } else {
                        $this->redirect(array('user/login'));
                    }
                }
            }
        }
        $this->render('/user/reset', array('model' => $model));
    }

}