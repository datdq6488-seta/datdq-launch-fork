<?php
/**
 * @Package:
 * @Author: mrthanhdo
 * @Date: 7/28/14
 * @Time: 2:00 AM
 */
class ForgotController extends Controller
{
    public $defaultAction = 'forgot';

    public function init()
    {
        parent::init();
        $this->layout = '//layouts/main';
    }

    public function actionForgot()
    {
        global $tenant;
        $loadFile = new LoaderUtil();
        $loadFile->load('root', array('css/forgotPw.css'));
        $model = new ForgotPwForm;
        $model->scenario = 'resetwcaptcha';
        if (isset($_POST['ForgotPwForm'])) {
            $model->setAttributes($_POST['ForgotPwForm']);
            if ($model->validate()) {
                $cond = array(
//                    'tenant_id' => $tenant['tenant_id'],
//                    'tenant_dbu' => $tenant['dbu']
                );
                if ($model->email) {
                    $cond = array_merge($cond, array('email' => $model->email));
                }
                if ($model->username) {
                    $cond = array_merge($cond, array('username' => $model->username));
                }
                //Find user reset Pw
                $user = User::model()->findByAttributes($cond);
                //Find existed token of user in DB
                $userKey = UserKey::model()->findByAttributes(array('user_id' => $user->id));
                if (!is_object($userKey)) { //If existed -> override, if not ->add new
                    $userKey = new UserKey();
                }
                $token = $userKey->generateToken($user->email);
                $userKey->user_id = $user->id;
                $userKey->key = $token;
                $userKey->created = time();
                $userKey->consume_time = Yii::app()->params['expried_token'];
                $userKey->expire_time = $userKey->created + $userKey->consume_time * 24 * 60 * 60;
                //$userKey->expire_time = $userKey->created + 2 * 60;
                $userKey->type = 1;
                if ($userKey->save()) {
                    //Send mail to user
                    $activation_url = $this->createAbsoluteUrl('/user/resetPassword/resetPassword', array('token' => $token, 'tmp' => md5($user->email)));
                    $activation_url = CHtml::link('Click Here!', $activation_url);
                    $fromName = 'Launch Admin';
                    $replyemail = NULL;
                    $subject = UserModule::t('Confirm reset password');
                    $params = array(
                        'message' => '', 'activation_url' => $activation_url
                    );
                    $view = 'forgot';
                    $layout = 'main1';
                    BaseEmail::sendHtmlEmail($user->email, $fromName, $replyemail, $subject, $params, $view, $layout);

                    Yii::app()->user->setFlash('success', UserModule::t('Your password has been reset successfully. Please check your input email for more detail'));
//                    $this->redirect(array('user/notice'));
                }
            }
        }
        $this->render('/user/forgot', array('model' => $model));
    }
}
