<?php

class RegistrationController extends Controller
{
    public $defaultAction = 'registration';

    public function init()
    {
        parent::init();
        $this->layout = '//layouts/main';
    }

    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
        );
    }

    /**
     * Registration user
     */
    public function actionRegistration()
    {
        LoaderUtil::load('root', array('css/userAccount.css', 'js/userAccount.js'));
        $model = new RegistrationForm;
        $model->scenario = 'registerwcaptcha';

        //Bỏ profile
//        $profile = new Profile;
//        $profile->regMode = true;
        // ajax validator
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'create-user-form') {
            echo UActiveForm::validate(array($model /*, $profile*/));
            Yii::app()->end();
        }

//        $model->attributes = $_POST['RegistrationForm'];
        if (Yii::app()->user->id) {
            $this->redirect(Yii::app()->controller->module->profileUrl);
        } else {
            if (isset($_POST['RegistrationForm'])) {
                $model->attributes = $_POST['RegistrationForm'];
//                $profile->attributes = ((isset($_POST['Profile']) ? $_POST['Profile'] : array()));

                if (CActiveForm::validate($model) == '[]') {
//                if (!CActiveForm::validate($model)/* && $profile->validate()*/) {
                    $soucePassword = $model->password;
                    $model->activation = UserModule::encrypting(microtime() . $model->password);
                    $model->password = UserModule::encrypting($model->password);
                    $model->verifyPassword = UserModule::encrypting($model->verifyPassword);
                    $model->superuser = 0;
                    $model->status = ((Yii::app()->controller->module->activeAfterRegister) ? User::STATUS_ACTIVE : User::STATUS_NOACTIVE);
                    $model->registerDate = date('Y-m-d H:i:s');

                    $transaction = Yii::app()->db->beginTransaction();
                    $result1 = $model->save(false);
                    if ($result1) {
                        //Insert table profiles
//                        $profile->user_id = $model->id;
//                        $profile->full_name = $model->name;
//                        $profile->save(false);

                        //Insert user_usergroup_map
                        $userGroupMap = new UserUsergroupMap();
                        $userGroupMap->user_id = $model->id;
                        $userGroupMap->group_id = intval($_POST['Usergroups']['id']);
                        $result2 = $userGroupMap->save();

                        if (!$result1 || !$result2) {
                            $transaction->rollback();
                        } else {
                            //Verify send mail
                            $sentMail = true;
                            if (Yii::app()->controller->module->sendActivationMail) {
                                try {
                                    $activation_url = $this->createAbsoluteUrl('/user/activation/activation', array("activation" => $model->activation, "email" => $model->email));
                                    //UserModule::sendMail($model->email, UserModule::t("You registered from {site_name}", array('{site_name}' => Yii::app()->name)), UserModule::t("Please activate you account go to {activation_url}", array('{activation_url}' => $activation_url)));
                                    $fromName = 'Launch Admin';
                                    $replyemail = NULL;
                                    $subject = UserModule::t($model->name . ', welcome to Launch');
                                    $params = array(
                                        'message' => '',
                                        'activation_url' => $activation_url,
                                        'full_name' => $model->name,
                                        'email' => $model->email,
                                    );
                                    $view = 'register';
                                    $layout = 'main1';
                                    BaseEmail::sendHtmlEmail($model->email, $fromName, $replyemail, $subject, $params, $view, $layout);
                                } catch (Exception $e) {
                                    $sentMail = false;
                                    Yii::app()->user->setFlash('registration', UserModule::t("Error! Can not send email active. Retry or contact to admin system."));
                                }
                            }
                        }
                        if ($result1 && $result2 && $sentMail) {
                            $transaction->commit();
                        }else{
                            $transaction->rollback();
                        }

                        if ((Yii::app()->controller->module->loginNotActiv || (Yii::app()->controller->module->activeAfterRegister && Yii::app()->controller->module->sendActivationMail == false)) && Yii::app()->controller->module->autoLogin) {
                            $identity = new UserIdentity($model->username, $soucePassword);
                            $identity->authenticate();
                            Yii::app()->user->login($identity, 0);
                            $this->redirect(Yii::app()->controller->module->returnUrl);
                        } else {
                            if (!Yii::app()->controller->module->activeAfterRegister && !Yii::app()->controller->module->sendActivationMail) {
                                Yii::app()->user->setFlash('registration', UserModule::t("Thank you for your registration. Contact Admin to activate your account."));
                            } elseif (Yii::app()->controller->module->activeAfterRegister && Yii::app()->controller->module->sendActivationMail == false) {
                                Yii::app()->user->setFlash('registration', UserModule::t("Thank you for your registration. Please {{login}}.", array('{{login}}' => CHtml::link(UserModule::t('Login'), Yii::app()->controller->module->loginUrl))));
                            } elseif (Yii::app()->controller->module->loginNotActiv) {
                                Yii::app()->user->setFlash('registration', UserModule::t("Thank you for your registration. Please check your email or login."));
                            } else {
                                Yii::app()->user->setFlash('registration', UserModule::t("Thank you for your registration. Please check your email."));
                            }
                            $this->refresh();
                        }
                    } else {
                        Yii::app()->user->setFlash('registration', UserModule::t("Error! Please retry."));
                    }
                } /*else $profile->validate();*/
            }
            $this->render('/user/registration', array('model' => $model, 'usergroup' => new Usergroups()));
        }
    }
}