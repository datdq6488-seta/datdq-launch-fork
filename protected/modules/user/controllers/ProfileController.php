<?php

class ProfileController extends Controller
{
    public $defaultAction = 'profile';

    /**
     * @var CActiveRecord the currently loaded data model instance.
     */
    private $_model;

    /**
     * Shows a particular model.
     */
    public function init()
    {
        parent::init();
        $this->layout = '//layouts/user';
    }

    /*public function actionProfile()
     {
         LoaderUtil::load('root', array('css/configAccount.css', 'css/dropdown.css', 'js/STDrop.js', 'js/configAccount.js', 'js/STMaxlength.js'));
         $model = $this->loadUser();
         var_dump($model->attributes);die;
         $comType = array(
             0 => UserModule::t('Choose one'),
             1 => UserModule::t('Information Technology'),
             2 => UserModule::t('Education'),
         );
         $this->render('profile',array(
             'model'=>$model,
             'profile'=>$model->profile,
             'comType'=> $comType,
             'avatar' => '',
         ));
     }*/
    public function actionProfile()
    {
        global $user;
        $users = $this->loadUser();
        $model = new UserProfile();
        $model->attributes = Profile::model()->find("user_id=".$users->id)->attributes;

        $loadFile = new LoaderUtil();
        $loadFile->load('root', array('css/configAccount.css', 'css/dropdown.css', 'js/STDrop.js', 'js/configAccount.js', 'js/STMaxlength.js'));
        $companyType = array(
            0 => Yii::t('_yii', '--- Choose one ---'),
            1 => Yii::t('_yii', 'Business and Industrial Markets'),
            2 => Yii::t('_yii', 'Infomation Technology'),
            3 => Yii::t('_yii', 'Education'),
        );
        if (isset($_POST['UserProfile'])) {
            $params = $_POST['UserProfile'];
            $model->attributes = $_POST['UserProfile'];
            //$avatar=CUploadedFile::getInstance($model,'avatar');
            if ($model->validate()) {
                $userId = $users->id;
                $user = Profile::model()->find("user_id=".$userId);
                $user->full_name = $params['full_name'];
                $user->company = $params['company'];
                $user->company_type = $params['company_type'];
                $user->bio_user = $params['bio_user'];
                $user->keep_private = $params['keep_private'];
                if ($params['avatar'])
                    $user->avatar = $params['avatar'];
                if ($user->update()) {
                    Yii::app()->user->setFlash('success', Yii::t('_yii', 'Your profile changed successfully!'));
                }
            }
        }
        $objThumb = new ThumbBases();
        if ($user['avatar']) {
            $pathAvatar = 'avatar/' . $user['avatar'];
        } else {
            $pathAvatar = 'avatar/default_profile_picture.jpg';
        }
        $avatar = $objThumb->Thumbs($pathAvatar, 125, 125);
        $data = array(
            'users' => $users,
            'model' => $model,
            'comType' => $companyType,
            'avatar' => $avatar,
        );
        $this->render('profile', $data);
    }


    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     */
    public function actionEdit()
    {
        $model = $this->loadUser();
        $profile = $model->profile;

        // ajax validator
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'profile-form') {
            echo UActiveForm::validate(array($model, $profile));
            Yii::app()->end();
        }

        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            $profile->attributes = $_POST['Profile'];

            if ($model->validate() && $profile->validate()) {
                $model->save();
                $profile->save();
                Yii::app()->user->updateSession();
                Yii::app()->user->setFlash('profileMessage', UserModule::t("Changes is saved."));
                $this->redirect(array('/user/profile'));
            } else $profile->validate();
        }

        $this->render('edit', array(
            'model' => $model,
            'profile' => $profile,
        ));
    }

    /**
     * Change password
     */
    public function actionChangepassword()
    {
        $model = new UserChangePassword;
        if (Yii::app()->user->id) {

            // ajax validator
            if (isset($_POST['ajax']) && $_POST['ajax'] === 'changepassword-form') {
                echo UActiveForm::validate($model);
                Yii::app()->end();
            }

            if (isset($_POST['UserChangePassword'])) {
                $model->attributes = $_POST['UserChangePassword'];
                if ($model->validate()) {
                    $new_password = User::model()->notsafe()->findbyPk(Yii::app()->user->id);
                    $new_password->password = UserModule::encrypting($model->password);
                    $new_password->activkey = UserModule::encrypting(microtime() . $model->password);
                    $new_password->save();
                    Yii::app()->user->setFlash('profileMessage', UserModule::t("New password is saved."));
                    $this->redirect(array("profile"));
                }
            }
            $this->render('changepassword', array('model' => $model));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the primary key value. Defaults to null, meaning using the 'id' GET variable
     */
    public function loadUser()
    {
        if ($this->_model === null) {
            if (Yii::app()->user->id)
                $this->_model = Yii::app()->controller->module->user();
            if ($this->_model === null)
                $this->redirect(Yii::app()->controller->module->loginUrl);
        }
        return $this->_model;
    }
}