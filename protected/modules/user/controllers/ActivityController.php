<?php
/**
 * @Package:
 * @Author: mrthanhdo
 * @Date: 7/28/14
 * @Time: 2:00 AM
 */
class ActivityController extends Controller
{
    public $defaultAction = 'activity';

    public function init(){
        parent::init();
        $this->layout = '//layouts/user';
    }

    public function actionActivity() {
        $this->layout = '//layouts/user';

        $loadFile = new LoaderUtil();
        $files = array(
            'css/dashboard.css',
            'css/activity.css'
        );
        $loadFile->load('root', $files);
        $this->render('/user/activity', array());
    }
}
