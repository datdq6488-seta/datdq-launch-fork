<?php
/**
 * @Package:
 * @Author: mrthanhdo
 * @Date: 7/28/14
 * @Time: 2:00 AM
 */
class DashboardController extends Controller
{
    public $defaultAction = 'dashboard';

    public function init(){
        parent::init();
        $this->layout = '//layouts/user';
    }

    public function actionDashboard() {
        $this->layout = '//layouts/user';

        $loadFile = new LoaderUtil();
        $files = array(
            'css/dashboard.css'
        );
        $loadFile->load('root', $files);
        $stepModel = new TStep();
        $steps = $stepModel->data;
        $this->render('/user/dashboard', array('steps' => $steps));
    }
}
