<?php

class LoginController extends Controller
{
    public $defaultAction = 'login';

    public function init()
    {
        parent::init();
        $this->layout = '//layouts/main';
    }

    /**
     * Displays the login page
     */
    public function actionLogin()
    {
        LoaderUtil::load('root', array('css/loginAccount.css', 'js/loginAccount.js'));
        if (Yii::app()->user->isGuest) {
            $model = new UserLogin;
            // collect user input data
            if (isset($_POST['UserLogin'])) {
                $model->attributes = $_POST['UserLogin'];
                // validate user input and redirect to previous page if valid
                if ($model->validate()) {
                    $this->lastViset();
                    if (Yii::app()->user->returnUrl == '/index.php')
                        $this->redirect(Yii::app()->controller->module->returnUrl);
                    else
                        $this->redirect(Yii::app()->user->returnUrl);
                }
            }
            // display the login form
            $this->render('/user/login', array('model' => $model));
        } else
            $this->redirect(Yii::app()->controller->module->returnUrl);
    }

    private function lastViset()
    {
        $lastVisit = User::model()->notsafe()->findByPk(Yii::app()->user->id);
        $lastVisit->lastvisitDate = new CDbExpression('NOW()');
        $lastVisit->save(false);
    }
}