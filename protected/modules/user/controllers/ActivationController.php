<?php

class ActivationController extends Controller
{
    public $defaultAction = 'activation';

    public function init()
    {
        parent::init();
        $this->layout = '//layouts/main';
    }

    /**
     * Activation user account
     */
    public function actionActivation()
    {
        $email = $_GET['email'];
        $activation = $_GET['activation'];
        if ($email && $activation) {
            $find = User::model()->notsafe()->findByAttributes(array('email' => $email));
            if (isset($find) && $find->status) {
                $this->redirect($this->getBaseUrl().Yii::app()->controller->module->loginUrl[0]);
//                $this->render('/user/message', array('title' => UserModule::t("User activation"), 'content' => UserModule::t("You account is active. Click <a href=\"". $this->getBaseUrl().Yii::app()->controller->module->loginUrl[0] ."\">here</a> to login.")));
            } elseif (isset($find->activation) && ($find->activation == $activation)) {
                $find->activation = UserModule::encrypting(microtime());
                $find->status = 1;
                $find->save();
                $this->render('/user/message', array('title' => UserModule::t("User activation"), 'content' => UserModule::t("You account is activated.")));
            } else {
                $this->render('/user/message', array('title' => UserModule::t("User activation"), 'content' => UserModule::t("Incorrect activation URL.")));
            }
        } else {
            $this->render('/user/message', array('title' => UserModule::t("User activation"), 'content' => UserModule::t("Incorrect activation URL.")));
        }
    }

}