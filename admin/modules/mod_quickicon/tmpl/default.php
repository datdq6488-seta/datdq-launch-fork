<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  mod_quickicon
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
$menu = ModQuickIconHelper::groupButtons($buttons);
$html = JHtml::_('links.linksgroups', ModQuickIconHelper::groupButtons($buttons));
?>
<div class="main_menu">
<?php foreach ($menu AS $k => $v) {
        ?>
        <?php if (JText::_($k) != '') { ?> <h4 class="menu_title"><?=JText::_($k);?></h4> <?php } ?>
        <ul>
            <?php
            for ($i=0;$i<count($v);$i++) {
                ?>
                    <li <?php if (JRequest::getVar('option', '') == $v[$i]['option']) echo 'class="m_active"'; ?>>
                        <a href="<?=$v[$i]['link'];?>"><?=$v[$i]['text'];?></a>
                    </li>
                <?php
            }
            ?>
        </ul>
        <?php
    ?>
        
    <?php
} ?>
</div>