<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  mod_quickicon
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Helper for mod_quickicon
 *
 * @package     Joomla.Administrator
 * @subpackage  mod_quickicon
 * @since       1.6
 */
abstract class ModQuickIconHelper
{
	/**
	 * Stack to hold buttons
	 *
	 * @since   1.6
	 */
	protected static $buttons = array();

	/**
	 * Helper method to return button list.
	 *
	 * This method returns the array by reference so it can be
	 * used to add custom buttons or remove default ones.
	 *
	 * @param   JObject  $params  The module parameters.
	 *
	 * @return  array  An array of buttons
	 *
	 * @since   1.6
	 */
	public static function &getButtons($params)
	{
		$key = (string) $params;

		if (!isset(self::$buttons[$key]))
		{
			$context = $params->get('context', 'mod_quickicon');

			if ($context == 'mod_quickicon')
			{
				// Load mod_quickicon language file in case this method is called before rendering the module
				JFactory::getLanguage()->load('mod_quickicon');

				self::$buttons[$key] = array(
					array(
						'link' => JRoute::_('index.php?option=com_dashboard'),
                        'option' => 'com_dashboard',
						'text' => JText::_('MOD_QUICKICON_DASHBOARD'),
						'group' => 'MOD_QUICKICON_UNKNOWN'
					),
                    array(
						'link' => JRoute::_('index.php?option=com_basic'),
                        'option' => 'com_basic',
						//'image' => 'list-view',
						//'icon' => 'header/icon-48-menumgr.png',
						'text' => JText::_('MOD_QUICKICON_COMBASIC'),
						'access' => array('core.manage', 'com_basic'),
						'group' => 'MOD_QUICKICON_UNKNOWN'
					),
					array(
						'link' => JRoute::_('index.php?option=com_page'),
                        'option' => 'com_pages',
						'text' => JText::_('MOD_QUICKICON_PAGES'),
						'group' => 'MOD_QUICKICON_UNKNOWN'
					),
					array(
						'link' => JRoute::_('index.php?option=com_step'),
                        'option' => 'com_steps',
						'text' => JText::_('MOD_QUICKICON_STEPS'),
						'group' => 'MOD_QUICKICON_UNKNOWN'
					),
					array(
						'link' => JRoute::_('index.php?option=com_quote'),
                        'option' => 'com_quotes',
						'text' => JText::_('MOD_QUICKICON_QUOTES'),
						'group' => 'MOD_QUICKICON_UNKNOWN'
					),
					array(
						'link' => JRoute::_('index.php?option=com_offers'),
                        'option' => 'com_special_offers',
						'text' => JText::_('MOD_QUICKICON_SPECIAL_OFFERS'),
						'group' => 'MOD_QUICKICON_UNKNOWN'
					),
					array(
						'link' => JRoute::_('index.php?option=com_book'),
                        'option' => 'com_books',
						'text' => JText::_('MOD_QUICKICON_BOOKS'),
						'group' => 'MOD_QUICKICON_UNKNOWN'
					),
					array(
						'link' => JRoute::_('index.php?option=com_referrals'),
                        'option' => 'com_referrals',
						'text' => JText::_('MOD_QUICKICON_REFERRALS'),
						'group' => 'MOD_QUICKICON_UNKNOWN'
					),
					array(
						'link' => JRoute::_('index.php?option=com_entrepreneur'),
                        'option' => 'com_entrepreneur',
						'text' => JText::_('MOD_QUICKICON_ENTREPRENEURS'),
						'group' => 'MOD_QUICKICON_ACCESS'
					),
					array(
						'link' => JRoute::_('index.php?option=com_provider'),
                        'option' => 'com_provider',
						'text' => JText::_('MOD_QUICKICON_CONTENT_PROVIDERS'),
						'group' => 'MOD_QUICKICON_ACCESS'
					),
					array(
						'link' => JRoute::_('index.php?option=com_partners'),
                        'option' => 'com_partners',
						'text' => JText::_('MOD_QUICKICON_PARTNERS'),
						'group' => 'MOD_QUICKICON_ACCESS'
					),
					array(
						'link' => JRoute::_('index.php?option=com_webinar'),
                        'option' => 'com_webinars',
						'text' => JText::_('MOD_QUICKICON_WEBINARS'),
						'group' => 'MOD_QUICKICON_EVENTS'
					),array(
						'link' => JRoute::_('index.php?option=com_live_events'),
                        'option' => 'com_live_events',
						'text' => JText::_('MOD_QUICKICON_LIVE_EVENTS'),
						'group' => 'MOD_QUICKICON_EVENTS'
					),array(
						'link' => JRoute::_('index.php?option=com_emails'),
                        'option' => 'com_emails',
						'text' => JText::_('MOD_QUICKICON_EMAIL_MANAGER'),
						'group' => 'MOD_QUICKICON_ADMIN'
					),array(
						'link' => JRoute::_('index.php?option=com_notifications'),
                        'option' => 'com_notifications',
						'text' => JText::_('MOD_QUICKICON_NOTIFICATIONS_MANAGER'),
						'group' => 'MOD_QUICKICON_ADMIN'
					),array(
						'link' => JRoute::_('index.php?option=com_template_designs'),
                        'option' => 'com_template_designs',
						'text' => JText::_('MOD_QUICKICON_TEMPLATE_DESIGN'),
						'group' => 'MOD_QUICKICON_ADMIN'
					),array(
						'link' => JRoute::_('index.php?option=com_menu_links'),
                        'option' => 'com_menu_links',
						'text' => JText::_('MOD_QUICKICON_MENU_LINKS'),
						'group' => 'MOD_QUICKICON_ADMIN'
					),array(
						'link' => JRoute::_('index.php?option=com_access_manager'),
                        'option' => 'com_access_manager',
						'text' => JText::_('MOD_QUICKICON_ACCESS_MANAGER'),
						'group' => 'MOD_QUICKICON_ADMIN'
					),
				);
			}
			else
			{
				self::$buttons[$key] = array();
			}

			// Include buttons defined by published quickicon plugins
			//JPluginHelper::importPlugin('quickicon');
			$app = JFactory::getApplication();
			$arrays = (array) $app->triggerEvent('onGetIcons', array($context));

			foreach ($arrays as $response)
			{
				foreach ($response as $icon)
				{
					$default = array(
						'link' => null,
						'image' => 'cog',
						'text' => null,
						'access' => true,
						'group' => 'MOD_QUICKICON_EXTENSIONS'
					);
					$icon = array_merge($default, $icon);

					if (!is_null($icon['link']) && !is_null($icon['text']))
					{
						self::$buttons[$key][] = $icon;
					}
				}
			}
		}

		return self::$buttons[$key];
	}

	/**
	 * Classifies the $buttons by group
	 *
	 * @param   array  $buttons  The buttons
	 *
	 * @return  array  The buttons sorted by groups
	 *
	 * @since   3.2
	 */
	public static function groupButtons($buttons)
	{
		$groupedButtons = array();

		foreach ($buttons as $button)
		{
			$groupedButtons[$button['group']][] = $button;
		}
		return $groupedButtons;
	}

	/**
	 * Get the alternate title for the module
	 *
	 * @param   JObject  $params  The module parameters.
	 * @param   JObject  $module  The module.
	 *
	 * @return  string	The alternate title for the module.
	 */
	public static function getTitle($params, $module)
	{
		$key = $params->get('context', 'mod_quickicon') . '_title';

		if (JFactory::getLanguage()->hasKey($key))
		{
			return JText::_($key);
		}
		else
		{
			return $module->title;
		}
	}
}
