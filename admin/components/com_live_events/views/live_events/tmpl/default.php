<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_cpanel
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
//Get custom field
JFormHelper::addFieldPath('components/com_books/models/fields');
$custom = JFormHelper::loadFieldType('Custom', false);
?>
<script type="text/javascript">
function toDetails(option_link) {
    jQuery('#overlay_bg, #overlay_content').show();
    //send the ajax request to the server
	jQuery.ajax({
		type: 'POST',
		url: option_link,
		dataType : "json",
		data: '',
		success: function(data,textStatus,jqXHR)
		{
            for (var x in data) {
                jQuery('input[name="'+x+'"]').val(data[x]);                
            }
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{
			alert("\ntextStatus: '" + textStatus + "'\nerrorThrown: '" + errorThrown + "'\nresponseText:\n" + XMLHttpRequest.responseText);
		}
	});
}
function toRemove(ind, option_link) {
    //send the ajax request to the server
	jQuery.ajax({
		type: 'POST',
		url: option_link,
		dataType : "json",
		data: '',
		success: function(data,textStatus,jqXHR)
		{
            jQuery('tr.row'+ind).remove();
            console.log(data);
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{
			alert("\ntextStatus: '" + textStatus + "'\nerrorThrown: '" + errorThrown + "'\nresponseText:\n" + XMLHttpRequest.responseText);
		}
	});
}

</script>
<div class="warper">
    <div class="w_header">
        <h3 id="title_header">Live Events</h3>
        <?=$custom->getAddButton();?>
    </div>
    <div class="w_content">
        <div class="warper">
            <div class="w_header">
                <h4 class="list_header">Live Events List</h4>
                <div id="filter_search" class="btn-wrapper input-append">
        			<?=$custom->getContentSearch();?>
        		</div>
                <span class="title_filter">Search: &nbsp;&nbsp;&nbsp; </span>
                <div class="styled-select">
                    <?=$custom->getSelect('w_select', '', array(1=>1, 2=>2, 3=>3));?>
                </div>
                <span class="title_filter">
                    Results per page: &nbsp;&nbsp;&nbsp;
                </span>
            </div>
            <div class="m_content">
                <table class="table table-striped" id="itemList" style="position: relative;">
    				<thead>
    					<tr>
    						<th style="width: 30%;" class="title">
    				            Title
    						</th>
    						<th style="" class="nowrap hidden-phone">
    				            Date
    						</th>
        					<th class="nowrap hidden-phone">
                                Approved
        					</th>
                            <th class="nowrap hidden-phone">
                                Created By
        					</th>
    						<th style="width: 22%;" class="nowrap hidden-phone">
                                Actions
    						</th>
    					</tr>
    				</thead>
    				<tbody class="ui-sortable">
                        <?php
                        for ($i=0;$i<15;$i++) {
                            ?>
                            <tr class="row<?=$i;?> dndlist-sortable">
                                <td>
                                    <?=$custom->getToDetails('index.php?option='.JRequest::getVar('option', '').'&task=getdata&id='.$i.'&tmpl=component', 'Launching a Business '.rand(0,100000));?>
                                </td>
                                <td><?=date('m/d/y');?></td>
                                <td><?php if ($i%2 == 1) echo 'Yes'; else if ($i%3 == 1) echo 'Pending'; else echo 'No';  ?></td>
                                <td>Scott Duffy</td>
                                <td>
                                    <?php if ($i%2 == 1) echo ''; else if ($i%3 == 1) echo '<a>Approve</a> | <a>Reject</a> | '; else echo '<a>Approve</a> | ';  ?>
                                    <?=$custom->getToDetails('index.php?option='.JRequest::getVar('option', '').'&task=getdata&id='.$i.'&tmpl=component', 'Edit');?>
                                    <?php if ($i%3 !== 1 OR $i%2 == 1) echo ' | '.$custom->getToRemove($i ,'index.php?option='.JRequest::getVar('option', '').'&task=removedata&id='.$i.'&tmpl=component'); ?>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        
                    </tbody>
    			</table>
            </div>
        </div>
        <div id="content_pagination" class="pagination pagination-toolbar">
            <?php echo $this->pagination->getPagesLinks(); ?>
        </div>
    </div>
</div>

<div id="overlay_bg" style="display: none;"></div>
<div id="overlay_content" style="display: none;">
    Need GD
</div>
