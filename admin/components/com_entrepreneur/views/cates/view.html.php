<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_entrepreneur
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Tags view class for the Tags package.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_entrepreneur
 * @since       3.1
 */
class EntrepreneurViewCates extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 */
	public function display($tpl = null)
	{
		$this->state		= $this->get('State');
		$this->items		= $this->get('Items');
		$this->pagination	= $this->get('Pagination');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   3.1
	 */
	protected function addToolbar()
	{
		$state	= $this->get('State');
		$canDo	= JHelperContent::getActions('com_entrepreneur');
		$user	= JFactory::getUser();

		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');

		JToolbarHelper::title(JText::_('COM_ENTREPRENEUR_MANAGER_ENTREPRENEUR'));

		if ($canDo->get('core.create'))
		{
            //$bar->appendButton( 'NewPopup', 'add-new', JText::_('COM_OFFER_MANAGER_ADD'), 'index.php?option=com_entrepreneur&view=entrepreneur&layout=edit&tmpl=component', 550, 400, false, false );
            // Instantiate a new JLayoutFile instance and render the batch button
			$layout = new JLayoutFile('joomla.toolbar.aepopup');

			$dhtml = $layout->render(array('title' => 'Add', 'url'=> 'index.php?option=com_entrepreneur&view=cate&layout=edit&tmpl=component', 'type' => 'add'));
			$bar->appendButton('Custom', $dhtml);
		}
        
        //add link back to entrepreneur
		$layout = new JLayoutFile('joomla.links.linkcustom');
		$dhtml = $layout->render(array('target' => '', 'link'=> 'index.php?option=com_entrepreneur', 'title' => '', 'text' => JText::_('JLAUNCH_BACK')));
		$bar->appendButton('Custom', $dhtml);
        
        if ($state->get('filter.published') == -2 && $canDo->get('core.delete'))
		{
			//JToolbarHelper::deleteList('', 'tags.delete', 'JTOOLBAR_EMPTY_TRASH');
		}
		elseif ($canDo->get('core.edit.state'))
		{
			//JToolbarHelper::trash('tags.trash');
		}
	}

	/**
	 * Returns an array of fields the table can be sorted by
	 *
	 * @return  array  Array containing the field name to sort by as the key and display text as value
	 *
	 * @since   3.0
	 */
	protected function getSortFields()
	{
		return array(
			'a.title' => JText::_('JGLOBAL_TITLE'),
            'a.state' => JText::_('JSTATUS'),
			'a.access' => JText::_('JGRID_HEADING_ACCESS'),
			'a.id' => JText::_('JGRID_HEADING_ID')
		);
	}
}
