<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_entrepreneur
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Tags view class for the Tags package.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_entrepreneur
 * @since       3.1
 */
class EntrepreneurViewEntrepreneurs extends JViewLegacy {

    protected $items;
    protected $pagination;
    protected $state;

    /**
     * Display the view
     */
    public function display($tpl = null) {
        $this->state = $this->get('State');
        $this->items = $this->get('Items');
        $this->pagination = $this->get('Pagination');
        $this->canDo = JHelperContent::getActions('com_users');

        // Check for errors.
        if (count($errors = $this->get('Errors'))) {
            JError::raiseError(500, implode("\n", $errors));
            return false;
        }

        $this->addToolbar();
        $this->sidebar = JHtmlSidebar::render();
        parent::display($tpl);
    }

    /**
     * Add the page title and toolbar.
     *
     * @since   3.1
     */
    protected function addToolbar() {
        $state = $this->get('State');
        $canDo = JHelperContent::getActions('com_entrepreneur');
        $user = JFactory::getUser();

        // Get the toolbar object instance
        $bar = JToolBar::getInstance('toolbar');

        JToolbarHelper::title(JText::_('COM_ENTREPRENEUR_MANAGER_ENTREPRENEUR'));

        if ($canDo->get('core.create')) {
            // Instantiate a new JLayoutFile instance and render the batch button
            $layout = new JLayoutFile('joomla.toolbar.aepopup');

            $dhtml = $layout->render(array('title' => JText::_('JLAUNCH_ADD'), 'url' => 'index.php?option=com_entrepreneur&view=entrepreneur&layout=edit&tmpl=component', 'type' => 'add'));
            $bar->appendButton('Custom', $dhtml);
        }

        if ($state->get('filter.published') == -2 && $canDo->get('core.delete')) {
            //JToolbarHelper::deleteList('', 'tags.delete', 'JTOOLBAR_EMPTY_TRASH');
        } elseif ($canDo->get('core.edit.state')) {
            //JToolbarHelper::trash('tags.trash');
        }

        JHtmlSidebar::setAction('index.php?option=com_tags&view=tags');

        // Build the active state filter options.
        $options = array();
        $options[] = JHtml::_('select.option', '0', 'JLAUNCH_FILTER_ACTIVE');
        $options[] = JHtml::_('select.option', '1', 'JLAUNCH_FILTER_BLOCKED');

        JHtmlSidebar::addFilter(
            JText::_('JLAUNCH_FILTER_LABEL'), 'filter_active', JHtml::_('select.options', $options, 'value', 'text', $this->state->get('filter.active'), true)
        );
    }

    /**
     * Returns an array of fields the table can be sorted by
     *
     * @return  array  Array containing the field name to sort by as the key and display text as value
     *
     * @since   3.0
     */
    protected function getSortFields() {
        return array(
            'a.name' => JText::_('JLAUNCH_SORT_NAME'),
            'a.block' => JText::_('JLAUNCH_SORT_STATUS'),
            'a.registerDate' => JText::_('JLAUNCH_SORT_CREATED_DATE')
        );
    }

}
