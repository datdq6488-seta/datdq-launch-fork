<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_entrepreneur
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
JHtml::_('behavior.tabstate');

$input = JFactory::getApplication()->input;

if (!JFactory::getUser()->authorise('core.manage', 'com_entrepreneur'))
{
	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
}

$task = $input->get('task');

$controller	= JControllerLegacy::getInstance('Entrepreneur');
$controller->execute($input->get('task'));
$controller->redirect();
