<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_basic
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the component HTML helpers.
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.dddropdown', 'select');

$app = JFactory::getApplication();
$user = JFactory::getUser();
$userId = $user->get('id');
$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));
$trashed = $this->state->get('filter.published') == -2 ? true : false;
?>
<form action="<?php echo JRoute::_('index.php?option=com_basic&view=basics');?>" method="post" name="adminForm" id="adminForm">
    <div class="w_content">
        <div class="warper">
            <div class="inner_content_ad">
                <div class="row">                
                    <div class="col-xs-5 col-sm-5 col-md-4 clearfix">
                        <h4 class="title_for_box"><?php echo JText::_('JLAUNCH_CATE_LIST'); ?></h4>
                    </div>
                    <div class="col-xs-7 col-sm-5 col-md-8 clearfix">
                        <div class="form-inline pull-right m_top" role="form">
                            <div class="form-group">
                                <label class="inline_filter_bar" for=""><?php echo JText::_('JLAUNCH_RESULTS_PER_PAGE'); ?>: </label>                    
                            </div>
                            <div class="form-group group_m_right">
                                <?php echo $this->pagination->getLimitBox(); ?>           
                            </div>  
                            
                            <div class="form-group">
                                <label class="inline_filter_bar" for="exampleInputEmail2"><?php echo JText::_('JLAUNCH_SEARCH'); ?>:</label>
                            </div>
                            <div class="form-group group_m_right">
                                
                            </div>  
                                                              
                        </div>
                    </div>                                
                </div>
            </div>

            <div class="m_content">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped tbl_admin" id="itemList" style="position: relative;">
                            <thead>
                                <tr>
                                    <th style="width: 45%;" class="title">
                    		            <?php echo JText::_('JLAUNCH_NAME');?>
                    				</th>
                    				<th style="width: 25%;" class="nowrap hidden-phone">
                    		            <?php echo JText::_('JLAUNCH_AUTHOR');?>
                    				</th>
                    				<th class="nowrap hidden-phone">
                                        <?php echo JText::_('JLAUNCH_PUBLISHED');?>
                    				</th>
                    				<th style="width: 20%;" class="nowrap hidden-phone">
                                        <?php echo JText::_('JLAUNCH_ACTIONS');?>
                    				</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
            				foreach ($this->items as $i => $item) :
            					$canEdit    = $user->authorise('core.edit',       'com_basic');
            					$canChange  = $user->authorise('core.edit.state', 'com_basic');            					
            					?>
        						<tr class="row<?php echo $i % 2; ?>">
                                    <td class="hidden center hidden-phone">
                						<?php echo JHtml::_('grid.id', $i, $item->id); ?>
                					</td>
        							<td>
        								<?php if ($canEdit || $canEditOwn) : ?>
                                        <?php
                                            // Instantiate a new JLayoutFile instance and render the batch button
                                            $layout = new JLayoutFile('joomla.toolbar.aepopup');
    
                                            $dhtml = $layout->render(array('title' => $this->escape($item->title), 'url' => '#', 'type' => ''));
                                            echo $dhtml;
                                        ?>
        								<?php else : ?>
        									<?php echo $this->escape($item->title); ?>
        								<?php endif; ?>
        							</td>
            						<td class="small hidden-phone">
            							<?php echo $this->escape($item->email); ?>
            						</td>
            						<td class="small nowrap hidden-phone">
                                        <?php echo $this->escape($item->state); ?>
        							</td>
        							<td class="center hidden-phone">
                                        <?php if ($canEdit || $canEditOwn) : ?>
                                        <?php
                                            // Instantiate a new JLayoutFile instance and render the batch button
                                			$layout = new JLayoutFile('joomla.toolbar.aepopup');
                                
                                			$dhtml = $layout->render(array('title' => JText::_('JLAUNCH_DETAILS'), 'url'=> 'index.php?option=com_basic&task=basic.edit&id='.$item->id.'&tmpl=component', 'type' => 'edit'));
                                            echo $dhtml;
                                        ?>
        								<?php else : ?>
        									<?php echo JText::_('JLAUNCH_DETAILS');?> 
        								<?php endif; ?>
                                        |
                                        <?php if ($canEdit || $canEditOwn || $trashed) : ?>
                                        <?php
                                            // Instantiate a new JLayoutFile instance and render the batch button
                                			$layout = new JLayoutFile('joomla.links.linkcustom');
                                
                                			$dhtml = $layout->render(array('target' => '', 'link'=> 'javascript:void(0);', 'onclick' => 'return listItemTask(\'cb'.$i.'\',\'basics.trash\')', 'title' => '', 'text' => JText::_('JLAUNCH_REMOVE')));
                                            echo $dhtml;
                                        ?>
        								<?php else : ?>
        									<?php echo JText::_('JLAUNCH_REMOVE');?>
        								<?php endif; ?>
        							</td>
        						</tr>
            				<?php endforeach; ?>
        				</tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-5 col-sm-5 col-md-5">
                
            </div>
            <div class="col-xs-7 col-sm-7 col-md-7">
                <div class="form-inline pull-right">
                    <div class="form-group pagination pagination-toolbar">
                        <?php echo $this->pagination->getListFooter(); ?>
                    </div>
                </div>
            </div>
            
        </div>       

        <div class="clearfix"></div>
    </div>
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
    <input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
    <?php echo JHtml::_('form.token'); ?>
</form>