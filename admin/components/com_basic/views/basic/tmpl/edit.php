<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_basic
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
JHtml::_('formbehavior.dddropdown', 'select');
// Create shortcut to parameters.
$params = $this->state->get('params');
$params = $params->toArray();
?>

<script type="text/javascript">
	Joomla.submitbutton = function(task)
    {
        if (task == 'basic.cancel') {
            jQuery('#overlay_bg, #overlay_content').hide();
            return false;
        }
        if (document.formvalidator.isValid(document.id('item-form'))) {
            Joomla.submitform(task, document.getElementById('item-form'));
        }
    }
</script>

<div class="col-md-8">
    <div id="for_entre" class="wrap_content_ad none_border">
        <form action="<?php echo JRoute::_('index.php?option=com_entrepreneur&layout=edit&id=' . (int) $this->item->id); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="item-form" class="form-validate">
            <?php echo $this->form->renderField('title'); ?>
            <?php echo $this->form->renderField('email'); ?>
            
            <div style="display: none;">
                <input type="hidden" name="task" value="" />
                <?php echo $this->form->renderField('id'); ?>
                <?php echo $this->form->renderField('published'); ?>
                <?php echo $this->form->renderField('access'); ?>
                <?php echo $this->form->renderField('metadesc'); ?>
                <?php echo $this->form->renderField('metakey'); ?>
                <?php echo $this->form->renderField('alias'); ?>
            	<?php echo JHtml::_('form.token'); ?>
            </div>
        </form>
    </div>
</div>
