<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_referral
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * HTML View class for the Tags component
 *
 * @package     Joomla.Administrator
 * @subpackage  com_referral
 * @since       3.1
 */
class ReferralViewReferral extends JViewLegacy
{
	protected $form;

	protected $item;

	protected $state;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  mixed  A string if successful, otherwise a Error object.
	 */
	public function display($tpl = null)
	{
		$this->form  = $this->get('Form');
		$this->item  = $this->get('Item');
		$this->state = $this->get('State');
		$this->canDo = JHelperContent::getActions('com_referral');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode("\n", $errors));

			return false;
		}

		$this->addToolbar();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since  3.1
	 *
	 * @return void
	 */
	protected function addToolbar()
	{
		$user		= JFactory::getUser();
		$userId		= $user->get('id');
        $bar = JToolbar::getInstance('toolbar');

		$isNew		= ($this->item->id == 0);
        
		// Need to load the menu language file as mod_menu hasn't been loaded yet.
		$lang = JFactory::getLanguage();
			$lang->load('com_referral', JPATH_BASE, null, false, true)
		||	$lang->load('com_referral', JPATH_ADMINISTRATOR . '/components/com_referral', null, false, true);


		// Get the results for each action.
		$canDo = $this->canDo;
        
        $title = JText::_('COM_REFERRAL_BASE_' . ($isNew ? 'ADD' : 'EDIT') . '_TITLE');

        // Prepare the toolbar.
        JToolbarHelper::title($title, 'referral referral-' . ($isNew ? 'add' : 'edit') . ($isNew ? 'add' : 'edit'));
        
        $bar->appendButton('NewCancel', 'cancel', 'JTOOLBAR_CANCEL', 'referral.cancel', false);
        
		// For new records, check the create permission.
		if ($isNew)
		{
            $bar->appendButton('NewStandard', 'save', 'JTOOLBAR_APPLY', 'referral.save', false);
		}
		// If not checked out, can save the item.
		elseif (($canDo->get('core.edit') || ($canDo->get('core.edit.own') && $this->item->created_user_id == $userId)))
		{
			$bar->appendButton('NewStandard', 'save', 'JTOOLBAR_APPLY', 'referral.save', false);
		}		
	}
}
