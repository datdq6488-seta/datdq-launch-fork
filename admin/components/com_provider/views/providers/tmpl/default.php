<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_provider
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the component HTML helpers.
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.dddropdown', 'select');

$app		= JFactory::getApplication();
$user		= JFactory::getUser();
$userId		= $user->get('id');
$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));
$trashed	= $this->state->get('filter.published') == -2 ? true : false;
$sortFields = $this->getSortFields();
?>
<script type="text/javascript">
	Joomla.orderTable = function() {
		table = document.getElementById("sortTable");
		direction = document.getElementById("directionTable");
		order = table.options[table.selectedIndex].value;
		if (order != '<?php echo $listOrder; ?>')
		{
			dirn = 'asc';
		} else {
			dirn = direction.options[direction.selectedIndex].value;
		}
		Joomla.tableOrdering(order, dirn, '');
	}
</script>
<form action="<?php echo JRoute::_('index.php?option=com_provider&view=providers');?>" method="post" name="adminForm" id="adminForm">
    <div class="w_content">
        <div class="warper">
            <div class="inner_content_ad">
                <div class="row">                
                    <div class="col-xs-5 col-sm-5 col-md-4 clearfix">
                        <h4 class="title_for_box"><?php echo JText::_('COM_PROVIDER_LIST'); ?></h4>
                    </div>
                    <div class="col-xs-7 col-sm-5 col-md-8 clearfix">
                        <div class="form-inline pull-right m_top" role="form">
                            <div class="form-group">
                                <label class="inline_filter_bar" for="exampleInputEmail2"><?php echo JText::_('JLAUNCH_FILTER_BY_LABEL'); ?>:</label>
                            </div>
                            <div class="form-group group_m_right">
                                <?php if (!empty($this->sidebar)): ?>
                                    <div class="styled-select">
                                        <?php echo $this->sidebar; ?>
                                    </div>
                                <?php endif; ?>                        
                            </div>
                            <div class="form-group">
                                <label class="inline_filter_bar" for=""><?php echo JText::_('JLAUNCH_SORT_LABEL'); ?>:</label>
                            </div>
                            <div class="form-group">
                                <select name="sortTable" id="sortTable" class="input-medium" onchange="Joomla.orderTable()">                                
                                    <?php echo JHtml::_('select.options', $sortFields, 'value', 'text', $listOrder); ?>
                                </select>
                            </div>
                        </div>
                    </div>                                
                </div>
            </div>

            <div class="m_content">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped" id="itemList" style="position: relative;">
                		<thead>
                			<tr>
                				<th style="width: 30%;" class="title">
                		            <?php echo JText::_('JLAUNCH_NAME');?>
                				</th>
                				<th style="width: 25%;" class="nowrap hidden-phone">
                		            <?php echo JText::_('JLAUNCH_COMPANY');?>
                				</th>
                				<th class="nowrap hidden-phone">
                                    <?php echo JText::_('JLAUNCH_STATUS');?>
                				</th>
                				<th style="width: 30%;" class="nowrap hidden-phone">
                                    <?php echo JText::_('JLAUNCH_ACTIONS');?>
                				</th>
                			</tr>
                		</thead>
                        <tbody>
                            <?php
            				foreach ($this->items as $i => $item) :
            					$canEdit    = $user->authorise('core.edit',       'com_provider');
            					$canChange  = $user->authorise('core.edit.state', 'com_provider');
                    
                    			// If this group is super admin and this user is not super admin, $canEdit is false
                    			if ((!$user->authorise('core.admin')) && JAccess::check($item->id, 'core.admin'))
                    			{
                    				$canEdit   = false;
                    				$canChange = false;
                    			}
            					?>
        						<tr class="row<?php echo $i % 2; ?>">
                                    <td class="hidden center hidden-phone">
                						<?php echo JHtml::_('grid.id', $i, $item->id); ?>
                					</td>
        							<td>
        								<?php if ($canEdit || $canEditOwn) : ?>
                                        <?php
                                            // Instantiate a new JLayoutFile instance and render the batch button
                                			$layout = new JLayoutFile('joomla.toolbar.aepopup');
                                
                                			$dhtml = $layout->render(array('title' => $this->escape($item->name), 'url'=> 'index.php?option=com_provider&task=provider.edit&tmpl=component&id='.$item->id, 'type' => 'edit'));
                                            echo $dhtml;
                                        ?>
        								<?php else : ?>
        									<?php echo $this->escape($item->name); ?>
        								<?php endif; ?>
        							</td>
            						<td class="hidden-phone">
            							<?php
                                        if ($item->params != '') {
                                            $entrep = unserialize($item->params);
                                            echo $entrep['company_name'];
                                        }
                                        ?>
            						</td>
            						<td class="nowrap hidden-phone" <?php echo ($item->block == 1 ? ' style="color:red" ' : ''); ?>>
                                        <?php if ($item->block == 0) echo JText::_('JLAUNCH_ACTIVE'); else if ($item->block == 1) echo JText::_('JLAUNCH_BLOCKED'); else  echo JText::_('JLAUNCH_PENDING'); ?>
        							</td>
        							<td class="hidden-phone">
                                        <?php if ($canEdit || $canEditOwn) : ?>
                                        <?php
                                            // Instantiate a new JLayoutFile instance and render the batch button
                                			$layout = new JLayoutFile('joomla.toolbar.aepopup');
                                
                                			$dhtml = $layout->render(array('title' => JText::_('JLAUNCH_DETAILS'), 'url'=> 'index.php?option=com_provider&task=provider.edit&tmpl=component&id='.$item->id, 'type' => 'edit'));
                                            echo $dhtml;
                                        ?>
        								<?php else : ?>
        									<?php echo JText::_('JLAUNCH_DETAILS');?> 
        								<?php endif; ?>
                                        
                                        |
                                        <?php if ($canEdit || $canEditOwn) : ?>
                                        <?php
                                            // Instantiate a new JLayoutFile instance and render the batch button
                                			$layout = new JLayoutFile('joomla.links.linkcustom');
                                            if ($item->block == 0) $tsk = 'block'; else if ($item->block == 1) $tsk = 'unblock'; else  $tsk = 'unblock';
                                            if ($item->block == 0) $tsk_text = JText::_('JLAUNCH_BLOCK'); else if ($item->block == 1) $tsk_text = JText::_('JLAUNCH_UNBLOCK'); else  $tsk_text = JText::_('JLAUNCH_APPROVE');
                                            
                                			$dhtml = $layout->render(array('target' => '', 'link'=> 'javascript:void(0);', 'onclick' => 'return listItemTask(\'cb'.$i.'\',\'providers.'.$tsk.'\')', 'title' => '', 'text' => $tsk_text));
                                            echo $dhtml;
                                        ?>
        								<?php else : ?>
        									<?php if ($item->block == 0) echo JText::_('JLAUNCH_BLOCK'); else if ($item->block == 1) echo JText::_('JLAUNCH_UNBLOCK'); else  echo JText::_('JLAUNCH_APPROVE'); ?>
        								<?php endif; ?>
                                        
                                        |
                                        <?php if (($canEdit || $canEditOwn || $trashed) && $item->published != -2) : ?>
                                        <?php
                                            // Instantiate a new JLayoutFile instance and render the batch button
                                			$layout = new JLayoutFile('joomla.links.linkcustom');
                                
                                			$dhtml = $layout->render(array('target' => '', 'link'=> 'javascript:void(0);', 'onclick' => 'return listItemTask(\'cb'.$i.'\',\'providers.trash\')', 'title' => '', 'text' => JText::_('JLAUNCH_REMOVE')));
                                            echo $dhtml;
                                        ?>
        								<?php else : ?>
        									<?php echo JText::_('JLAUNCH_REMOVE');?>
        								<?php endif; ?>
        							</td>
        						</tr>
            				<?php endforeach; ?>
        				</tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-5 col-sm-5 col-md-5">
                <div class="form-inline space_top">
                    <div class="form-group">
                        <label class="perpage_inline" for=""><?php echo JText::_('JLAUNCH_RESULTS_PER_PAGE'); ?>: </label>                    
                    </div>
                    <div class="form-group group_m_right">
                        <?php echo $this->pagination->getLimitBox(); ?>           
                    </div>                                      
                </div>
            </div>
            <div class="col-xs-7 col-sm-7 col-md-7">
                <div class="form-inline pull-right">
                    <div class="form-group pagination pagination-toolbar">
                        <?php echo $this->pagination->getListFooter(); ?>
                    </div>
                </div>
            </div>
            
        </div>       

        <div class="clearfix"></div>
    </div>
		
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
		<?php echo JHtml::_('form.token'); ?>
</form>