<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_provider
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
JHtml::_('formbehavior.dddropdown', 'select');
// Create shortcut to parameters.
//$params = $this->state->get('params');
//$params = $params->toArray();
$company_name = '';
$type = '';
$title = '';
$bio = '';
$private = 1;
if ((int) $this->item->id) {
    $entre = unserialize($this->item->params);
    $company_name = $entre['company_name'];
    $type = $entre['type'];
    $title = $entre['title'];
    $bio = $entre['bio'];
    $private = $entre['private'];
}
?>

<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'provider.cancel') {
            jQuery('#overlay_bg, #overlay_content').hide();
            return false;            
		}
        if (document.formvalidator.isValid(document.id('item-form'))) {
            Joomla.submitform(task, document.getElementById('item-form'));
        }
	}
</script>
<div class="col-md-4">
    <div class="main_menu">
        <ul>
            <li class="m_active">
                <a href="#"><?php echo JText::_('COM_PROVIDER_DETAILS');?></a>
            </li>
            <li>
                <a href="#"><?php echo JText::_('COM_PROVIDER_BOOKS');?></a>
            </li>
            <li>
                <a href="#"><?php echo JText::_('COM_PROVIDER_MEDIA');?></a>
            </li>
            <li>
                <a href="#"><?php echo JText::_('COM_PROVIDER_WEBINARS');?></a>
            </li>
            <li>
                <a href="#"><?php echo JText::_('COM_PROVIDER_LIVE_EVENT');?></a>
            </li>
            <li>
                <a href="#"><?php echo JText::_('COM_PROVIDER_BOOK_TO_SPEAK');?></a>
            </li>
            <li>
                <a href="#"><?php echo JText::_('COM_PROVIDER_REFERRALS');?></a>
            </li>
        </ul>
    </div>
</div>

<div class="col-md-8">
    <div id="for_entre" class="wrap_content_ad none_border">
        <form action="<?php echo JRoute::_('index.php?option=com_provider&layout=edit&id=' . (int) $this->item->id); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="item-form" class="form-validate">
            <?php echo $this->form->renderField('name'); ?>
            
            <?php echo $this->form->renderField('username'); ?>
            
            <?php echo $this->form->renderField('password', '', ''); ?>
            <?php echo $this->form->renderField('oldpassword', '', $this->item->password); ?>
                    
            <?php echo $this->form->renderField('company_name', '', $company_name); ?>
            
            <?php echo $this->form->renderField('title', '', $title); ?>
            
            <?php echo $this->form->renderField('type', '', $type); ?>
            
            <?php //echo $this->form->renderField('profile'); ?>
            
            <!--upload file-->
            <div class="control-group">
                <div class="control-label"><label aria-invalid="false">Profile Photo</label></div>
                <div class="control">
                    <div class="media">
                        <a class="pull-left" href="#" target="_blank">
                            <img src="<?= $this->baseurl; ?>/templates/isis/img/no_avatar.png" title="" alt="" rel="" />
                        </a>
                        <div class="media-body">
                            <div class="avatar-profile-file">
                                <input id="ytUserProfile_avatar" type="hidden" value="" name="data_img"/>
                                <input name="avatar" link="<?php echo JRequest::getVar('option', ''); ?>" id="UserProfile_avatar" type="file"/>
                                <button type="button" name="avatarProfile" id="avatarProfile">Browse or drag an image here...</button>
                                <span id="nameFile-Profile"></span>
                            </div>
                            <div class="intro-photo-profile">Photos should be at least 125px by 125px and in PNG, JPG, or GIF format.</div>
                        </div>
                    </div>
                </div>                
            </div>
            
            <?php //echo $this->form->renderField('company_logo'); ?>
            
            <!--upload file-->
            <div class="control-group">
                <div class="control-label"><label aria-invalid="false">Company Photo <small>(Optinal)</small></label></div>
                <div class="control">
                    <div class="media">
                        <a class="pull-left" href="#" target="_blank">
                            <img src="<?= $this->baseurl; ?>/templates/isis/img/no_avatar.png" title="" alt="" rel="" />
                        </a>
                        <div class="media-body">
                            <div class="avatar-profile-file">
                                <input id="ytUserProfile_avatar" type="hidden" value="" name="data_img"/>
                                <input name="avatar" link="<?php echo JRequest::getVar('option', ''); ?>" id="UserProfile_avatar" type="file"/>
                                <button type="button" name="avatarProfile" id="avatarProfile">Browse or drag an image here...</button>
                                <span id="nameFile-Profile"></span>
                            </div>
                            <div class="intro-photo-profile">Photos should be at least 125px by 125px and in PNG, JPG, or GIF format.</div>
                        </div>
                    </div>
                </div>                
            </div>
            
            <?php echo $this->form->renderField('bio', '', $bio); ?>
            
            <div class="control-group">
                <div class="control">
                    <?php
                    //echo $this->form->renderField('private', '', $private, ($private == 1) ? array('checked'=>'checked') : '' );
                    $check_file = $this->form->getField('private', '', $private);
                    $check_file->hidden = true;
                    if ($check_file) {
                        echo $check_file->renderField(($private == 1) ? array('checked' => 'checked') : '');
                    } else
                        echo '';
                    ?> <?php echo JText::_('JLAUNCH_FIELD_PRIVATE_TRUE_LABEL'); ?>
                </div>
            </div>
            
            <div style="display: none;">
                <input type="hidden" name="task" value="" />
                <input type="hidden" name="cid[]" value="<?php echo (int)$this->item->id; ?>" />
                <?php echo $this->form->renderField('id'); ?>
                <?php echo $this->form->renderField('published'); ?>
                <?php echo $this->form->renderField('access'); ?>
            	<?php echo JHtml::_('form.token'); ?>
            </div>
        </form>
    </div>
</div>