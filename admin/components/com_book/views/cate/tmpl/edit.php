<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_book
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');

// Create shortcut to parameters.
$params = $this->state->get('params');
$params = $params->toArray();
?>

<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'cate.cancel') {
			jQuery('#overlay_bg, #overlay_content').hide();
			return false;
		}
		if (document.formvalidator.isValid(document.id('item-form'))) {
			Joomla.submitform(task, document.getElementById('item-form'));
		}
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_book&task&view=cate&layout=edit&id=' . (int) $this->item->id); ?>" method="post" name="adminForm" id="item-form" class="form-validate">
    <?php echo $this->form->renderField('title'); ?>

    <div style="display: none;">
        <input type="hidden" name="task" value="" />
        <?php echo $this->form->renderField('id'); ?>
        <?php echo $this->form->renderField('published'); ?>
        <?php echo $this->form->renderField('access'); ?>
        <?php echo $this->form->renderField('metadesc'); ?>
        <?php echo $this->form->renderField('metakey'); ?>

    	<?php echo JHtml::_('form.token'); ?>
    </div>
</form>
