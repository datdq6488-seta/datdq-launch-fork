<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_offer
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Tags view class for the Tags package.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_offer
 * @since       3.1
 */
class OfferController extends JControllerLegacy
{
    /**
	 * @var		string	The default view.
	 * @since   1.6
	 */
	protected $default_view = 'offers';

	/**
	 * Method to display a view.
	 *
	 * @param   boolean  $cachable   If true, the view output will be cached
	 * @param   array    $urlparams  An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return  JControllerLegacy  This object to support chaining.
	 *
	 * @since   3.1
	 */
	public function display($cachable = false, $urlparams = false)
	{

		$view   = $this->input->get('view', 'offers');
		$layout = $this->input->get('layout', 'default');
		$id     = $this->input->getInt('id');

		// Check for edit form.
		if ($view == 'offer' && $layout == 'edit' && !$this->checkEditId('com_offer.edit.offer', $id))
		{
			// Somehow the person just went to the form - we don't allow that.
			$this->setError(JText::sprintf('JLIB_APPLICATION_ERROR_UNHELD_ID', $id));
			$this->setMessage($this->getError(), 'error');
			$this->setRedirect(JRoute::_('index.php?option=com_offer&view=offers', false));

			return false;
		}
		parent::display();

		return $this;

	}
}
