<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_cpanel
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Cpanel Controller
 *
 * @package     Joomla.Administrator
 * @subpackage  com_cpanel
 * @since       1.5
 */

class Menu_linksController extends JControllerLegacy
{
    /**
	 * Method to log in a user.
	 *
	 * @return  void
	 */
	public function upload_img()
	{
		// Check for request forgeries.
        $params = $_FILES;
        
        $uploadPath = 'components/'.JRequest::getVar('option', '');
        
        $user = JFactory::getUser();

        $allowedExts = array("png", "PNG", "jpg", "JPG", 'gif',"GIF","jpeg","JPEG");
        $extension = explode(".", $params["uploadphoto"]["name"]);
        $extension = $extension[count($extension) - 1];
        if (($params["uploadphoto"]["size"] <= 20*1024*1024) && in_array($extension, $allowedExts)){
            if ($params["uploadphoto"]["error"] > 0) {
                $url = '';
            } else {
                $name = time() . '-' . rand(1000, 9999) . '-' . date('Y-m-d-H-i-s') . '.' . $extension;
                $path = $uploadPath . '/media/' . $name;
                if (move_uploaded_file($params["uploadphoto"]["tmp_name"], $path)) {
                    @chmod($path, 0777);
                }
            }
        } else {
            $url = '';
        }
        $data['name'] = $name;
        $data['path'] = $path;
        echo json_encode($data);
        die();
		parent::display();
	}
    
    public function getdata()
    {
//        $id = JRequest::getVar('id', '');
//        $db = JFactory::getDBO();
//        $query = "SELECT * FROM `table`";
//        $db->setQuery($query);
//        $result = $db->query();
        $array['id'] = 1;
        $array['task'] = 'edit';
        $array['title'] = '1';
        $array['text'] = '2';
        $array['author'] = '3';
        $array['category'] = '4';
        $array['link'] = '5';
        $array['published_date'] = '6';
        $array['publisher'] = '7';
        $array['avatar'] = '8';
        $array['img'] = '8';
        $array['data_img'] = '10';
        $array['keyword'] = '9';
        
        echo json_encode($array);
        die();
    }
    
    public function removedata()
    {
        $array['status'] = 1;
        echo json_encode($array);
        die();
    }
}
