<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_cpanel
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// No access check.
jimport( 'joomla.environment.request' );
$controller	= JControllerLegacy::getInstance('Menu_links');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
