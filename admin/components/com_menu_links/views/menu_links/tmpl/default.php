<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_cpanel
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
//Get custom field
JFormHelper::addFieldPath('components/com_books/models/fields');
$custom = JFormHelper::loadFieldType('Custom', false);
?>

<script type="text/javascript">
function toDetails(option_link) {
    jQuery('#overlay_bg, #overlay_content').show();
    //send the ajax request to the server
	jQuery.ajax({
		type: 'POST',
		url: option_link,
		dataType : "json",
		data: '',
		success: function(data,textStatus,jqXHR)
		{
            for (var x in data) {
                jQuery('input[name="'+x+'"]').val(data[x]);
            }
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{
			alert("\ntextStatus: '" + textStatus + "'\nerrorThrown: '" + errorThrown + "'\nresponseText:\n" + XMLHttpRequest.responseText);
		}
	});
}
function toRemove(ind, option_link) {
    //send the ajax request to the server
	jQuery.ajax({
		type: 'POST',
		url: option_link,
		dataType : "json",
		data: '',
		success: function(data,textStatus,jqXHR)
		{
            jQuery('tr.row'+ind).remove();
            console.log(data);
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{
			alert("\ntextStatus: '" + textStatus + "'\nerrorThrown: '" + errorThrown + "'\nresponseText:\n" + XMLHttpRequest.responseText);
		}
	});
}

</script>
<div class="warper">
    <div class="w_header">
        <h3 id="title_header">Menu Manager</h3>
        <?=$custom->getAddButton();?>
    </div>
    <div class="w_content">
        <div class="warper">
            <div class="w_header">
                <h4 class="list_header">Menu Links List</h4>
                <div id="filter_search" class="btn-wrapper input-append">
        			<?=$custom->getContentSearch();?>
        		</div>
                <span class="title_filter">Search: &nbsp;&nbsp;&nbsp; </span>
                <div class="styled-select">
                    <?=$custom->getSelect('w_select', '', array(1=>10, 2=>20, 3=>30));?>
                </div>
                <span class="title_filter">
                    Results per page: &nbsp;&nbsp;&nbsp;
                </span>
            </div>
            <div class="m_content">
                <table class="table table-striped" id="itemList" style="position: relative;">
    				<thead>
    					<tr>
    						<th style="width: 45%;" class="title">
    				            Name
    						</th>
    						<th style="width: 25%;" class="nowrap hidden-phone">
    				            Type
    						</th>
        					<th class="nowrap hidden-phone">
                                Page
        					</th>
    						<th style="width: 18%;" class="nowrap hidden-phone">
                                Actions
    						</th>
    					</tr>
    				</thead>
    				<tbody class="ui-sortable">
                        <?php
                        for ($i=0;$i<5;$i++) {
                            ?>
                            <tr class="row<?=$i;?> dndlist-sortable">
                                <td>
                                    <?=$custom->getToDetails('index.php?option='.JRequest::getVar('option', '').'&task=getdata&id='.$i.'&tmpl=component', 'Ask The Expert '.rand(0,100000));?>
                                </td>
                                <td><?php if($i%2 == 1){echo 'Internal Page';} else {echo 'External Page';} ?></td>
                                <td><?php if($i%2 == 1){echo 'Events';} else {echo 'Experts';} ?></td>
                                <td>
                                    <?=$custom->getToDetails('index.php?option='.JRequest::getVar('option', '').'&task=getdata&id='.$i.'&tmpl=component', 'Edits');?>
                                    | <?=$custom->getToRemove($i ,'index.php?option='.JRequest::getVar('option', '').'&task=removedata&id='.$i.'&tmpl=component');?>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
    			</table>
            </div>
        </div>
        <div id="content_pagination" class="pagination pagination-toolbar">
             <?php echo $this->pagination->getPagesLinks(); ?>
        </div>
    </div>
</div>

<div id="overlay_bg" style="display: none;"></div>
<div id="overlay_content" style="display: none;">
    <form action="" method="POST" id="jform">
        <input type="hidden" name="task" value="" />
        <input type="hidden" name="id" value="" />
        <div class="w_header">
            <h4 id="title_header">Create Menu Link</h4>
        </div>
        <div id="p_content">
            <p class="p_title"><span>Name</span></p>
            <p class="p_form">
                <?=$custom->getCustomInput('text', 'name', '', 'input-xxlarge input-large-text');?>
            </p>
                        
            <p class="p_title">Type</p>
            <p class="p_form">
                <?=$custom->getSelect('p_select selectTab', '', array(1=>'Internal Page', 2=>'External Page'));?>
            </p>
            <div class="_stab tab1">
            <p class="p_title">Page</p>
            <p class="p_form">
                <?=$custom->getSelect('p_select', 'page', array(1=>'Book', 2=>'Webinar', 3=>'Events', 4=>'Expert', 5=>'About'));?>
            </p>
            </div>
            <div class="_stab tab2">
            <p class="p_title"><span>Link</span></p>
            <p class="p_form">
                <?=$custom->getCustomInput('text', 'link', '', 'input-xxlarge input-large-text');?>
            </p>
            </div>
            
            <p class="p_title">Target</p>
            <p class="p_form">
                <?=$custom->getSelect('p_select', '', array(1=>'New Tab', 2=>'New Window'));?>
            </p>

            
            <p class="p_title"><span>Order</span></p>
            <p class="p_form">
                <?=$custom->getCustomInput('text', 'order', '', 'input-slarge input-large-text');?>
            </p>
        </div>
        <div class="w_footer">
            <?=$custom->getSaveButton();?>
        </div>
    </form>
</div>
