<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_webinar
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the component HTML helpers.
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.dddropdown', 'select');

$app		= JFactory::getApplication();
$user		= JFactory::getUser();
$userId		= $user->get('id');
$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));
$trashed	= $this->state->get('filter.published') == -2 ? true : false;
?>
<form action="<?php echo JRoute::_('index.php?option=com_webinar&view=cates');?>" method="post" name="adminForm" id="adminForm">
        <div class="w_content">
            <div class="warper">
                <div class="w_header">
                    <h4 class="list_header"><?php echo JText::_('JLAUNCH_CATE_LIST');?></h4>
                    <div id="filter_search" class="btn-wrapper input-append">
                        <input type="text" name="filter_search" id="content_search" class="js-stools-search-string" placeholder="<?php echo JText::_('JSEARCH_FILTER'); ?>" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" />
                        <button type="submit" class="btn hasTooltip"><i class="icon-search"></i></button>
            		</div>
                    <span class="title_filter"><?php echo JText::_('JLAUNCH_SEARCH');?>: &nbsp;&nbsp;&nbsp; </span>
                    <div class="styled-select">
                        <?php echo $this->pagination->getLimitBox(); ?>
                    </select>
                    </div>
                    <span class="title_filter">
                        <?php echo JText::_('JLAUNCH_RESULTS_PER_PAGE');?>: &nbsp;&nbsp;&nbsp;
                    </span>
                </div>
                <div class="m_content">
                    <table class="table table-striped" id="itemList" style="position: relative;">
                		<thead>
                			<tr>
                				<th style="width: 45%;" class="title">
                		            <?php echo JText::_('JLAUNCH_NAME');?>
                				</th>
				                <th class="nowrap hidden-phone">
					                <?php echo JText::_('JLAUNCH_PUBLISHED');?>
				                </th>
                				<th style="width: 18%;" class="nowrap hidden-phone">
                                    <?php echo JText::_('JLAUNCH_ACTIONS');?>
                				</th>
                			</tr>
                		</thead>
                        <tbody>
                            <?php
            				foreach ($this->items as $i => $item) :
            					$canEdit    = $user->authorise('core.edit',       'com_webinar');
            					$canChange  = $user->authorise('core.edit.state', 'com_webinar');
            					?>
        						<tr class="row<?php echo $i % 2; ?>">
                                    <td class="hidden center hidden-phone">
                						<?php echo JHtml::_('grid.id', $i, $item->id); ?>
                					</td>
        							<td>
        								<?php if ($canEdit || $canEditOwn) : ?>
        									<a href="<?php echo JRoute::_('index.php?option=com_webinar&task=webinar.edit&id='.$item->id);?>">
        										<?php echo $this->escape($item->title); ?></a>
        								<?php else : ?>
        									<?php echo $this->escape($item->title); ?>
        								<?php endif; ?>
        							</td>
							        <td class="small nowrap hidden-phone">
								        <?php echo $this->escape($item->published); ?>
							        </td>
        							<td class="center hidden-phone">
                                        <?php if ($canEdit || $canEditOwn) : ?>
                                        <?php
                                            // Instantiate a new JLayoutFile instance and render the batch button
                                			$layout = new JLayoutFile('joomla.toolbar.aepopup');
                                
                                			$dhtml = $layout->render(array('title' => 'Details', 'url'=> 'index.php?option=com_webinar&task=cate.edit&id='.$item->id.'&tmpl=component', 'type' => 'edit'));
                                            echo $dhtml;
                                        ?>
        								<?php else : ?>
        									<?php echo JText::_('COM_OFFER_DETAILS');?>
        								<?php endif; ?>
                                        |
                                        <?php if ($canEdit || $canEditOwn || $trashed) : ?>
                                        <?php
                                            // Instantiate a new JLayoutFile instance and render the batch button
                                			$layout = new JLayoutFile('joomla.links.linkcustom');
                                
                                			$dhtml = $layout->render(array('target' => '', 'link'=> 'javascript:void(0);', 'onclick' => 'if(confirm(\'Are you sure you want delete this item?\')) {return listItemTask(\'cb'.$i.'\',\' cates.trash\')}', 'title' => '', 'text' => JText::_('JLAUNCH_REMOVE')));
                                            echo $dhtml;
                                        ?>
        								<?php else : ?>
        									<?php echo JText::_('JLAUNCH_REMOVE');?>
        								<?php endif; ?>
        							</td>
        						</tr>
            				<?php endforeach; ?>
        				</tbody>
                    </table>
                </div>
            </div>
            <div id="content_pagination" class="pagination pagination-toolbar">
                 <?php echo $this->pagination->getListFooter(); ?>
            </div>
        </div>
		
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
		<?php echo JHtml::_('form.token'); ?>
</form>