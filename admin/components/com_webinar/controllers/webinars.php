<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_webinar
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * The Tags List Controller
 *
 * @package     Joomla.Administrator
 * @subpackage  com_webinar
 * @since       3.1
 */
class WebinarControllerWebinars extends JControllerAdmin
{
	/**
	 * Proxy for getModel
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 *
	 * @return  JModelLegacy  The model.
	 * @since   3.1
	 */
	public function getModel($name = 'Webinar', $prefix = 'WebinarModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}
    
    
    /**
	 * Rebuild the nested set tree.
	 *
	 * @return  boolean  False on failure or error, true on success.
	 *
	 * @since   3.1
	 */
	public function rebuild()
	{
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		$this->setRedirect(JRoute::_('index.php?option=com_webinar&view=webinars', false));

		$model = $this->getModel();

		if ($model->rebuild()) {
			// Rebuild succeeded.
			$this->setMessage(JText::_('COM_WEBINAR_REBUILD_SUCCESS'));
			return true;
		} else {
			// Rebuild failed.
			$this->setMessage(JText::_('COM_WEBINARS_REBUILD_FAILURE'));
			return false;
		}
	}
	public function approve() {

		// Check for request forgeries
		JSession::checkToken() or die(JText::_('JINVALID_TOKEN'));
		echo 'one';
		// Get items to remove from the request.
		$cid = JFactory::getApplication()->input->get('cid', array(), 'array');
		// Get the model.
		$model = $this->getModel();

		// approve the items.
		if ($model->approve($cid))
		{
			$this->setMessage(JText::plural($this->text_prefix . '_N_ITEMS_APPROVED', count($cid)));
		}
		else
		{
			$this->setMessage($model->getError());
		}

		$this->setMessage(JText::plural($this->text_prefix . '_N_ITEMS_APPROVED', count($cid)));


		$this->setRedirect(JRoute::_('index.php?option=com_webinar&view=webinars', false));
		return false;
	}

	public function reject() {

		// Check for request forgeries
		JSession::checkToken() or die(JText::_('JINVALID_TOKEN'));
		echo 'one';
		// Get items to remove from the request.
		$cid = JFactory::getApplication()->input->get('cid', array(), 'array');
		// Get the model.
		$model = $this->getModel();

		// approve the items.
		if ($model->reject($cid))
		{
			$this->setMessage(JText::plural($this->text_prefix . '_N_ITEMS_REJECT', count($cid)));
		}
		else
		{
			$this->setMessage($model->getError());
		}

		$this->setMessage(JText::plural($this->text_prefix . '_N_ITEMS_REJECT', count($cid)));


		$this->setRedirect(JRoute::_('index.php?option=com_webinar&view=webinars', false));
		return false;
	}

	public function test() {

		// Check for request forgeries
		JSession::checkToken() or die(JText::_('JINVALID_TOKEN'));
		echo 'one';
		// Get items to remove from the request.
		$cid = JFactory::getApplication()->input->get('cid', array(), 'array');
		// Get the model.
		$model = $this->getModel();

		// approve the items.
		if ($model->approve($cid))
		{
			$this->setMessage(JText::plural($this->text_prefix . '_N_ITEMS_DELETED', count($cid)));
		}
		else
		{
			$this->setMessage($model->getError());
		}

		$this->setMessage(JText::plural($this->text_prefix . '_N_ITEMS_DELETED', count($cid)));


		$this->setRedirect(JRoute::_('index.php?option=com_webinar&view=webinars', false));
		return false;
	}
}
