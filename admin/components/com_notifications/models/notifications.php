<?php

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla modelitem library
jimport('joomla.application.component.modelitem');

class NotificationsModelNotifications extends JModelList {

	public function __construct($config = array()) {
		parent::__construct($config);
	}

	public function getSearchFilter() {
		return $this->getState('filter.search');
	}

	public function getData($id) {
		// Get a db connection.
		$db = JFactory::getDbo();

		// Create a new query object.
		$query = $db->getQuery(true);

		// Order it by the ordering field.
		$query->select('*');
		$query->from($db->quoteName('#__notifications'));
		$query->where($db->quoteName('id') . ' = ' . $id);

		// Reset the query using our newly populated query object.
		$db->setQuery($query);

		// Load the results as a list of stdClass objects (see later for more options on retrieving data).
		$results = $db->loadObject();
		return $results;
	}

	protected function getListQuery() {
		// Create a new query object.           
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);

		// Select some fields from the hello table
		$query->select('*');
		// Filter by search in title

		$search = $this->getState('filter.search');

		if (!empty($search)) {
			if (stripos($search, 'id:') === 0) {
				$query->where('n.id = ' . (int) substr($search, 3));
			} else {
				$search = $db->quote('%' . $db->escape($search, true) . '%');
				$query->where('(n.title LIKE ' . $search . ')');
			}
		}
		$query->from($db->quoteName('#__notifications') . ' AS n');

		return $query;
	}

	public function getUserGroup() {
		return array(1 => 'Entrepreneur', 2 => 'Content Provider', 3 => 'Partner');
	}

	public function getTriggerGroup() {
		return array(1 => 'Hardcoded Description', 2 => 'Dynamic Description');
	}

}
