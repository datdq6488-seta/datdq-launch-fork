<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_cpanel
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * HTML View class for the Cpanel component
 *
 * @package     Joomla.Administrator
 * @subpackage  com_cpanel
 * @since       1.0
 */
class NotificationsViewNotifications extends JViewLegacy {

	protected $pagination;

	/**
	 * Execute and display a template script.
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  mixed  A string if successful, otherwise a Error object.
	 */
	public function display($tpl = null) {
		// Get data from the model
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');
		$limit = JRequest::getVar('list', null);
		$this->limit = (isset($limit) && $limit == "" ? 10 : $limit['limit']);

		$this->FilterSearch = $this->get('SearchFilter');
		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		$this->user_group = $this->get('UserGroup');
		$this->trigger_group = $this->get('TriggerGroup');
		// Display the template
		parent::display($tpl);
	}

}
