<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_cpanel
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
//Get custom field
JFormHelper::addFieldPath('components/com_books/models/fields');
require_once('components/com_books/helper/custom.php');
$custom = JFormHelper::loadFieldType('Custom', false);
$notifications = $this->items;
$user_group = $this->user_group;
$trigger_group = $this->trigger_group;
$CustomJS = new CustomJS();
echo $CustomJS->prJS(JRequest::getVar('option', ''));
?>
<form action="<?php echo JRoute::_('index.php?option=com_notifications'); ?>" method="post" name="adminForm" id="adminForm">
	<div class="warper">
		<div class="w_header">
			<h3 id="title_header"><?php echo JText::_('Notification Manager'); ?></h3>
			<?= $custom->getAddButton(); ?>
		</div>
		<div class="w_content">
			<div class="warper">
				<?php echo $custom->getContentHeader_1(JText::_('Notification List'), $this->FilterSearch, $this->limit); ?>
				<div class="m_content">
					<table class="table table-striped" id="itemList" style="position: relative;">
						<thead>
						<th style="width: 45%;" class="title">
							<?php echo JText::_('Title'); ?>
						</th>
						<th style="width: 25%;" class="nowrap hidden-phone">
							<?php echo JText::_('Times Sent'); ?>
						</th>
						<th style="width: 18%;" class="nowrap hidden-phone">
							<?php echo JText::_('Actions'); ?>
						</th>
						</thead>
						<tbody class="ui-sortable">
							<?php for ($i = 0; $i < count($notifications); $i++) : ?>
								<tr class="row<?php echo $notifications[$i]->id; ?> dndlist-sortable">
									<td>
										<?php echo $custom->getToDetails('index.php?option=' . JRequest::getVar('option', '') . '&task=notification.getdata&id=' . $notifications[$i]->id . '&tmpl=component', $notifications[$i]->title); ?>
									</td>
									<td><?php echo $notifications[$i]->times_sent ?></td>
									<td>
										<?php echo $custom->getToDetails('index.php?option=' . JRequest::getVar('option', '') . '&task=notification.getdata&id=' . $notifications[$i]->id . '&tmpl=component', 'Edit'); ?>
										| 
										<?php echo $custom->getToRemove($notifications[$i]->id, 'index.php?option=' . JRequest::getVar('option', '') . '&task=notification.delete&id=' . $notifications[$i]->id . '&tmpl=component'); ?>
									</td>
								</tr>
							<?php endfor; ?>
						</tbody>
					</table>
				</div>
			</div>
			<div id="content_pagination" class="pagination pagination-toolbar">
				<?php echo $this->pagination->getListFooter(); ?>
			</div>
		</div>
	</div>
	<?php echo JHtml::_('form.token'); ?>
</form>

<div id="overlay_bg"></div>
<div id="overlay_content">
    <form action="" method="POST" id="jform">
		<input type="hidden" name="task" value="" />
        <input type="hidden" name="option" value="<?php echo JRequest::getVar('option', ''); ?>" />
        <input type="hidden" name="id" value="" />
		<div class="w_header">
            <h4 id="title_header"><?php echo JText::_('Add Notification'); ?></h4>
        </div>
        <div id="p_content">        
			<p class="p_title"><span><?php echo JText::_('Title'); ?></span></p>
			<p class="p_form">
				<?php echo $custom->getCustomInput('text', 'title', '', 'input-xxlarge input-large-text'); ?>
            </p>
			<p class="p_title"><span><?php echo JText::_('Body'); ?></span></p>
			<p class="p_form">
                <textarea name="body" id="tinyeditor" class="mce_editable"></textarea>
				<label id="tinyeditor-error" class="error2" for="tinyeditor"><?=JText::_('This field is required.');?></label>
            </p>
			
            <div class="p_left">
				<p class="p_title"><?php echo JText::_('User Type'); ?></p>
				<p class="p_form"> 
					<?php echo $custom->getSelect('p_select', 'user_group_id', $user_group, 'user_group_id'); ?>
				</p>
            </div>
            <div class="p_right">
				<p class="p_title"><?php echo JText::_('Trigger Description'); ?></p>
				<p class="p_form"> 
					<?php echo $custom->getSelect('p_select', 'trigger_description_id', $trigger_group, 'trigger_description_id'); ?>
				</p>
            </div>
        </div>
		<div class="w_footer">
			<?= $custom->getSaveButton(); ?>
		</div>
    </form>
</div>
