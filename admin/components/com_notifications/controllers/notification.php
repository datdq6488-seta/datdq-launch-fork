<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_categories
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

// import Joomla controllerform library
jimport('joomla.application.component.controllerform');

class NotificationsControllerNotification extends JControllerAdmin {

	public function __construct($config = array()) {
		parent::__construct($config);
	}

	public function getModel($name = 'Notifications', $prefix = 'NotificationsModel', $config = array('ignore_request' => true)) {
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

	function add() {
		$jinput = JFactory::getApplication()->input;
		$noti = new stdClass();
		$noti->id = NULL;
		$noti->title = $jinput->get('title','','HTML');
		$noti->body = $jinput->get('body','','RAW');
		$noti->created = date('Y-m-d h:i:s');
		$noti->trigger_description_id = $jinput->get('trigger_description_id');
		$noti->user_group_id = $jinput->get('user_group_id');
		$result = JFactory::getDbo()->insertObject('#__notifications', $noti);
		if ($result === true) {
			JFactory::getApplication()->redirect(JURI::base() . 'index.php?option=' . $jinput->get('option'));
		}
		die();
	}

	function edit() {
		$jinput = JFactory::getApplication()->input;
		$noti = new stdClass();
		$noti->id = $jinput->get('id');
		$noti->title = $jinput->get('title','','HTML');
		$noti->body = $jinput->get('body','','RAW');
		$noti->trigger_description_id = $jinput->get('trigger_description_id');
		$noti->user_group_id = $jinput->get('user_group_id');
		$noti->modified = date('Y-m-d h:i:s');

		// Update the object into the table.
		$result = JFactory::getDbo()->updateObject('#__notifications', $noti, 'id');
		if ($result === true) {
			JFactory::getApplication()->redirect(JURI::base() . 'index.php?option=' . $jinput->get('option'));
		}
		die();
	}

	function delete() {
		$db = JFactory::getDbo();
		$jinput = JFactory::getApplication()->input;
		$query = $db->getQuery(true);

		// delete all custom keys for user 1001.
		$conditions = array(
			$db->quoteName('id') . ' = ' . $jinput->get('id')
		);

		$query->delete($db->quoteName('#__notifications'));
		$query->where($conditions);

		$db->setQuery($query);
		$result = $db->query();
		if ($result == true) {
			JFactory::getApplication()->redirect(JURI::base() . 'index.php?option=' . $jinput->get('option'));
		} else
			die('ERROR');
	}

	function getdata() {
		$jinput = JFactory::getApplication()->input;
		$model = $this->getModel();
		$data = $model->getData($jinput->get('id'));
		echo json_encode($data);
		die();
	}

}
