<?php
/**
 * @package     Joomla.Site
 * @subpackage  Layout
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$title = $displayData['title'];
$url = $displayData['url'];
$type = $displayData['type'];
?>
<?php
if ($type == 'add') {
    ?>
    <button class="btn btn-small btn-primary pull-right <?php echo $type; ?>popup" data-url="<?php echo $url; ?>">
    	<?php echo $title; ?>
    </button>
    <?php
} else {
    ?>
    <a class="<?php echo $type; ?>popup" data-url="<?php echo $url;?>">
        <?php echo $title;?> </a>
    <?php
}

?>
