<?php
/**
 * @package     Joomla.Site
 * @subpackage  Layout
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$title = $displayData['title'];
$url = $displayData['url'];
$type = $displayData['type'];
?>
<button class="modal button-add <?php echo $type; ?>popup" data-url="<?php echo $url; ?>">
    <?php echo $title; ?>
</button>

