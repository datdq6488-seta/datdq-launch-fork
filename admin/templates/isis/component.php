<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  Templates.isis
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$lang = JFactory::getLanguage();
$this->language = $doc->language;
$this->direction = $doc->direction;
$input = $app->input;
$user = JFactory::getUser();
?>
<jdoc:include type="content" />
<style type="text/css">
    .w_header #toolbar-cancel,
    .w_header #toolbar-save,
    .w_footer #toolbar-block,
    .w_footer #toolbar-unblock,
    .w_footer #toolbar-trash
    {
        display:none;
    }
</style>
<div class="w_header">
    <div class="row">
        <div class="col-md-4">
            <h4 class="title_header"><jdoc:include type="modules" name="title" /></h4>        
        </div>
        <div class="col-md-8">
            <!--call popup header toolbar-->
            <jdoc:include type="modules" name="toolbar" style="no" />
        </div>
    </div>
</div>
<div id="p_content">
    <!--call popup content-->
    <div class="row">
        <jdoc:include type="component" />
    </div>    
</div>
<div class="w_footer">
    <div class="row">
        <div class="col-md-12">
            <!--call popup footer toolbar-->
            <jdoc:include type="modules" name="toolbar" style="no" />
        </div>
    </div>
</div>