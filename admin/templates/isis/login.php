<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  Templates.isis
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$lang = JFactory::getLanguage();

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');
JHtml::_('bootstrap.tooltip');

// Add Stylesheets
$doc->addStyleSheet('templates/' .$this->template. '/css/template.css');

// Load optional RTL Bootstrap CSS
JHtml::_('bootstrap.loadCss', false, $this->direction);

// Load specific language related CSS
$file = 'language/' . $lang->getTag() . '/' . $lang->getTag() . '.css';
if (is_file($file))
{
	$doc->addStyleSheet($file);
}

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->get('sitename');
$logo = $this->baseurl . '/templates/' . $this->template . '/images/admin/header_logo.png';
$slogan = $this->baseurl . '/templates/' . $this->template . '/images/admin/header_slogan.png';
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>" >
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<jdoc:include type="head" />
	<script type="text/javascript">
       	    jQuery(function($) {
            	$( "#form-login input[name='username']" ).focus();
            });
	</script>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	<!--[if lt IE 9]>
		<script src="../media/jui/js/html5.js"></script>
	<![endif]-->
</head>

<body class="site <?php echo $option . " view-" . $view . " layout-" . $layout . " task-" . $task . " itemid-" . $itemid . " ";?>">
    <!-- Header -->
    <header class="header">
        <div class="container-logo">
            <img src="<?php echo $logo; ?>" class="logo" alt="<?php echo $sitename;?>" />
        </div>
        <div class="container-slogan">
            <img src="<?php echo $slogan; ?>" class="slogan" alt="<?php echo $sitename;?>" />
        </div>
    </header>
    
	<!-- Container -->
	<div class="container">
        <div class="login-box-header">
            <label class="login-label"><?php echo JText::_('JGLOBAL_LOGIN_TO_ADMIN'); ?></label>
        </div>
		<div id="content">
			<!-- Begin Content -->
			<div id="element-box" class="login well">
				<jdoc:include type="message" />
				<jdoc:include type="component" />
			</div>
			<noscript>
				<?php echo JText::_('JGLOBAL_WARNJAVASCRIPT') ?>
			</noscript>
			<!-- End Content -->
		</div>
        <div class="login-box-footer">
            <a class="login-joomla pull-left" href="<?php echo JUri::root(); ?>" target="_blank" class="hasTooltip" title="<?php echo JHtml::tooltipText('TPL_ISIS_ISFREESOFTWARE');?>">Privacy Policy</a>
            <a class="login-joomla pull-left" href="<?php echo JUri::root(); ?>" target="_blank" class="hasTooltip" title="<?php echo JHtml::tooltipText('TPL_ISIS_ISFREESOFTWARE');?>">Contac Us</a>
            <a class="login-joomla pull-left" href="<?php echo JUri::root(); ?>" target="_blank" class="hasTooltip" title="<?php echo JHtml::tooltipText('TPL_ISIS_ISFREESOFTWARE');?>">Terms of Use</a>
            <a class="login-joomla pull-left" href="<?php echo JUri::root(); ?>" target="_blank" class="hasTooltip" title="<?php echo JHtml::tooltipText('TPL_ISIS_ISFREESOFTWARE');?>">Site Map</a>
            <br>
            <p class="pull-left">
                &copy; <?php echo date('Y'); ?> <?php echo $sitename; ?>, LLC. All rights reserved.
            </p>
        </div>
	</div>
	<jdoc:include type="modules" name="debug" style="none" />
</body>
</html>
