<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  Templates.isis
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @since       3.0
 */
defined('_JEXEC') or die;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$lang = JFactory::getLanguage();
$this->language = $doc->language;
$this->direction = $doc->direction;
$input = $app->input;
$user = JFactory::getUser();

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');
$doc->addScriptVersion('templates/' . $this->template . '/js/template.js');

// Add Stylesheets
//$doc->addStyleSheetVersion('templates/' . $this->template . '/css/template' . ($this->direction == 'rtl' ? '-rtl' : '') . '.css');
$doc->addStyleSheetVersion('templates/' . $this->template . '/css/bootstrap.min.css');
$doc->addStyleSheetVersion('templates/' . $this->template . '/css/bootstrap-theme.css');
$doc->addStyleSheetVersion('templates/' . $this->template . '/css/style.css');

// Load specific language related CSS
$file = 'language/' . $lang->getTag() . '/' . $lang->getTag() . '.css';

if (is_file($file)) {
    $doc->addStyleSheetVersion($file);
}

// Detecting Active Variables
$option = $input->get('option', '');
$view = $input->get('view', '');
$layout = $input->get('layout', '');
$task = $input->get('task', '');
$itemid = $input->get('Itemid', '');
$sitename = $app->getCfg('sitename');

// Logo file
$logo = $this->baseurl . '/templates/' . $this->template . '/images/admin/logo.png';
$slogan = $this->baseurl . '/templates/' . $this->template . '/images/admin/header_slogan.png';
$avatar = $this->baseurl . '/templates/' . $this->template . '/images/admin/no-avatar.png';

$cpanel = ($option === 'com_cpanel');
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <jdoc:include type="head" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>    
        <!--[if lt IE 9]>
        <script src="../media/jui/js/html5.js"></script>
        <![endif]-->
    </head>

    <body class="admin <?php echo $option . ' view-' . $view . ' layout-' . $layout . ' task-' . $task . ' itemid-' . $itemid; ?>" >
        <!-- Header -->
        <jdoc:include type="modules" name="debug" style="none" />
        <div class="header-wrapper-main">
            <div class="container">
                <div class="row">                    
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <a class="logo_site" href="">
                            <img src="<?php echo $logo; ?>" alt="" />                    
                        </a>
                    </div>
                    <div class="col-xs-8 col-sm-8 col-md-8">                
                        <div class="user-avatar">
                            <img class="img-responsive" src="<?php echo $avatar; ?>" alt="<?php echo $user->name; ?>" />
                        </div>
                        <div class="user-info">
                            <span>Hi <strong><?php echo $user->name; ?></strong>, welcome back</span><br>
                                <a href="index.php?option=com_admin&task=profile.edit&id=<?php echo $user->id; ?>"><?php echo JText::_('TPL_ISIS_EDIT_ACCOUNT'); ?></a>
                                <a href="#">Help</a>
                                <a href="<?php echo JRoute::_('index.php?option=com_login&task=logout&' . JSession::getFormToken() . '=1'); ?>"><?php echo JText::_('TPL_ISIS_LOGOUT'); ?></a>
                        </div>                  
                    </div>            
                </div>
            </div>
        </div><!--End Header-->

        <div class="message">
            <div class="container">
                <span>Your public profile is currently <b>Private</b>. <a href="#">Click here to make public</a></span>
            </div>
        </div>

        <div style="margin-bottom: 20px"></div>

        <!-- container-fluid -->
        <div class="container">
            <section id="content" class="bg_content_admin">
                <div class="">
                    <!-- Begin Content -->
                    <div class="row">
                        <div class="col-md-3">
                            <jdoc:include type="modules" name="icon" style="none" />
                        </div>
                        
                        <div class="col-md-9">                            
                            <div class="wrap_content_ad">
                                <jdoc:include type="message" />
                                <div class="head_title_content">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <h4 id="title_header"><jdoc:include type="modules" name="title" /></h4>    
                                        </div>
                                        <div class="col-md-2">
                                            <jdoc:include type="modules" name="toolbar" style="no" />    
                                        </div>
                                    </div>                                                                        
                                </div>
                                
                                <div class="body_content">                                    
                                    <jdoc:include type="component" />
                                </div>                                
                            </div>                            
                        </div>                                                
                    </div>
                </div>
            </section>
            
            <footer class="footer">                
                <div class="row">
                    <footer class="col-md-12">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="nav_foot">
                                    <ul class="clearfix">
                                        <li><a class="pull-left" href="<?php echo JUri::root(); ?>" target="_blank" class="hasTooltip" title="<?php echo JHtml::tooltipText('TPL_ISIS_ISFREESOFTWARE'); ?>">Privacy Policy</a></li>
                                        <li><a class="pull-left" href="<?php echo JUri::root(); ?>" target="_blank" class="hasTooltip" title="<?php echo JHtml::tooltipText('TPL_ISIS_ISFREESOFTWARE'); ?>">Contact Us</a></li>
                                        <li><a class="pull-left" href="<?php echo JUri::root(); ?>" target="_blank" class="hasTooltip" title="<?php echo JHtml::tooltipText('TPL_ISIS_ISFREESOFTWARE'); ?>">Terms of Use</a></li>
                                        <li><a class="pull-left" href="<?php echo JUri::root(); ?>" target="_blank" class="hasTooltip" title="<?php echo JHtml::tooltipText('TPL_ISIS_ISFREESOFTWARE'); ?>">Site Map</a></li>
                                    </ul>
                                </div>
                                <span class="coppy_right">&copy; <?php echo date('Y'); ?> <?php echo $sitename; ?>, LLC. All rights reserved.</span>
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                    </footer>
                </div>                                                                                               
            </footer>            
        </div>
        
        <!-- AJAX DIV TO SHOW ADD / EDIT -->
        <div id="overlay_bg"></div>
        <div id="overlay_content">
            <div id="myModal">

            </div>
        </div>
    </body>
</html>
