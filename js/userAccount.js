/**
 * Created by Pham Quoc Hung on 6/9/14.
 */
var USERACCOUNT = {
    /**
     * Call all function in script;
     **/
    init: function () {
        USERACCOUNT.blurEvents();
        USERACCOUNT.changeTerms();
        USERACCOUNT.submitCreateAccount();
    },

    /**
     * Event Blur check validate field form;
     **/
    blurEvents: function () {
        $('#RegistrationForm_email, #RegistrationForm_name, #RegistrationForm_company, #RegistrationForm_username, #RegistrationForm_password, #RegistrationForm_verifyPassword, #recaptcha_response_field').blur(function () {
            var objId, objCheck, _this = this, value, validate = true;
            value = $.trim($(_this).val());
            objId = $(_this).attr('id');
            if (objId == 'RegistrationForm_name') {
                $('label[for="' + objId + '"] .invalid-email').remove();
                var patts = /[!@#$%^&*()\{\}<>~`?_\[\]\/:;|"1-9]/g;
                if (value.match(patts) || value.match(/\d/g)) {
                    $('label[for="' + objId + '"]').append('<span class="invalid-email">Invalid Full Name.</span>');
                    $('#' + objId).removeClass('ok-validate').addClass('hight-light error-validate');
                    validate = false;
                } else if (value.length == 0) {
                    $('label[for="' + objId + '"]').append('<span class="invalid-email">Full Name cannot be blank</span>');
                    $(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                    validate = false;
                } else {
                    $(_this).removeClass('hight-light error-validate').addClass('ok-validate');
                }
            } else if (objId == 'RegistrationForm_email') {
                $('label[for="RegistrationForm_email"] .invalid-email').remove();
                if (value.length == 0) {
                    $('label[for="' + objId + '"]').append('<span class="invalid-email">Email Address cannot be blank</span>');
                    $(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                } else {
                    $(_this).removeClass('hight-light error-validate');
                    objCheck = USERACCOUNT.validateEmail(value);
                    if (!objCheck) {
                        $(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                        $('label[for="' + objId + '"] .invalid-email').remove();
                        $('label[for="' + objId + '"] .invalid-strength').remove();
                        $('label[for="' + objId + '"]').append('<span class="invalid-email">Invalid Email Address</span>');
                        validate = false;
                    } else {
                        objCheck = USERACCOUNT.ajaxObjCheck(value);
                        $('label[for="' + objId + '"] .invalid-email').remove();
                        if (objCheck) {
                            $(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                            $('label[for="' + objId + '"]').append('<span class="invalid-email">Email already exists</span>');
                            validate = false;
                        } else {
                            $(_this).removeClass('hight-light error-validate');
                            $(_this).addClass('ok-validate');
                        }
                    }
                }
            } else if (objId == 'RegistrationForm_username') {
                $('label[for="' + objId + '"] .invalid-email').remove();
                if (value.match(/\s/g)) {
                    $('label[for="' + objId + '"]').append('<span class="invalid-email">Invalid Username.</span>');
                    $(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                    validate = false;
                } else if (value.length == 0) {
                    $('label[for="' + objId + '"]').append('<span class="invalid-email">Username cannot be blank</span>');
                    $(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                    validate = false;
                } else if (value.length < 4 || value.length > 24) {
                    $('label[for="' + objId + '"]').append('<span class="invalid-email">The username must be between 4 and 24 characters</span>');
                    $(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                    validate = false;
                } else {
                    objCheck = USERACCOUNT.ajaxObjCheck(value);
                    if (objCheck) {
                        $(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                        $('label[for="' + objId + '"]').append('<span class="invalid-email">Username already exists</span>');
                        validate = false;
                    } else {
                        $(_this).removeClass('hight-light error-validate').addClass('ok-validate');
                    }
                }

            } else if (objId == 'RegistrationForm_password') {
                $('label[for="' + objId + '"] .invalid-email').remove();
                $('label[for="' + objId + '"] .invalid-strength').remove();
                if (value.length == 0) {
                    $('label[for="' + objId + '"]').append('<span class="invalid-email">Password cannot be blank</span>');
                    $(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                    validate = false;
                } else if (value.length < 6 || value.length > 18) {
                    $('label[for="' + objId + '"]').append('<span class="invalid-email">Password must be between 6 and 18 characters</span>');
                    $(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                    validate = false;
                } else {
                    USERACCOUNT.checkValidatePass(value, objId);
                    $(_this).removeClass('hight-light error-validate').addClass('ok-validate');
                }
            } else if (objId == 'RegistrationForm_verifyPassword') {
                $('label[for="' + objId + '"] .invalid-email').remove();
                if (value.length == 0) {
                    $('label[for="' + objId + '"]').append('<span class="invalid-email">Confirm Password cannot be blank</span>');
                    $(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                    validate = false;
                } else {
                    var pass = $('#RegistrationForm_password').val();
                    if (pass != value) {
                        $('label[for="' + objId + '"]').append('<span class="invalid-email">The confirm password does not match.</span>');
                        $(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                        validate = false;
                    } else
                        $(_this).removeClass('hight-light error-validate').addClass('ok-validate');
                }

            }

            return validate;
        });
    },    /**
     * Check data after click submit form.
     **/
    submitCreateAccount: function () {
        $('#registerId').click(function () {
            var _this = this, fullname, email, pass, rePass, company, usergroup_id, username, term, captcha, validate = true;
            fullname = 'RegistrationForm_name';
            email = 'RegistrationForm_email';
            pass = 'RegistrationForm_password';
            rePass = 'RegistrationForm_verifyPassword';
            company = 'RegistrationForm_company';
            username = 'RegistrationForm_username';
            usergroup_id = 'RegistrationForm_accountType';
            term = $('#RegistrationForm_user_terms').is(':checked');
            captcha = $('#recaptcha_response_field').val();
            var arrErrors = new Array();

            // Full name check
            $('label[for="'+ fullname +'"] .invalid-email').remove();
            var patts = /[!@#$%^&*()\{\}<>~`?_\[\]\/:;|"1-9]/g;
            if ($.trim($('#' + fullname).val()).match(patts) || $.trim($('#' + fullname).val()).match(/\d/g)) {
                $('label[for="'+ fullname +'"]').append('<span class="invalid-email">Invalid Full Name.</span>');
                $('#'+ fullname +'').removeClass('ok-validate').addClass('hight-light error-validate');
                arrErrors.push(false);
            } else if ($.trim($('#' + fullname).val()).length == 0) {
                $('label[for="'+ fullname +'"]').append('<span class="invalid-email">Full Name cannot be blank</span>');
                $('#'+ fullname +'').removeClass('ok-validate').addClass('hight-light error-validate');
                arrErrors.push(false);
            } else {
                $('#'+ fullname +'').removeClass('hight-light error-validate').addClass('ok-validate');
            }

            //================================
            //Validate Email
            $('label[for="'+ email +'"] .invalid-email').remove();
            if ($('#' + email).val().length == 0) {
                $('label[for="' + email + '"]').append('<span class="invalid-email">Email Address cannot be blank</span>');
                $($('#' + email)).removeClass('ok-validate').addClass('hight-light error-validate');
                arrErrors.push(false);
            } else {
                $($('#' + email)).removeClass('hight-light error-validate');
                objCheck = USERACCOUNT.validateEmail($('#' + email).val());
                if (!objCheck) {
                    $($('#' + email)).removeClass('ok-validate').addClass('hight-light error-validate');
                    $('label[for="' + email + '"] .invalid-email').remove();
                    $('label[for="' + email + '"] .invalid-strength').remove();
                    $('label[for="' + email + '"]').append('<span class="invalid-email">Invalid Email Address</span>');
                    arrErrors.push(false);
                } else {
                    objCheck = USERACCOUNT.ajaxObjCheck($('#' + email).val());
                    $('label[for="' + email + '"] .invalid-email').remove();
                    if (objCheck) {
                        $($('#' + email)).removeClass('ok-validate').addClass('hight-light error-validate');
                        $('label[for="' + email + '"]').append('<span class="invalid-email">Email already exists</span>');
                        arrErrors.push(false);
                    } else {
                        $($('#' + email)).removeClass('hight-light error-validate');
                        $($('#' + email)).addClass('ok-validate');
                    }
                }
            }
            //Validate Username
            $('label[for="' + username + '"] .invalid-email').remove();
            if ($('#' + username).val().match(/\s/g)) {
                $('label[for="' + username + '"]').append('<span class="invalid-email">Invalid Username.</span>');
                $('#' + username).removeClass('ok-validate').addClass('hight-light error-validate');
                arrErrors.push(false);
            } else if ($('#' + username).val().length == 0) {
                $('label[for="' + username + '"]').append('<span class="invalid-email">Username cannot be blank</span>');
                $('#' + username).removeClass('ok-validate').addClass('hight-light error-validate');
                arrErrors.push(false);
            } else if ($('#' + username).val().length < 4 || $('#' + username).val().length > 24) {
                $('label[for="' + username + '"]').append('<span class="invalid-email">The username must be between 4 and 24 characters</span>');
                $('#' + username).removeClass('ok-validate').addClass('hight-light error-validate');
                arrErrors.push(false);
            } else {
                objCheck = USERACCOUNT.ajaxObjCheck($('#' + username).val());
                if (objCheck) {
                    $('#' + username).removeClass('ok-validate').addClass('hight-light error-validate');
                    $('label[for="' + username + '"]').append('<span class="invalid-email">Username already exists</span>');
                    arrErrors.push(false);
                } else {
                    $('#' + username).removeClass('hight-light error-validate').addClass('ok-validate');
                }
            }

            //Validate password
            $('label[for="' + pass + '"] .invalid-email').remove();
            $('label[for="' + pass + '"] .invalid-strength').remove();
            if ($('#' + pass).val().length == 0) {
                $('label[for="' + pass + '"]').append('<span class="invalid-email">Password cannot be blank</span>');
                $('#' + pass).removeClass('ok-validate').addClass('hight-light error-validate');
                arrErrors.push(false);
            } else if ($('#' + pass).val().length < 6 || $('#' + pass).val().length > 18) {
                $('label[for="' + pass + '"]').append('<span class="invalid-email">Password must be between 6 and 18 characters</span>');
                $('#' + pass).removeClass('ok-validate').addClass('hight-light error-validate');
                arrErrors.push(false);
            } else {
                USERACCOUNT.checkValidatePass($('#' + pass).val(), pass);
                $('#' + pass).removeClass('hight-light error-validate').addClass('ok-validate');
            }

            //Validate verifyPassword
            $('label[for="' + rePass + '"] .invalid-email').remove();
            if ($('#' + rePass).val().length == 0) {
                $('label[for="' + rePass + '"]').append('<span class="invalid-email">Confirm Password cannot be blank</span>');
                $('#' + rePass).removeClass('ok-validate').addClass('hight-light error-validate');
                arrErrors.push(false);
            } else {
                var pass = $('#' + pass).val();
                if (pass != $('#' + rePass).val()) {
                    $('label[for="' + rePass + '"]').append('<span class="invalid-email">The confirm password does not match.</span>');
                    $('#' + rePass).removeClass('ok-validate').addClass('hight-light error-validate');
                    arrErrors.push(false);
                } else
                    $('#' + rePass).removeClass('hight-light error-validate').addClass('ok-validate');
            }

            //Validate terms
            var term = $('#RegistrationForm_user_terms').is(':checked');
            $('.user-field-terms .invalid-email').remove();
            if (!term) {
                $('.user-field-terms').append('<span class="invalid-email">Please check the box to indicate your acceptance.</span>');
                arrErrors.push(false);
            }
            if(arrErrors.length > 0){
                return false;
            }else{
                $(this).parents('form').submit();
            }
        });
    },

    changeTerms: function () {
        $('#RegistrationForm_user_terms').change(function () {
            var term = $(this).is(':checked');
            $('.user-field-terms .invalid-email').remove();
            if (!term) {
                $('.user-field-terms').append('<span class="invalid-email">Please check the box to indicate your acceptance.</span>');
            }
        });
    },
    /**
     * Check validate email;
     **/
    validateEmail: function (email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    },

    /**
     * Check password
     ***/
    checkValidatePass: function (value, objId) {
        var i = 0;
        var j = 0;
        var ch = '';
        var checkUpperCase = 0;
        var checkLowerCase = 0;
        var checkNumCase = 0;
        var checIntNum = 0;
        var checkfmCase = 0;
        var checkValue1 = '';
        var checkValue2 = '';
        var checkValue3 = '';
        var patt1 = /[0-9]/g;
        var patt2 = /[a-zA-Z]/g;
        var patt3 = /[@*&^%$#!]/g;
        var checkInt = value.match(patt1) == null ? 0 : 1;
        var checkChart1 = value.match(patt1) == null ? '' : value.match(patt1);
        var checkChart2 = value.match(patt2) == null ? '' : value.match(patt2);
        var checkChart3 = value.match(patt3) == null ? '' : value.match(patt3);

        if (checkChart1 != '') {
            var myString = new String(checkChart1);
            var checkValue1 = myString.split(',').join('');
        }
        if (checkChart2 != '') {
            var myString = new String(checkChart2);
            var checkValue2 = myString.split(',').join('');
        }
        if (checkChart3 != '') {
            var myString = new String(checkChart3);
            var checkValue3 = myString.split(',').join('');
        }
        if (checkChart1 != '') {
            checkNumCase = 1;
        }
        if (checkChart3 != '') {
            checkfmCase = 1;
        }
        while (j < checkValue2.length) {
            character2 = checkValue2.charAt(j);
            if (character2 == character2.toUpperCase()) {
                checkUpperCase = 1;
            }
            if (character2 == character2.toLowerCase()) {
                checkLowerCase = 1;
            }
            j++;
        }
        $('label[for="' + objId + '"] .invalid-email').remove();
        $('label[for="' + objId + '"] .invalid-strength.weak').remove();
        $('label[for="' + objId + '"] .invalid-strength.moderate').remove();
        $('label[for="' + objId + '"] .invalid-strength.strong').remove();
        if ((checkUpperCase == 1 && checkLowerCase == 1 && checkNumCase == 1) || (checkUpperCase == 1 && checkLowerCase == 1 && checkfmCase == 1) || (checkNumCase == 1 && checkLowerCase == 1 && checkfmCase == 1) || (checkNumCase == 1 && checkUpperCase == 1 && checkfmCase == 1)) {
            $('label[for="' + objId + '"]').append('<span class="invalid-strength strong">Password strength: <span>strong</span></span>');
        }
        if (((checkfmCase != 1) && ((checkNumCase == 1 && checkLowerCase == 0 && checkUpperCase == 1) || (checkNumCase == 1 && checkLowerCase == 1 && checkUpperCase == 0) || (checkUpperCase == 1 && checkLowerCase == 1 && checkNumCase == 0) || (checkUpperCase == 1 && checkLowerCase == 0 && checkNumCase == 1) || (checkUpperCase == 0 && checkLowerCase == 1 && checkNumCase == 1))) ||
            ((checkfmCase == 1) && ((checkNumCase == 1 && checkLowerCase == 0 && checkUpperCase == 0) || (checkNumCase == 0 && checkLowerCase == 1 && checkUpperCase == 0) || (checkUpperCase == 1 && checkLowerCase == 0 && checkNumCase == 0)))) {
            $('label[for="' + objId + '"]').append('<span class="invalid-strength moderate">Password strength: <span>moderate</span></span>');
        }
        if ((checkfmCase != 1) && ((checkNumCase == 0 && checkLowerCase == 1 && checkUpperCase == 0) || (checkNumCase == 0 && checkLowerCase == 0 && checkUpperCase == 1) || (checkNumCase == 1 && checkLowerCase == 0 && checkUpperCase == 0))) {
            $('label[for="' + objId + '"]').append('<span class="invalid-strength weak">Password strength: <span>weak</span></span>');
        }

    },

    /**
     * Ajax check Email or Username already exist;
     **/
    ajaxObjCheck: function (value) {
        var check = true;
        $.ajax({
            url: '/service/user/checkobj',
            type: 'post',
            async: false,
            data: {
                value: value
            },
            success: function (result) {
                check = result.status;
            }
        });
        return check;
    }

}
$('document').ready(function ($) {
    USERACCOUNT.init();
});