var EVENT = {
	init: function() {
		EVENT.ajaxLoadMore();
	},
	/**
	 * Ajax Load more data page;
	 **/
	ajaxLoadMore: function() {
		jQuery('.book-load-more').click(function(e) {
			var page = 1, result;
			e.preventDefault();
			AJAX.ajaxCallback = function() {
				if (AJAX.ajaxResult) {
					jQuery('.content-bookstore-page > ul').append(AJAX.ajaxResult);
				}
			};
			AJAX.ajaxData = {};
			AJAX.ajaxData.page = page;
			AJAX.ajaxAsync = false;
			AJAX.ajaxParseJSON = false;
			AJAX.ajaxUrl = PARAMS.baseUrl + '/events/loadevent';
			AJAX.request();
			return result;
		});
	}
}

jQuery('document').ready(function() {
	EVENT.init();
});
