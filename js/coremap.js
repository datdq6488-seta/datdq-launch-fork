var COREMAP = {
	init: function(events, tag_id) {
		google.maps.event.addDomListener(window, 'load', COREMAP.initialize(events, tag_id));
	},
	initialize: function(events, tag_id) {
		//Create center point : its first element in event collection
		var myCenter = new google.maps.LatLng(events[1].lat, events[1].lng);
		//Create map option
		var mapProp = {
			zoom: 12,
			center: myCenter,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			disableDefaultUI: true,
		};
		//Create Map
		var map = new google.maps.Map(document.getElementById(tag_id), mapProp);
		//Create info window object
		var infowindow = new google.maps.InfoWindow({
		});

		var marker;
		var markers = new Array();

		var event;
		// Add the markers and infowindows to the map
		for (index in events) {
			event = events[index];
			//Create Marker
			marker = new google.maps.Marker({
				position: new google.maps.LatLng(event.lat, event.lng),
				animation: google.maps.Animation.DROP,
				map: map, //if no set map, we can use marker.setMap(map);
				title: event.name
			});

			//Push to Marker array, use for AutoCenter action
			markers.push(marker);
			//Only add click for event in Map view, not Detail Page
			if (typeof (event.disable_info) == 'undefined') {
				google.maps.event.addListener(marker, 'click', (function(marker, i, event) {
					return function() {
						COREMAP.createInfoWindow(event, marker, map, infowindow);
					}
				})(marker, index, event));
			}
			//Highlight when mark one event location
			if (typeof (event.highlight_info) != 'undefined' && event.highlight_info == true) {
				COREMAP.createInfoWindow(event, marker, map, infowindow);
			}

		}
		//If detail page, no need to Auto center because center point is that detail event
		if (markers.length > 1) {
			COREMAP.autoCenter(markers, map);
		}

	},
	autoCenter: function(markers, map) {
		//  Create a new viewpoint bound
		var bounds = new google.maps.LatLngBounds();
		//  Go through each...
		$.each(markers, function(index, marker) {
			bounds.extend(marker.position);
		});
		//  Fit these bounds to the map
		map.fitBounds(bounds);
	},
	createInfoWindow: function(event, marker, map, infowindow) {
		infowindow.setContent(COREMAP.renderPopupEvent(event));
		infowindow.open(map, marker);
	},
	getEventData: function(href) {
		AJAX.ajaxCallback = null;
		AJAX.ajaxData = {};
		AJAX.ajaxAsync = false;
		AJAX.ajaxParseJSON = true;
		AJAX.ajaxUrl = href;
		AJAX.request();
		return AJAX.ajaxResult;
	},
	renderPopupEvent: function(event) {
		var html = "<div class='popup-event'>";
		html += "<div class='popup-event-image'><a href='" + PARAMS.baseUrl + "/events/detail'><img src='" + event.img + "'/></a></div>";
		html += "<div class='popup-event-content'>";
		html += "<div class='title-event'><a href='" + PARAMS.baseUrl + "/events/detail'>" + event.name + "</a></div>";
		html += "<div class='time-event'><strong>" + event.time + "</strong></div>";
		html += "<div class='location-event'><strong>Location:</strong>&nbsp;<span>" + event.location + "</span></div>";
		html += "<div class='detail-link-event'><a href='" + PARAMS.baseUrl + "/events/detail'>View Information</a></div>";
		html += "</div>";
		html += "</div>";
		return html;
	}

}
