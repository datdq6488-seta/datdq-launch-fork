var BOOKSTORE = {
	init: function() {
		BOOKSTORE.ajaxLoadMore();
		BOOKSTORE.bookEvents();
	},
	/**
	 * Ajax Load more data page;
	 **/
	ajaxLoadMore: function() {
		var page = 0, result;
		if (jQuery('#current-page').val()) {
			page = jQuery('#current-page').val();
		}
		AJAX.ajaxCallback = function() {
			if (AJAX.ajaxResult) {
				jQuery('#addtional_provider').append(AJAX.ajaxResult);
				jQuery('#current-page').val(parseInt(jQuery('#current-page').val()) + 1);
				if (jQuery('#current-page').val() === jQuery('#total-page').val()) {
					jQuery('.view_more').remove();
				}
				jQuery('#no-result').addClass('hide');
//				jQuery("body,html").animate({
//					scrollTop: jQuery("#header-top").offset().top - 10
//				}, 1800);
			} else {
				jQuery('#no-result').removeClass('hide');
			}
		};
		var sort = jQuery('#book_sort').val();
		var key = jQuery.trim(jQuery('#key').val());
		AJAX.ajaxData = {sort: sort, key: key};
		//console.log(AJAX.ajaxData);
		AJAX.ajaxData.page = parseInt(page) + 1;
		AJAX.ajaxAsync = false;
		AJAX.ajaxParseJSON = false;
		AJAX.ajaxUrl = PARAMS.baseUrl + '/books/loadMore';
		AJAX.request();
		return result;
	},
	bookEvents: function() {
		// Add Share Button
		try {
			stLight.options({publisher: "667b8d6a-ff22-44ac-a2ed-4ccc46f7001e", doNotHash: true, doNotCopy: false, hashAddressBar: false, shorten: false, popup: false});
			jQuery("#share .share-social").jqSocialSharer();
		} catch (e) {

		}
	}
}

jQuery('document').ready(function() {
	//For list page
	if(jQuery('#total-page').val()){
		//Run ajax first time
		BOOKSTORE.init();
		//Click View More action
		jQuery('.book-load-more').click(function() {
			BOOKSTORE.ajaxLoadMore();
		});

		//Call function for dropdown sort by
		jQuery("#book_sort").msDropDown();
		
		//Get request from Url
		var params = window.location.search;
		
		if(params !== ''){
			//if has params search or sort -> scroll to result block
			if(params.indexOf('sort') !== -1 || params.indexOf('key') !== -1){
				jQuery("body,html").animate({
					scrollTop: jQuery("#addtional_provider").offset().top - 10
				}, 200);	
			}
		}
	} else {
		BOOKSTORE.bookEvents();
	}
	
});