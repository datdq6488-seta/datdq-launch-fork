jQuery('doucment').ready(function() {
	//WLCONFIG.init();
	var obj = jQuery('#logo_image');
	var callback = '';
	CONFIGACCOUNTS.media_upload_media(obj, callback);

	//Page Background
	jQuery('.page-bg').colorpicker({
		//color: '#db38c6',
	}).on('changeColor', function(ev) {
		jQuery('.inner_layout')[0].style.backgroundColor = ev.color.toHex();
	});
	
	//Page Font
	jQuery('.page-font').colorpicker({
		//color: '#db38c6',
	}).on('changeColor', function(ev) {
		jQuery('.r_sb li').each(function(){
			this.style.backgroundColor = ev.color.toHex();
		});
	});
	
	//Page link
	jQuery('.page-link').colorpicker({
		//color: '#db38c6',
	}).on('changeColor', function(ev) {
		jQuery('.links')[0].style.backgroundColor = ev.color.toHex();
	});
	
	//Section title
	jQuery('.section-title').colorpicker({
		//color: '#db38c6',
	}).on('changeColor', function(ev) {
		jQuery('.section_title')[0].style.backgroundColor = ev.color.toHex();
	});
	
	//Header background
	jQuery('.header-bg').colorpicker({
		//color: '#db38c6',
	}).on('changeColor', function(ev) {
		jQuery('.layouts_header')[0].style.backgroundColor = ev.color.toHex();
	});
	
	//Header font
	jQuery('.header-font').colorpicker({
		//color: '#db38c6',
	}).on('changeColor', function(ev) {
		jQuery('.layouts_logo')[0].style.backgroundColor = ev.color.toHex();
		jQuery('.navvigate ul li').each(function(){
			this.style.backgroundColor = ev.color.toHex();
		});
	});
	
	jQuery('.header-font').colorpicker();
});