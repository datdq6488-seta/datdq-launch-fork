/* Author:
 Pham Quoc Hung
 */

var LAUNCH = {
    init:function(){
        LAUNCH.privatePublic();
    },
    /**
     * Check validate email;
     **/
    validateEmail:function(email){
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    },

    /**
     * Check password
     ***/
    checkValidatePass:function(value){
        var i=0;
        var j=0;
        var ch='';
        var checkUpperCase = 0;
        var checkLowerCase = 0;
        var checkNumCase = 0;
        var checIntNum = 0;
        var checkfmCase = 0;
        var checkValue1 = '';
        var checkValue2 = '';
        var checkValue3 = '';
        var patt1=/[0-9]/g;
        var patt2=/[a-zA-Z]/g;
        var patt3=/[@*&^%$#!]/g;
        var checkInt = value.match(patt1) == null ? 0: 1;
        var checkChart1 = value.match(patt1) == null ? '': value.match(patt1);
        var checkChart2 = value.match(patt2) == null ? '': value.match(patt2);
        var checkChart3 = value.match(patt3) == null ? '': value.match(patt3);

        if(checkChart1 != ''){
            var myString = new String(checkChart1);
            var checkValue1 = myString.split(',').join('');
        }
        if(checkChart2 != ''){
            var myString = new String(checkChart2);
            var checkValue2 = myString.split(',').join('');
        }
        if(checkChart3 != ''){
            var myString = new String(checkChart3);
            var checkValue3 = myString.split(',').join('');
        }
        if(checkChart1 !=''){
            checkNumCase = 1;
        }
        if(checkChart3 !=''){
            checkfmCase = 1;
        }
        while (j < checkValue2.length){
            character2 = checkValue2.charAt(j);
            if (character2 == character2.toUpperCase()) {
                checkUpperCase = 1;
            }
            if ( character2 == character2.toLowerCase() ){
                checkLowerCase = 1;
            }
            j++;
        }
        jQuery('label[for="TUser_password"] .invalid-email').remove();
        jQuery('label[for="TUser_password"] .invalid-strength.weak').remove();
        jQuery('label[for="TUser_password"] .invalid-strength.moderate').remove();
        jQuery('label[for="TUser_password"] .invalid-strength.strong').remove();
        if( (checkUpperCase == 1 && checkLowerCase == 1 && checkNumCase == 1) || (checkUpperCase == 1 && checkLowerCase == 1 && checkfmCase == 1) || (checkNumCase == 1 && checkLowerCase == 1 && checkfmCase == 1) || (checkNumCase == 1 && checkUpperCase == 1 && checkfmCase == 1) ){
            jQuery('label[for="TUser_password"]').append('<span class="invalid-strength strong">Password strength: <span>strong</span></span>');
        }
        if( ((checkfmCase != 1) && ((checkNumCase == 1 && checkLowerCase == 0 && checkUpperCase == 1) || (checkNumCase == 1 && checkLowerCase == 1 && checkUpperCase == 0) || (checkUpperCase == 1 && checkLowerCase == 1 && checkNumCase == 0) || (checkUpperCase == 1 && checkLowerCase == 0 && checkNumCase == 1) || (checkUpperCase == 0 && checkLowerCase == 1 && checkNumCase == 1))) ||
            ((checkfmCase == 1) && ((checkNumCase == 1 && checkLowerCase == 0 && checkUpperCase == 0) || (checkNumCase == 0 && checkLowerCase == 1 && checkUpperCase == 0) || (checkUpperCase == 1 && checkLowerCase == 0 && checkNumCase == 0))) ){
            jQuery('label[for="TUser_password"]').append('<span class="invalid-strength moderate">Password strength: <span>moderate</span></span>');
        }
        if( (checkfmCase != 1) && ((checkNumCase == 0 && checkLowerCase == 1 && checkUpperCase == 0) || (checkNumCase == 0 && checkLowerCase == 0 && checkUpperCase == 1) || (checkNumCase == 1 && checkLowerCase == 0 && checkUpperCase == 0)) ){
            jQuery('label[for="TUser_password"]').append('<span class="invalid-strength weak">Password strength: <span>weak</span></span>');
        }

    },

    privatePublic:function(){
            jQuery( ".line-header-content a" ).on( "click", function(e) {
                e.preventDefault();
                var status,_this=this,result;
                status = jQuery(_this).attr('rel');
                AJAX.ajaxCallback = function(){
                    result = parseInt(AJAX.ajaxResult);
                    window.location.reload();
                };
                AJAX.ajaxData = {};
                AJAX.ajaxData.status = status;
                AJAX.ajaxAsync = false;
                AJAX.ajaxParseJSON = false;
                AJAX.ajaxUrl = PARAMS.baseUrl + '/user/private';
                AJAX.request();
                return result;
            });
    },

    browserVersion:function() {
        if(!jQuery.browser){

            jQuery.browser = {};
            jQuery.browser.mozilla = false;
            jQuery.browser.webkit = false;
            jQuery.browser.opera = false;
            jQuery.browser.safari = false;
            jQuery.browser.chrome = false;
            jQuery.browser.msie = false;
            jQuery.browser.android = false;
            jQuery.browser.blackberry = false;
            jQuery.browser.ios = false;
            jQuery.browser.operaMobile = false;
            jQuery.browser.windowsMobile = false;
            jQuery.browser.mobile = false;

            var nAgt = navigator.userAgent;
            jQuery.browser.ua = nAgt;

            jQuery.browser.name  = navigator.appName;
            jQuery.browser.fullVersion  = ''+parseFloat(navigator.appVersion);
            jQuery.browser.majorVersion = parseInt(navigator.appVersion,10);
            var nameOffset,verOffset,ix;

// In Opera, the true version is after "Opera" or after "Version"
            if ((verOffset=nAgt.indexOf("Opera"))!=-1) {
                jQuery.browser.opera = true;
                jQuery.browser.name = "Opera";
                jQuery.browser.fullVersion = nAgt.substring(verOffset+6);
                if ((verOffset=nAgt.indexOf("Version"))!=-1)
                    jQuery.browser.fullVersion = nAgt.substring(verOffset+8);
            }

// In MSIE < 11, the true version is after "MSIE" in userAgent
            else if ( (verOffset=nAgt.indexOf("MSIE"))!=-1) {
                jQuery.browser.msie = true;
                jQuery.browser.name = "Microsoft Internet Explorer";
                jQuery.browser.fullVersion = nAgt.substring(verOffset+5);
            }

// In TRIDENT (IE11) => 11, the true version is after "rv:" in userAgent
            else if (nAgt.indexOf("Trident")!=-1 ) {
                jQuery.browser.msie = true;
                jQuery.browser.name = "Microsoft Internet Explorer";
                var start = nAgt.indexOf("rv:")+3;
                var end = start+4;
                jQuery.browser.fullVersion = nAgt.substring(start,end);
            }

// In Chrome, the true version is after "Chrome"
            else if ((verOffset=nAgt.indexOf("Chrome"))!=-1) {
                jQuery.browser.webkit = true;
                jQuery.browser.chrome = true;
                jQuery.browser.name = "Chrome";
                jQuery.browser.fullVersion = nAgt.substring(verOffset+7);
            }
// In Safari, the true version is after "Safari" or after "Version"
            else if ((verOffset=nAgt.indexOf("Safari"))!=-1) {
                jQuery.browser.webkit = true;
                jQuery.browser.safari = true;
                jQuery.browser.name = "Safari";
                jQuery.browser.fullVersion = nAgt.substring(verOffset+7);
                if ((verOffset=nAgt.indexOf("Version"))!=-1)
                    jQuery.browser.fullVersion = nAgt.substring(verOffset+8);
            }
// In Safari, the true version is after "Safari" or after "Version"
            else if ((verOffset=nAgt.indexOf("AppleWebkit"))!=-1) {
                jQuery.browser.webkit = true;
                jQuery.browser.name = "Safari";
                jQuery.browser.fullVersion = nAgt.substring(verOffset+7);
                if ((verOffset=nAgt.indexOf("Version"))!=-1)
                    jQuery.browser.fullVersion = nAgt.substring(verOffset+8);
            }
// In Firefox, the true version is after "Firefox"
            else if ((verOffset=nAgt.indexOf("Firefox"))!=-1) {
                jQuery.browser.mozilla = true;
                jQuery.browser.name = "Firefox";
                jQuery.browser.fullVersion = nAgt.substring(verOffset+8);
            }
// In most other browsers, "name/version" is at the end of userAgent
            else if ( (nameOffset=nAgt.lastIndexOf(' ')+1) < (verOffset=nAgt.lastIndexOf('/')) ){
                jQuery.browser.name = nAgt.substring(nameOffset,verOffset);
                jQuery.browser.fullVersion = nAgt.substring(verOffset+1);
                if (jQuery.browser.name.toLowerCase()==jQuery.browser.name.toUpperCase()) {
                    jQuery.browser.name = navigator.appName;
                }
            }

            /*Check all mobile environments*/
            jQuery.browser.android = (/Android/i).test(nAgt);
            jQuery.browser.blackberry = (/BlackBerry/i).test(nAgt);
            jQuery.browser.ios = (/iPhone|iPad|iPod/i).test(nAgt);
            jQuery.browser.operaMobile = (/Opera Mini/i).test(nAgt);
            jQuery.browser.windowsMobile = (/IEMobile/i).test(nAgt);
            jQuery.browser.mobile = jQuery.browser.android || jQuery.browser.blackberry || jQuery.browser.ios || jQuery.browser.windowsMobile || jQuery.browser.operaMobile;


// trim the fullVersion string at semicolon/space if present
            if ((ix=jQuery.browser.fullVersion.indexOf(";"))!=-1)
                jQuery.browser.fullVersion=jQuery.browser.fullVersion.substring(0,ix);
            if ((ix=jQuery.browser.fullVersion.indexOf(" "))!=-1)
                jQuery.browser.fullVersion=jQuery.browser.fullVersion.substring(0,ix);

            jQuery.browser.majorVersion = parseInt(''+jQuery.browser.fullVersion,10);
            if (isNaN(jQuery.browser.majorVersion)) {
                jQuery.browser.fullVersion  = ''+parseFloat(navigator.appVersion);
                jQuery.browser.majorVersion = parseInt(navigator.appVersion,10);
            }
            jQuery.browser.version = jQuery.browser.majorVersion;
        }
        var browsers = {
            name:jQuery.browser.name,
            version:jQuery.browser.version,
        }
        return browsers;
    },
}

jQuery('doucment').ready(function(){
    LAUNCH.init();
});
