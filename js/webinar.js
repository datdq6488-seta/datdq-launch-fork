var WEBINAR = {
    init:function(){
        WEBINAR.ajaxLoadMore();
    },

    /**
     * Ajax Load more data page;
     **/
	ajaxLoadMore: function() {
		var page = 0, result;
		if (jQuery('#current-page').val()) {
			page = jQuery('#current-page').val();
		}
		AJAX.ajaxCallback = function() {
			if (AJAX.ajaxResult) {
				jQuery('#addtional_provider').append(AJAX.ajaxResult);
				jQuery('#current-page').val(parseInt(jQuery('#current-page').val()) + 1);
				if (jQuery('#current-page').val() === jQuery('#total-page').val()) {
					jQuery('.view_more').remove();
				}
				jQuery('#no-result').addClass('hide');
			} else {
				jQuery('#no-result').removeClass('hide');
			}
		};
		var sort = jQuery('#webinar_sort').val();
		var key = jQuery.trim(jQuery('#key').val());
		AJAX.ajaxData = {sort: sort, key: key};
		//console.log(AJAX.ajaxData);
		AJAX.ajaxData.page = parseInt(page) + 1;
		AJAX.ajaxAsync = false;
		AJAX.ajaxParseJSON = false;
		AJAX.ajaxUrl = PARAMS.baseUrl + '/webinars/loadMore';
		AJAX.request();
		return result;
	}
}

jQuery('document').ready(function() {
	//For list page
	if(jQuery('#total-page').val()){
		//Run ajax first time
		WEBINAR.init();
		//Click View More action
		jQuery('.book-load-more').click(function() {
			WEBINAR.ajaxLoadMore();
		});

		//Call function for dropdown sort by
		jQuery("#webinar_sort").msDropDown();
		jQuery("#date_range").msDropDown();
		
		//location.hash = 'Test';
		//Get request from Url
		var params = window.location.search;
		
		if(params !== ''){
			//if has params search or sort -> scroll to result block
			if(params.indexOf('sort') !== -1 || params.indexOf('key') !== -1){
				jQuery("body,html").animate({
					scrollTop: jQuery("#addtional_provider").offset().top - 10
				}, 200);	
			}
		}
		
	}
	
});