var VIDEO = {
	init: function() {
		VIDEO.checkAll();
		VIDEO.uncheckAll();
	},
	checkAll: function() {
		jQuery('.check-all').click(function() {
			var ul = (jQuery(this).attr('for')).toString();
			$(ul + ' :checkbox').prop('checked', true);
		});
	},
	uncheckAll: function() {
		jQuery('.uncheck-all').click(function() {
			var ul = (jQuery(this).attr('for')).toString();
			$(ul + ' :checkbox').prop('checked', false);
		});
	}
}

jQuery('document').ready(function() {
	VIDEO.init();
});