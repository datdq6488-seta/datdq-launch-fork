/**
 * Created by Pham Quoc Hung on 6/9/14.
 */
var SITEACCOUNT = {
    /**
     * Call all function in script;
     **/
    init:function(){
        SITEACCOUNT.blurEvents();
        SITEACCOUNT.changeTerms();
        SITEACCOUNT.submitCreateAccount();
    },

    /**
     * Event Blur check validate field form;
     **/
    blurEvents:function(){
        jQuery('#TTenant_email, #TTenant_full_name, #TTenant_business_name, #TTenant_dbu, #TTenant_e_dbpwd, #TTenant_re_password, #TTenant_domain').blur(function(){
            var objId, objCheck, _this=this,value, validate=true;
            value = jQuery.trim(jQuery(_this).val());
            objId = jQuery(_this).attr('id');
            if(objId == 'TTenant_email'){
                objCheck = SITEACCOUNT.validateEmail(value);
                if(!objCheck){
                    jQuery(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                    jQuery('label[for="TTenant_email"] .invalid-email').remove();
                    jQuery('label[for="TTenant_email"]').append('<span class="invalid-email">Invalid Email Address</span>');
                    validate = false;
                }else{
                    objCheck = SITEACCOUNT.ajaxObjCheck(value);
                    if(!objCheck){
                        jQuery(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                        jQuery('label[for="TTenant_email"] .invalid-email').remove();
                        jQuery('label[for="TTenant_email"]').append('<span class="invalid-email">Email already exists</span>');
                    }else{
                        jQuery('label[for="TTenant_email"] .invalid-email').remove();
                        jQuery(_this).removeClass('hight-light error-validate').addClass('ok-validate');
                    }

                }
            }else if(objId == 'TTenant_full_name'){
                jQuery('label[for="TTenant_full_name"] .invalid-email').remove();
                if(value.match(/\d/g)){
                    jQuery('label[for="TTenant_full_name"]').append('<span class="invalid-email">Invalid Full Name.</span>');
                    jQuery('#TTenant_full_name').removeClass('ok-validate').addClass('hight-light error-validate');
                }else if(value.length == 0){
                    jQuery('label[for="TTenant_full_name"]').append('<span class="invalid-email">Full Name cannot be blank</span>');
                    jQuery(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                }else if( value.length < 2 || value.length > 50 ){
                    jQuery('label[for="TTenant_full_name"]').append('<span class="invalid-email">The Full Name must be between 2 and 50 characters</span>');
                    jQuery(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                    validate = false;
                }else{
                    jQuery(_this).removeClass('hight-light error-validate').addClass('ok-validate');
                }
            }else if(objId == 'TTenant_business_name'){
                jQuery('label[for="TTenant_business_name"] .invalid-email').remove();
                if(value.length == 0){
                    jQuery('label[for="TTenant_business_name"]').append('<span class="invalid-email">Company cannot be blank</span>');
                    jQuery(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                }else if( value.length < 2 ){
                    jQuery('label[for="TTenant_business_name"]').append('<span class="invalid-email">The Company must from 2 characters</span>');
                    jQuery(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                    validate = false;
                }else{
                    jQuery(_this).removeClass('hight-light error-validate').addClass('ok-validate');
                }
            }else if(objId == 'TTenant_dbu'){
                jQuery('label[for="TTenant_dbu"] .invalid-email').remove();
                if(value.match(/\s/g)){
                    jQuery('label[for="TTenant_dbu"]').append('<span class="invalid-email">Invalid User Name.</span>');
                    jQuery(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                    validate = false;
                }else if(value.length == 0){
                    jQuery('label[for="TTenant_dbu"]').append('<span class="invalid-email">User name cannot be blank</span>');
                    jQuery(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                }else if( value.length < 4 || value.length > 24 ){
                    jQuery('label[for="TTenant_dbu"]').append('<span class="invalid-email">The user name must be between 4 and 24 characters</span>');
                    jQuery(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                    validate = false;
                }else{
                    objCheck = SITEACCOUNT.ajaxObjCheck(value);
                    if(!objCheck){
                        jQuery(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                        jQuery('label[for="TTenant_dbu"]').append('<span class="invalid-email">Account already exists</span>');
                    }else{
                        jQuery(_this).removeClass('hight-light error-validate').addClass('ok-validate');
                    }
                }

            }else if(objId == 'TTenant_e_dbpwd'){
                jQuery('label[for="TTenant_e_dbpwd"] .invalid-email').remove();
                jQuery('label[for="TTenant_e_dbpwd"] .invalid-strength').remove();
                if(value.length == 0){
                    jQuery('label[for="TTenant_e_dbpwd"]').append('<span class="invalid-email">Password cannot be blank</span>');
                    jQuery(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                }else if( value.length < 6 || value.length > 18 ){
                    jQuery('label[for="TTenant_e_dbpwd"]').append('<span class="invalid-email">Password must be between 6 and 18 characters</span>');
                    jQuery(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                    validate = false;
                }else{
                    objCheck = SITEACCOUNT.checkValidatePass(value);
                    jQuery(_this).removeClass('hight-light error-validate').addClass('ok-validate');
                }

            }else if(objId == 'TTenant_re_password'){
                if( value.length < 5 || value.length > 50){
                    jQuery(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                    validate = false;
                }else{
                    var pass = jQuery('#TTenant_e_dbpwd').val();
                    if(pass != value){
                        jQuery(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                        validate = false;
                    }else
                        jQuery(_this).removeClass('hight-light error-validate').addClass('ok-validate');
                }

            }else if(objId == 'TTenant_domain'){
                jQuery('label[for="TTenant_domain"] .invalid-email').remove();
                if(value.match(/\s/g)){
                    jQuery('label[for="TTenant_domain"]').append('<span class="invalid-email">Invalid Site.</span>');
                    jQuery(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                    validate = false;
                }else if(value.length == 0){
                    jQuery('label[for="TTenant_domain"]').append('<span class="invalid-email">Site cannot be blank</span>');
                    jQuery(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                    validate = false;
                }else if(value.length < 2 || value.length > 24){
                    jQuery('label[for="TTenant_domain"]').append('<span class="invalid-email">The site must be between 2 and 24 characters</span>');
                    jQuery(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                    validate = false;
                }else{
                    jQuery(_this).removeClass('hight-light error-validate').addClass('ok-validate');
                }
            }

            return validate;
        });
    },

    changeTerms:function(){
        jQuery('#TTenant_user_terms').change(function(){
            var term = jQuery(this).is(':checked');
            if(!term){
                jQuery('.user-field-terms .invalid-email').remove();
                jQuery('.user-field-terms').append('<span class="invalid-email">Please check the box to indicate your acceptance.</span>');
            }else{
                jQuery('.user-field-terms .invalid-email').remove();
            }
        });
    },
    /**
     * Check validate email;
     **/
    validateEmail:function(email){
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    },

    /**
     * Check password
     ***/
    checkValidatePass:function(value){
        var i=0;
        var j=0;
        var ch='';
        var checkUpperCase = 0;
        var checkLowerCase = 0;
        var checkNumCase = 0;
        var checIntNum = 0;
        var checkfmCase = 0;
        var checkValue1 = '';
        var checkValue2 = '';
        var checkValue3 = '';
        var patt1=/[0-9]/g;
        var patt2=/[a-zA-Z]/g;
        var patt3=/[@*&^%$#!]/g;
        var checkInt = value.match(patt1) == null ? 0: 1;
        var checkChart1 = value.match(patt1) == null ? '': value.match(patt1);
        var checkChart2 = value.match(patt2) == null ? '': value.match(patt2);
        var checkChart3 = value.match(patt3) == null ? '': value.match(patt3);

        if(checkChart1 != ''){
            var myString = new String(checkChart1);
            var checkValue1 = myString.split(',').join('');
        }
        if(checkChart2 != ''){
            var myString = new String(checkChart2);
            var checkValue2 = myString.split(',').join('');
        }
        if(checkChart3 != ''){
            var myString = new String(checkChart3);
            var checkValue3 = myString.split(',').join('');
        }
        if(checkChart1 !=''){
            checkNumCase = 1;
        }
        if(checkChart3 !=''){
            checkfmCase = 1;
        }
        while (j < checkValue2.length){
            character2 = checkValue2.charAt(j);
            if (character2 == character2.toUpperCase()) {
                checkUpperCase = 1;
            }
            if ( character2 == character2.toLowerCase() ){
                checkLowerCase = 1;
            }
            j++;
        }
        jQuery('label[for="TTenant_e_dbpwd"] .invalid-email').remove();
        jQuery('label[for="TTenant_e_dbpwd"] .invalid-strength.weak').remove();
        jQuery('label[for="TTenant_e_dbpwd"] .invalid-strength.moderate').remove();
        jQuery('label[for="TTenant_e_dbpwd"] .invalid-strength.strong').remove();
        if( (checkUpperCase == 1 && checkLowerCase == 1 && checkNumCase == 1) || (checkUpperCase == 1 && checkLowerCase == 1 && checkfmCase == 1) || (checkNumCase == 1 && checkLowerCase == 1 && checkfmCase == 1) || (checkNumCase == 1 && checkUpperCase == 1 && checkfmCase == 1) ){
            jQuery('label[for="TTenant_e_dbpwd"]').append('<span class="invalid-strength strong">Password strength: <span>strong</span></span>');
        }
        if( ((checkfmCase != 1) && ((checkNumCase == 1 && checkLowerCase == 0 && checkUpperCase == 1) || (checkNumCase == 1 && checkLowerCase == 1 && checkUpperCase == 0) || (checkUpperCase == 1 && checkLowerCase == 1 && checkNumCase == 0) || (checkUpperCase == 1 && checkLowerCase == 0 && checkNumCase == 1) || (checkUpperCase == 0 && checkLowerCase == 1 && checkNumCase == 1))) ||
            ((checkfmCase == 1) && ((checkNumCase == 1 && checkLowerCase == 0 && checkUpperCase == 0) || (checkNumCase == 0 && checkLowerCase == 1 && checkUpperCase == 0) || (checkUpperCase == 1 && checkLowerCase == 0 && checkNumCase == 0))) ){
            jQuery('label[for="TTenant_e_dbpwd"]').append('<span class="invalid-strength moderate">Password strength: <span>moderate</span></span>');
        }
        if( (checkfmCase != 1) && ((checkNumCase == 0 && checkLowerCase == 1 && checkUpperCase == 0) || (checkNumCase == 0 && checkLowerCase == 0 && checkUpperCase == 1) || (checkNumCase == 1 && checkLowerCase == 0 && checkUpperCase == 0)) ){
            jQuery('label[for="TTenant_e_dbpwd"]').append('<span class="invalid-strength weak">Password strength: <span>weak</span></span>');
        }

    },

    /**
     * Ajax check Email or Username already exist;
     **/
    ajaxObjCheck:function(value){
        var result;
        AJAX.ajaxCallback = function(){
            result = parseInt(AJAX.ajaxResult);
        };
        AJAX.ajaxData = {};
        AJAX.ajaxData.value = value;
        AJAX.ajaxAsync = false;
        AJAX.ajaxParseJSON = false;
        AJAX.ajaxUrl = PARAMS.baseUrl + '/user/checkobj';
        AJAX.request();
        return result;
    },

    /**
     * Check data after click submit form.
     **/
    submitCreateAccount:function(){
        jQuery('#whitelableId').click(function(){
            var _this=this,fullname,email,pass,rePass,company,accountType,username,term,captcha,validate=true;
            fullname = jQuery('#TTenant_full_name').val();
            email = jQuery('#TTenant_email').val();
            pass = jQuery('#TTenant_e_dbpwd').val();
            rePass = jQuery('#TTenant_re_password').val();
            company = jQuery('#TTenant_business_name').val();
            username = jQuery('#TTenant_dbu').val();
            term = jQuery('#TTenant_user_terms').is(':checked');
            captcha = jQuery('#recaptcha_response_field').val();

            // Full name check
            jQuery('label[for="TTenant_full_name"] .invalid-email').remove();
            if(jQuery.trim(fullname).length == 0){
                jQuery('label[for="TTenant_full_name"]').append('<span class="invalid-email">Full Name cannot be blank</span>');
                jQuery('#TTenant_full_name').removeClass('ok-validate').addClass('hight-light error-validate');
            }else if( jQuery.trim(fullname).length < 2 || jQuery.trim(username).fullname > 50 ){
                jQuery('label[for="TTenant_full_name"]').append('<span class="invalid-email">The Full Name must be between 2 and 50 characters</span>');
                jQuery('#TTenant_full_name').removeClass('ok-validate').addClass('hight-light error-validate');
                validate = false;
            }else{
                jQuery('#TTenant_full_name').removeClass('hight-light error-validate').addClass('ok-validate');
            }

            // Email check
            objCheck = SITEACCOUNT.validateEmail(email);
            jQuery('label[for="TTenant_email"] .invalid-email').remove();
            if(!objCheck){
                jQuery('#TTenant_email').removeClass('ok-validate').addClass('hight-light error-validate');
                jQuery('label[for="TTenant_email"]').append('<span class="invalid-email">Invalid Email Address</span>');
                validate = false;
            }else{
                objCheck = SITEACCOUNT.ajaxObjCheck(email);
                if(!objCheck){
                    jQuery('#TTenant_email').removeClass('ok-validate').addClass('hight-light error-validate');
                    jQuery('label[for="TTenant_email"]').append('<span class="invalid-email">Email already exists</span>');
                }else{
                    jQuery('#TTenant_email').removeClass('hight-light error-validate').addClass('ok-validate');
                }
            }

            // Company check
            jQuery('label[for="TTenant_business_name"] .invalid-email').remove();
            if(jQuery.trim(company).length == 0){
                jQuery('label[for="TTenant_business_name"]').append('<span class="invalid-email">Company cannot be blank</span>');
                jQuery('#TTenant_business_name').removeClass('ok-validate').addClass('hight-light error-validate');
            }else if( jQuery.trim(company).length < 2 ){
                jQuery('label[for="TTenant_business_name"]').append('<span class="invalid-email">From 2 characters</span>');
                jQuery('#TTenant_business_name').removeClass('ok-validate').addClass('hight-light error-validate');
                validate = false;
            }else{
                jQuery('#TTenant_business_name').removeClass('hight-light error-validate').addClass('ok-validate');
            }

            // Username Check
            jQuery('label[for="TTenant_dbu"] .invalid-email').remove();
            if(jQuery.trim(username).match(/\s/g)){
                jQuery('label[for="TTenant_dbu"]').append('<span class="invalid-email">Invalid User Name.</span>');
                jQuery('#TTenant_dbu').removeClass('ok-validate').addClass('hight-light error-validate');
            }else if(jQuery.trim(username).length == 0){
                jQuery('label[for="TTenant_dbu"]').append('<span class="invalid-email">User name cannot be blank</span>');
                jQuery('#TTenant_dbu').removeClass('ok-validate').addClass('hight-light error-validate');
            }else if( jQuery.trim(username).length < 4 || jQuery.trim(username).length > 24 ){
                jQuery('label[for="TTenant_dbu"]').append('<span class="invalid-email">The user name must be between 4 and 24 characters</span>');
                jQuery('#TTenant_dbu').removeClass('ok-validate').addClass('hight-light error-validate');
                validate = false;
            }else{
                objCheck = SITEACCOUNT.ajaxObjCheck(username);
                if(!objCheck){
                    jQuery('#TTenant_dbu').removeClass('ok-validate').addClass('hight-light error-validate');
                    jQuery('label[for="TTenant_dbu"]').append('<span class="invalid-email">Account already exists</span>');
                }else{
                    jQuery('#TTenant_dbu').removeClass('hight-light error-validate').addClass('ok-validate');
                }
            }

            // Pass Check
            jQuery('label[for="TTenant_e_dbpwd"] .invalid-strength').remove();
            jQuery('label[for="TTenant_e_dbpwd"] .invalid-email').remove();
            if(jQuery.trim(pass).length == 0){
                jQuery('label[for="TTenant_e_dbpwd"]').append('<span class="invalid-email">Password cannot be blank</span>');
                jQuery('#TTenant_e_dbpwd').removeClass('ok-validate').addClass('hight-light error-validate');
            }else if( jQuery.trim(pass).length < 6 || jQuery.trim(pass).length > 18 ){
                jQuery('label[for="TTenant_e_dbpwd"]').append('<span class="invalid-email">Password must be between 6 and 18 characters</span>');
                jQuery('#TTenant_e_dbpwd').removeClass('ok-validate').addClass('hight-light error-validate');
                validate = false;
            }else{
                var objCheck = SITEACCOUNT.checkValidatePass(jQuery.trim(pass));
                jQuery('#TTenant_e_dbpwd').removeClass('hight-light error-validate').addClass('ok-validate');
            }

            // Re Pass check
            if( jQuery.trim(rePass).length < 6 || jQuery.trim(rePass).length > 18 ){
                jQuery('#TTenant_re_password').removeClass('ok-validate').addClass('hight-light error-validate');
                validate = false;
            }else{
                if(jQuery.trim(pass) != jQuery.trim(rePass)){
                    jQuery('#TTenant_re_password').removeClass('ok-validate').addClass('hight-light error-validate');
                    validate = false;
                }else
                    jQuery('#TTenant_re_password').removeClass('hight-light error-validate').addClass('ok-validate');
            }

            // Check Captcha
            if(jQuery.trim(captcha).length ==0){
                jQuery('.user-field-captcha').append('<div class="alert alert-error">The verification code is incorrect.</div>');
                validate = false;
            }

            // Check Terms
            if(!term){
                jQuery('.user-field-terms .invalid-email').remove();
                jQuery('.user-field-terms').append('<span class="invalid-email">Please check the box to indicate your acceptance.</span>');
                validate = false;
            }
            return validate;
        });
    }

}
jQuery('document').ready(function($){
    SITEACCOUNT.init();
});