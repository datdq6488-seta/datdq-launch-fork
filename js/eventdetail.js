var EVENTDETAIL = {
	init: function() {
		// Define your locations: HTML content for the info window, latitude, longitude
		//Get Event Data
		var events = COREMAP.getEventData(PARAMS.baseUrl + '/events/getEventLocation/type/detail');
		COREMAP.init(events, "map-detail");
	}
}
jQuery('document').ready(function() {
	EVENTDETAIL.init();
});
