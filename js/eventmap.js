var EVENTMAP = {
	init: function() {
		var mark_id = jQuery("#mark-id").val();
		var href = PARAMS.baseUrl + '/events/getEventLocation';
		if (mark_id != ""){
			href += '/mark/'+mark_id;
		}
		// Define your locations: HTML content for the info window, latitude, longitude
		//Get Event Data
		var events = COREMAP.getEventData(href);
		COREMAP.init(events, "google-map");
	}
}
jQuery('document').ready(function() {
	EVENTMAP.init();
});
