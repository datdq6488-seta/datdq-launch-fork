<div class="white_box">
    <div class="white_box_title">
        <h4>Create a Site</h4>
    </div>
    <div class="white_box_body">
        <div class="fld_row">
            <label for="">Fullname</label>
            <input class="ipt_box ok-validate" type="text" name="name" />
        </div>

        <div class="fld_row">
            <label for="">Company <span>(optional)</span></label>
            <input class="ipt_box ok-validate" type="text" name="name" />
        </div>

        <div class="fld_row">
            <label for="">
                Email Address
                <span class="invalid-email">Invalid Email Address</span>
            </label>
            <input class="ipt_box hight-light error-validate" type="text" name="name" />
        </div>

        <div class="fld_row">
            <label for="">Password</label>
            <input class="ipt_box" type="password" name="name" />
        </div>

        <div class="fld_row">
            <label for="">Confirm Password</label>
            <input class="ipt_box" type="password" name="name" />
        </div>

        <div class="fld_row">
            <label for="">Site</label>
            <span class="site">http:// </span><input type="text" name="name" /><span class="site"> launchproject.com</span>
        </div>

        <div class="fld_row">
            <label for="">Company Website</label>
            <input class="ipt_box" type="text" name="name" />
        </div>

        <div class="line_box"></div>

        <div class="white_captcha">
            <?php
            //echo $form->labelEx($model, 'validation');
            $this->widget('application.extensions.recaptcha.EReCaptcha', array('attribute' => 'validateCapt',
                'theme' => 'red', 'language' => 'en_US',
                'publicKey' => Yii::app()->params['recaptcha']['publicKey']));            
            ?>
        </div>

        <div class="user_agree">
            <input type="checkbox" name="name" checked="checked" />
            I agree to the <a href="#">terms and conditions</a>
        </div>

        <div class="something">
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna 
                aliqua. Ut enim ad minim veniam.
            </p>
        </div>                
    </div>
    <div class="white_box_foot">
        <a href="#" class="btn_ac">Sign Up</a>
    </div>
</div>

