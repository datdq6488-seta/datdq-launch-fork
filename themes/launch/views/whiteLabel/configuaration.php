<!--view of Site Configuaration-->
<div class="wrap clearfix">
    <div class="row">    
        <div class="span3">
            <div class="main_menu">
                <ul>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/whiteLabel/dashboard">Dashboard</a></li>
                    <li class="m_active"><a href="<?php echo Yii::app()->request->baseUrl; ?>">Site Configuaration</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Activity</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Users</a></li>
                </ul>
            </div>
        </div>

        <div class="span9">
            <div class="wrap_container">                
                <div class="frame">
                    <div class="header_frame clearfix">
                        <h4 class="pull-left">Site Configuaration</h4>
                        <a href="#" class="pull-right btn_act">Save</a>
                        <a href="#" class="pull-right btn_cancel">Cancel</a>
                    </div>
                    <div class="frame_body">
                        <div class="inner_body">
                            <div class="wrap_site_confi">
                                <div class="fld_row">
                                    <label for="">Entrepreneur Site</label>
                                    <span class="site">http:// </span><input type="text" name="name" value="Yahoo" /><span class="site"> launchproject.com</span>
                                </div>

                                <div class="fld_row">
                                    <label for="">Site Name</label>
                                    <input class="ipt_box" type="text" name="name" value="Yahoo" />
                                </div>     

                                <div class="fld_row">
                                    <label for="">Industry</label>
                                    <select class="selectpicker dd_list" name="" id="whitelabel_Industry">
                                        <option value="1">Consumer Online Portal</option>
                                        <option value="2">Consumer Online Portal</option>
                                        <option value="3">Consumer Online Portal</option>
                                    </select>
                                </div> 
                            </div>  
                            <div class="fld_row" style="margin-bottom: 0">
                                <label for="">Logo</label>
                                <div class="profile-photo-wrapper">
                                    <div class="photo-logo-image">
                                        <div class="load-process"><span class="background-media-proccess-logo"></span></div>
                                        <a href="#" title=""><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/files/logo/yahoo.png" alt="" /></a>
                                    </div>
                                    <div class="field-row-avatar">
                                        <div class="avatar-profile-file">
                                            <input name="avatar" id="config_avatar" type="file">
                                            <button type="button" name="avatarProfile" id="avatarProfile"><?php echo Yii::t('_yii', 'Browse or drag an image here...') ?></button>
                                            <span id="nameFile-Profile"></span>
                                        </div>
                                        <div class="intro-photo-profile"><?php echo Yii::t('_yii', 'Photos should be at least 600px by 250px and in PNG, JPG, or GIF format.') ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    </div>
                </div>

                <div class="frame">
                    <div class="header_frame clearfix">
                        <h4 class="pull-left">Theme Manager</h4>
                        <a href="#" class="pull-right btn_act">Save</a>
                        <a href="#" class="pull-right btn_cancel">Cancel</a>
                    </div>
                    <div class="frame_body">
                        <div class="inner_body">
                            <div class="coming_soon"><span>coming soon</span></div>                                                        
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
