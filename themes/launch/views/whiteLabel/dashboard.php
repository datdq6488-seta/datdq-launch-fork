<!--view of dashboard-->
<div class="wrap clearfix">
    <div class="row">    
        <div class="span3">
            <div class="main_menu">
                <ul>
                    <li class="m_active"><a href="<?php echo Yii::app()->request->baseUrl; ?>/whiteLabel/dashboard">Dashboard</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Site Configuaration</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Activity</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Users</a></li>
                </ul>
            </div>
        </div>

        <div class="span9">
            <div class="wrap_container">                
                <div class="frame">
                    <div class="header_frame clearfix">
                        <h4 class="pull-left">Dashboard</h4>
                    </div>
                    <div class="frame_body">
                        <div class="inner_body">
                            <div class="clearfix">
                                <div class="item_dashboard pull-left">
                                    <div class="header_frame it_head clearfix">
                                        <h4>Total Sign-ups</h4>
                                    </div>
                                    <div class="body_db">
                                        <div class="stt">
                                            <p>5,355</p>
                                            <span>since August 5,2013</span>
                                        </div>                                        
                                    </div>
                                </div>

                                <div class="item_dashboard pull-right">
                                    <div class="header_frame it_head clearfix">
                                        <h4>Users Activity</h4>
                                    </div>
                                    <div class="body_db">
                                        <div class="inner_body_db">
                                            <table class="table table-striped tbl_dashboard">                                            
                                                <tbody>
                                                    <tr>
                                                        <td>20 seconds ago</td>
                                                        <td>Roodney Meyers Signed up</td>
                                                    </tr>
                                                    <tr>
                                                        <td>20 seconds ago</td>
                                                        <td>Roodney Meyers Signed up</td>
                                                    </tr>
                                                    <tr>
                                                        <td>20 seconds ago</td>
                                                        <td>Roodney Meyers Signed up</td>
                                                    </tr>
                                                    <tr>
                                                        <td>20 seconds ago</td>
                                                        <td>Roodney Meyers Signed up</td>
                                                    </tr>
                                                    <tr>
                                                        <td>20 seconds ago</td>
                                                        <td>Roodney Meyers Signed up</td>
                                                    </tr>
                                                    <tr>
                                                        <td>20 seconds ago</td>
                                                        <td>Roodney Meyers Signed up</td>
                                                    </tr><tr>
                                                        <td>20 seconds ago</td>
                                                        <td>Roodney Meyers Signed up</td>
                                                    </tr>
                                                    <tr>
                                                        <td>20 seconds ago</td>
                                                        <td>Roodney Meyers Signed up</td>
                                                    </tr>
                                                    <tr>
                                                        <td>20 seconds ago</td>
                                                        <td>Roodney Meyers Signed up</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="frame t_margin" style="margin-bottom: 0">
                                <div class="header_frame it_head clearfix">
                                    <h4>Top 10 Users</h4>
                                </div>
                                <div class="frame_body">
                                    <div class="inner_body" style="padding: 0 15px;">
                                        <table class="table table-striped tbl_top">                                            
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Name</th>
                                                    <th>Email Address</th>
                                                    <th>Points</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>John Moore</td>
                                                    <td>emailaddress@gmail.com</td>
                                                    <td>10,245</td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>John Moore</td>
                                                    <td>emailaddress@gmail.com</td>
                                                    <td>10,245</td>
                                                </tr>
                                                <tr>
                                                    <td>3</td>
                                                    <td>John Moore</td>
                                                    <td>emailaddress@gmail.com</td>
                                                    <td>10,245</td>
                                                </tr>
                                                <tr>
                                                    <td>4</td>
                                                    <td>John Moore</td>
                                                    <td>emailaddress@gmail.com</td>
                                                    <td>10,245</td>
                                                </tr>
                                                <tr>
                                                    <td>5</td>
                                                    <td>John Moore</td>
                                                    <td>emailaddress@gmail.com</td>
                                                    <td>10,245</td>
                                                </tr>
                                                <tr>
                                                    <td>6</td>
                                                    <td>John Moore</td>
                                                    <td>emailaddress@gmail.com</td>
                                                    <td>10,245</td>
                                                </tr>
                                                <tr>
                                                    <td>7</td>
                                                    <td>John Moore</td>
                                                    <td>emailaddress@gmail.com</td>
                                                    <td>10,245</td>
                                                </tr>
                                                <tr>
                                                    <td>8</td>
                                                    <td>John Moore</td>
                                                    <td>emailaddress@gmail.com</td>
                                                    <td>10,245</td>
                                                </tr>
                                                <tr>
                                                    <td>9</td>
                                                    <td>John Moore</td>
                                                    <td>emailaddress@gmail.com</td>
                                                    <td>10,245</td>
                                                </tr>
                                                <tr>
                                                    <td>10</td>
                                                    <td>John Moore</td>
                                                    <td>emailaddress@gmail.com</td>
                                                    <td>10,245</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>                        
                                </div>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
