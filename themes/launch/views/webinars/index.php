<?php
/* @var $this WebinarController */

$this->breadcrumbs = array(
    'Webinar',
);
?>
<div class="row">
    <div class="col-md-12">                        
        <div class="ask_content content_expert">                       
            <div class="head_box_line clearfix">
                <div class="row">
                    <input type="hidden" name="total-page" id="total-page" value="<?php echo isset($totalPage) ? $totalPage : 1; ?>"/>
                    <div class="col-md-3">
                        <span class="title_for_box"><?php echo Yii::t('_yii', 'Live Webinar'); ?></span>
                    </div>
                    <div class="col-md-9">
                        <form class="form-inline pull-right m_top" role="form">
                            <div class="form-group">
                                <label class="inline_filter_bar" for="exampleInputEmail2"><?php echo Yii::t('_yii', 'Sort by:'); ?></label>
                            </div>
                            <div class="form-group group_m_right">                                
                                <?php echo CHtml::dropDownList('sort', $selectedSort, $sortBy, array('id' => 'webinar_sort', 'onchange' => 'this.form.submit()')); ?>                                
                            </div>
                            <div class="form-group">
                                <label class="inline_filter_bar" for="exampleInputEmail2"><?php echo Yii::t('_yii', 'Date Range:'); ?></label>
                            </div>
                            <div class="form-group group_m_right">                                
                                <?php echo CHtml::dropDownList('date_range', '', array(), array('id' => 'date_range')); ?>                                
                            </div>
                            <div class="form-group">
                                <label class="inline_filter_bar" for=""><?php echo Yii::t('_yii', 'Search:'); ?></label>
                            </div>
                            <div class="form-group">
                                <div class="input-group">                                                    
                                    <?php echo Chtml::textField('key', $keySearch, array('class' => 'form-control re_ipt_search')); ?>
                                    <span class="input-group-btn">                                        
                                        <button type="submit" class="btn btn-default re_button" id="submit-search"><span class="glyphicon glyphicon-search"></span></button>
                                    </span>
                                </div>
                            </div>                      
                        </form>
                    </div>
                </div>
            </div>

            <div class="list_item_expert bg_sponsored">
                <h3 class="tt_list_expert"><?php echo Yii::t('_yii', 'Sponsored By:') ?></h3>

                <?php echo $this->renderPartial('_item_sponsored', array('modelSponsored' => $modelSponsored)); ?>
            </div>

            <div class="line_box_client"></div>

            <input type="hidden" name="current-page" id="current-page" value="<?php echo isset($currentPage) ? $currentPage : 1; ?>"/>

            <div class="list_item_expert" id="addtional_provider">
                <h3 class="tt_list_expert"><?php echo Yii::t('_yii', 'Additional Providers:') ?></h3>
                <div class="alert alert-danger hide" role="alert" id="no-result">
                    <?php echo Yii::t('_yii', 'No search result found.'); ?>
                </div>
            </div>

            <div class="view_more">
                <a href="javascript:void(0)" class="book-load-more"><?php echo Yii::t('_yii', 'View more'); ?></a>
            </div>
        </div>
    </div>
</div>
