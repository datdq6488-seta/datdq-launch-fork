<?php
/* @var $this WebinarController */

$this->breadcrumbs = array(
    'Webinar Detail',
);
?>
<div class="row">
    <div class="col-md-4">
        <div class="ask_avatar">
            <div class="inner_avatar">
                <img class="img-responsive" src="<?php echo BASE_URL; ?>/images/files/webinar/pro_webbinar.png" alt="" />
            </div>            

            <div class="l_btn_contacted">
                <ul class="btn_ct">
                    <li><a class="btn btn-primary btn-block" href="javascript:void(0)"><?php echo Yii::t('_yii', 'Webinar Registration'); ?></a></li>
                    <li><a class="btn btn-default btn-block" href="<?php echo BASE_URL; ?>/webinars" class="view-more"><?php echo Yii::t('_yii', 'View More Webinars'); ?></a></li>
                </ul>                                
            </div>           
        </div>
    </div>

    <div class="col-md-8">
        <div class="box_expert_detai">
            <div class="box_content line clearfix">                
                <h4 class="head_title pull-left"><?php echo $model->title; ?></h4>

                <div class="series_webbinar pull-right">
                    <a href="#"><?php echo Yii::t('_yii', 'Series'); ?></a>
                </div>                
            </div>
            <div style="padding: 20px">
                <div class="box_content clearfix">
                    <h4 class="box_title"><?php echo Yii::t('_yii', 'Webinar Details'); ?></h4>
                    <div class="author-detail"><strong><?php echo $model->approved_date; ?>&nbsp;EST</strong></div>
                    <div class="author-detail"><strong><?php echo Yii::t('_yii', 'Posted by'); ?>:</strong> <a href="<?php echo Yii::app()->request->baseUrl; ?>/authors/detail"><?php echo $model->author; ?></a></div>
                </div>

                <div class="box_content clearfix">
                    <h4 class="box_title"><?php echo Yii::t('_yii', 'Infomation'); ?></h4>
                    <p class="graphy_content">
                        <?php echo $model->description; ?>
                    </p>
                </div>                

            </div>
        </div>
    </div>
</div>