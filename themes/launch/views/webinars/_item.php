<?php foreach ($model as $row):?>
<div class="media expert_item clearfix">
	<a href="<?php echo BASE_URL; ?>/webinars/detail/<?php echo $row->id;?>" class="pull-left media-img-webinar ask_shadow">
            <img class="img-responsive" src="<?php echo BASE_URL; ?>/images/files/webinar/webinar-item.png" alt="" />
	</a>
	<div class="media-body">
		<div class="it_book_detail">
			<h5 class="media-heading"><a href="<?php echo BASE_URL; ?>/webinars/detail/<?php echo $row->id;?>"><?php echo $row->title; ?></a></h5>
			<span><?php echo Yii::app()->dateFormatter->format("MMM dd, yyyy - h:m a", $row->approved_date) ; ?>&nbsp;EST</span>
			<p class="description_book">
				<?php echo StringUtil::cutText($row->description, 330); ?>
				<a href="<?php echo BASE_URL;?>/webinars/detail/<?php echo $row->id;?>" class="view-more-item"><?php echo Yii::t('_yii', 'more');?></a>
			</p>                            
		</div>                        
	</div>
</div>
<?php endforeach;?>