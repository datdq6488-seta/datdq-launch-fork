<?php
/* @var $this BookstoreController */

$this->breadcrumbs=array(
	'Bookstore',
);
?>
<div id="bookstore" class="l-container container">
    <div class="main-bookstore">
        <div class="wrapper-bookstore">
            <div class="header-bookstore-page">
                <h3><?php echo Yii::t('_yii','Live Events');?></h3>
				<div class="filter-type">
					<a href="<?php echo Yii::app()->request->baseUrl;?>/events/map" class="map-view">Map View</a>
					<a href="<?php echo Yii::app()->request->baseUrl;?>/events" class="list-view">List View</a>
				</div>
                <div class="info-search-filter">
                    <form name="search-book" id="search-book" action="" method="get">
                        <div class="filter-sort">
                            <label><?php echo Yii::t('_yii','Sort by:');?></label>
                            <select name="sort" id="sortbook">
                                <option value="title-asc"><?php echo Yii::t('_yii','Date (newest first)');?></option>
                            </select>
                        </div>
						<div class="filter-sort">
                            <label><?php echo Yii::t('_yii','Date Range:');?></label>
                            <select name="sort" id="sortbook">
                                <option value="title-asc"><?php echo Yii::t('_yii','None');?></option>
                            </select>
                        </div>
                        <div class="filter-search">
                            <div>
                                <label><?php echo Yii::t('_yii','Search:');?></label>
                                <input type="text" name="key" class="key-search-input" value="<?php echo isset($_GET['key'])?$_GET['key']:'';?>">
                                <button type="submit" class="submit-search" id="submit-search"><?php echo Yii::t('_yii','Search');?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
			<input id="mark-id" type="hidden" value="<?php echo $mark;?>"/>
            <div class="content-map-page">
				<div class="google-map" id="google-map"></div>
            </div>
        </div>
    </div>
</div>