
<?php $this->pageTitle = Yii::app()->name; ?>

<div class="hero-unit visible-desktop">
    <h1>Welcome to <em><?php echo CHtml::encode(Yii::app()->name); ?></em></h1>
    <p>You may change the content of this page by modifying the following two files:</p>
    <ul>
        <li>View file: <tt><?php echo __FILE__; ?></tt></li>
        <li>Layout file: <tt><?php echo $this->getLayoutFile('main'); ?></tt></li>
    </ul>
</div>