<!--View for managerpage-->
<div class="wrap clearfix">
    <div class="row">    
        <div class="span3">
            <div class="main_menu">
                <ul>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Dashboard</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Pages</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Steps</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Quotes</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Videos</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Special Offers</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Books</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Referrals</a></li>                    
                </ul>
                <h4 class="menu_title">Access</h4>
                <ul>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Entrepreneurs</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Content Providers</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Partners</a></li>                                      
                </ul>
                <h4 class="menu_title">Events</h4>
                <ul>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Webinars</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Live Events</a></li>                                                         
                </ul>
                <h4 class="menu_title">Admin</h4>
                <ul>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Email Manager</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Notifications Manager</a></li>
                    <li class="m_active"><a href="<?php echo Yii::app()->request->baseUrl; ?>">Template Design</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Menu Links</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Access Manager</a></li>                    
                </ul>
            </div>
        </div>

        <div class="span9">
            <div class="wrap_container">               
                <div class="frame">
                    <div class="header_frame clearfix">
                        <h4 class="pull-left">Template Design</h4>
                        <a href="#" class="pull-right btn_ac" style="font-weight: normal">Save</a>
                    </div>
                    <div class="frame_body">
                        <div class="inner_body clearfix">
                            <div class="wr_bd">
                                <div class="fld_row" style="margin-bottom: 0">
                                    <label for="">Logo</label>
                                    <div class="profile-photo-wrapper">
                                        <div class="photo-logo-avatar">
                                            <div class="load-process"><span class="background-media-proccess-logo"></span></div>
                                            <a href="#" title=""><img src="" alt="" /></a>
                                        </div>
                                        <div class="field-row-avatar">
                                            <div class="avatar-profile-file">
                                                <input name="avatar" id="logo_image" type="file">
                                                <button type="button" name="avatarProfile" id="avatarProfile"><?php echo Yii::t('_yii', 'Browse or drag an image here...') ?></button>
                                                <span id="nameFile-Profile"></span>
                                            </div>
                                            <div class="intro-photo-profile"><?php echo Yii::t('_yii', 'Photos should be at least 170px by 50px and in PNG, JPG, or GIF format.') ?></div>
                                        </div>
                                    </div>
                                </div>
								
                                <div class="frame t_margin" style="margin-bottom: 0;margin-top: 40px;">
                                    <div class="header_frame clearfix">
                                        <h4>Color Manager</h4>
                                    </div>
                                    <div class="frame_body">
                                        <div class="color_manager clearfix">
                                            <div class="man_left pull-left">
                                                <div class="color_man_row clearfix input-group page-bg">
                                                    <label for="">Page Background</label>
                                                    <input type="text" name="name" value="" class="form-control" />
                                                    <div class="gen"><div class="gen_page_bg input-group-addon"></div></div>
                                                </div>
                                                <div class="color_man_row clearfix input-group page-font">
                                                    <label for="">Page Font</label>
                                                    <input type="text" name="name" value="" />
                                                    <div class="gen"><div class="gen_page_font input-group-addon"></div></div>
                                                </div>
                                                <div class="color_man_row clearfix input-group page-link">
                                                    <label for="">Links</label>
                                                    <input type="text" name="name" value="" />
                                                    <div class="gen"><div class="gen_page_link input-group-addon"></div></div>
                                                </div>
                                                <div class="color_man_row clearfix input-group section-title">
                                                    <label for="">Section Title</label>
                                                    <input type="text" name="name" value="" />
                                                    <div class="gen"><div class="gen_sec_title input-group-addon"></div></div>
                                                </div>

                                                <div class="color_man_row clearfix input-group header-bg" style="margin-top: 40px;">
                                                    <label for="">Header Background</label>
                                                    <input type="text" name="name" value="" />
                                                    <div class="gen"><div class="gen_header_bg input-group-addon"></div></div>
                                                </div>
                                                <div class="color_man_row clearfix input-group header-font">
                                                    <label for="">Header Font</label>
                                                    <input type="text" name="name" value="" />
                                                    <div class="gen"><div class="gen_header_font input-group-addon"></div></div>
                                                </div>
                                            </div>
                                            <div class="color_preview pull-right">
                                                <div class="design_preview">
                                                    <h4>Preview</h4>
                                                    <div class="layouts">
                                                        <div class="inner_layout">
                                                            <div class="layouts_header clearfix">
                                                                <div class="layouts_logo pull-left"></div>
                                                                <div class="navvigate pull-right">
                                                                    <ul class="clearfix">
                                                                        <li></li>
                                                                        <li></li>
                                                                        <li></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <div class="layouts_content">
                                                                <div class="section_title"><h6>section title</h6></div>
                                                                <div class="clearfix">
                                                                    <div class="left_content pull-left"></div>
                                                                    <div class="right_content pull-right">
                                                                        <ul class="r_sb">
                                                                            <li></li>
                                                                            <li></li>
                                                                            <li></li>
                                                                        </ul>
                                                                        <div class="links"></div>
                                                                    </div>
                                                                </div>                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>                        
                                    </div>
                                </div>
                            </div>
                        </div>                                              
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>