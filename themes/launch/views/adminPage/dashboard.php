<div class="wrap clearfix">
    <div class="row">    
        <div class="span3">
            <div class="main_menu">
                <ul>
                    <li class="m_active"><a href="<?php echo Yii::app()->request->baseUrl; ?>">Dashboard</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Pages</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Steps</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Quotes</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Videos</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Special Offers</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Books</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Referrals</a></li>                    
                </ul>
                <h4 class="menu_title">Access</h4>
                <ul>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Entrepreneurs</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Content Providers</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Partners</a></li>                                      
                </ul>
                <h4 class="menu_title">Events</h4>
                <ul>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Webinars</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Live Events</a></li>                                                         
                </ul>
                <h4 class="menu_title">Admin</h4>
                <ul>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Email Manager</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Notifications Manager</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Template Design</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Menu Links</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Access Manager</a></li>                    
                </ul>
            </div>
        </div>

        <div class="span9">
            <div class="wrap_container">               
                <div class="frame">
                    <div class="header_frame clearfix">
                        <h4 class="pull-left">Dashboard</h4>
                        <a href="#" class="pull-right btn_act">Save</a>
                    </div>
                    <div class="frame_body">
                        <div class="inner_body clearfix">
                            <div class="clearfix">
                                <div class="item_dashboard pull-left">
                                    <div class="header_frame it_head clearfix">
                                        <h4>Sign Ups</h4>
                                    </div>
                                    <div class="body_db" style="height: 350px;">
                                        <div class="inner_body_db">
                                            <table class="table table-striped tbl_dashboard">                                            
                                                <tbody>
                                                    <tr>
                                                        <td>20 seconds ago</td>
                                                        <td><span>Roodney Meyers</span> Signed up</td>
                                                    </tr>
                                                    <tr>
                                                        <td>20 seconds ago</td>
                                                        <td><span>Roodney Meyers</span> Signed up</td>
                                                    </tr>
                                                    <tr>
                                                        <td>20 seconds ago</td>
                                                        <td><span>Roodney Meyers</span> Signed up</td>
                                                    </tr>
                                                    <tr>
                                                        <td>20 seconds ago</td>
                                                        <td><span>Roodney Meyers</span> Signed up</td>
                                                    </tr>
                                                    <tr>
                                                        <td>20 seconds ago</td>
                                                        <td><span>Roodney Meyers</span> Signed up</td>
                                                    </tr>
                                                    <tr>
                                                        <td>20 seconds ago</td>
                                                        <td><span>Roodney Meyers</span> Signed up</td>
                                                    </tr><tr>
                                                        <td>20 seconds ago</td>
                                                        <td><span>Roodney Meyers</span> Signed up</td>
                                                    </tr>
                                                    <tr>
                                                        <td>20 seconds ago</td>
                                                        <td><span>Roodney Meyers</span> Signed up</td>
                                                    </tr>
                                                    <tr>
                                                        <td>20 seconds ago</td>
                                                        <td><span>Roodney Meyers</span> Signed up</td>
                                                    </tr>
                                                    <tr>
                                                        <td>20 seconds ago</td>
                                                        <td><span>Roodney Meyers</span> Signed up</td>
                                                    </tr><tr>
                                                        <td>20 seconds ago</td>
                                                        <td><span>Roodney Meyers</span> Signed up</td>
                                                    </tr>
                                                    <tr>
                                                        <td>20 seconds ago</td>
                                                        <td><span>Roodney Meyers</span> Signed up</td>
                                                    </tr>
                                                    <tr>
                                                        <td>20 seconds ago</td>
                                                        <td><span>Roodney Meyers</span> Signed up</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="item_dashboard pull-right">
                                    <div class="header_frame it_head clearfix">
                                        <h4>Waiting Approval</h4>
                                    </div>
                                    <div class="item_db_body">
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs dashboard_tab" role="tablist" style="padding: 5px 5px 0;margin-bottom: 0">
                                            <li class="active"><a href="#w_label" role="tab" data-toggle="tab">White Label</a></li>
                                            <li><a href="#c_provider" role="tab" data-toggle="tab">Content Provider</a></li>                                            
                                        </ul>
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div class="tab-pane fade in active" id="w_label">
                                                <div class="body_db" style="height: 310px;">
                                                    <div class="inner_body_db" style="padding-top: 0">
                                                        <table class="table table-striped tbl_approval">                                            
                                                            <tbody>
                                                                <tr>
                                                                    <td>Rooney mayer</td>
                                                                    <td class="pull-right"><span>approve</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Rooney mayer</td>
                                                                    <td class="pull-right"><span>approve</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Rooney mayer</td>
                                                                    <td class="pull-right"><span>approve</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Rooney mayer</td>
                                                                    <td class="pull-right"><span>approve</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Rooney mayer</td>
                                                                    <td class="pull-right"><span>approve</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Rooney mayer</td>
                                                                    <td class="pull-right"><span>approve</span></td>
                                                                </tr><tr>
                                                                    <td>Rooney mayer</td>
                                                                    <td class="pull-right"><span>approve</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Rooney mayer</td>
                                                                    <td class="pull-right"><span>approve</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Rooney mayer</td>
                                                                    <td class="pull-right"><span>approve</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Rooney mayer</td>
                                                                    <td class="pull-right"><span>approve</span></td>
                                                                </tr><tr>
                                                                    <td>Rooney mayer</td>
                                                                    <td class="pull-right"><span>approve</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Rooney mayer</td>
                                                                    <td class="pull-right"><span>approve</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Rooney mayer</td>
                                                                    <td class="pull-right"><span>approve</span></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="c_provider">
                                                <div class="body_db" style="height: 310px;">
                                                    <div class="inner_body_db" style="padding-top: 0">
                                                        <table class="table table-striped tbl_approval">                                            
                                                            <tbody>
                                                                <tr>
                                                                    <td>Rooney mayer</td>
                                                                    <td class="pull-right"><span>approve</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Rooney mayer</td>
                                                                    <td class="pull-right"><span>approve</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Rooney mayer</td>
                                                                    <td class="pull-right"><span>approve</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Rooney mayer</td>
                                                                    <td class="pull-right"><span>approve</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Rooney mayer</td>
                                                                    <td class="pull-right"><span>approve</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Rooney mayer</td>
                                                                    <td class="pull-right"><span>approve</span></td>
                                                                </tr><tr>
                                                                    <td>Rooney mayer</td>
                                                                    <td class="pull-right"><span>approve</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Rooney mayer</td>
                                                                    <td class="pull-right"><span>approve</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Rooney mayer</td>
                                                                    <td class="pull-right"><span>approve</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Rooney mayer</td>
                                                                    <td class="pull-right"><span>approve</span></td>
                                                                </tr><tr>
                                                                    <td>Rooney mayer</td>
                                                                    <td class="pull-right"><span>approve</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Rooney mayer</td>
                                                                    <td class="pull-right"><span>approve</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Rooney mayer</td>
                                                                    <td class="pull-right"><span>approve</span></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>                                            
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix" style="margin-top: 30px;">
                                <div class="item_dashboard pull-left">
                                    <div class="header_frame it_head clearfix">
                                        <h4>Achievement</h4>
                                    </div>
                                    <div class="body_db" style="height: 203px;">
                                        <div class="inner_body_db">
                                            <table class="table table-striped tbl_achieve">                                            
                                                <tbody>
                                                    <tr>
                                                        <td><span>Rooney Meyers</span></td>
                                                        <td>Completed Step 1:4</td>
                                                    </tr>
                                                    <tr>
                                                        <td><span>Rooney Meyers</span></td>
                                                        <td>Completed Step 1:4</td>
                                                    </tr>
                                                    <tr>
                                                        <td><span>Rooney Meyers</span></td>
                                                        <td>Completed Step 1:4</td>
                                                    </tr>
                                                    <tr>
                                                        <td><span>Rooney Meyers</span></td>
                                                        <td>Completed Step 1:4</td>
                                                    </tr>
                                                    <tr>
                                                        <td><span>Rooney Meyers</span></td>
                                                        <td>Completed Step 1:4</td>
                                                    </tr>
                                                    <tr>
                                                        <td><span>Rooney Meyers</span></td>
                                                        <td>Completed Step 1:4</td>
                                                    </tr><tr>
                                                        <td><span>Rooney Meyers</span></td>
                                                        <td>Completed Step 1:4</td>
                                                    </tr>
                                                    <tr>
                                                        <td><span>Rooney Meyers</span></td>
                                                        <td>Completed Step 1:4</td>
                                                    </tr>
                                                    <tr>
                                                        <td><span>Rooney Meyers</span></td>
                                                        <td>Completed Step 1:4</td>
                                                    </tr>
                                                    <tr>
                                                        <td><span>Rooney Meyers</span></td>
                                                        <td>Completed Step 1:4</td>
                                                    </tr><tr>
                                                        <td><span>Rooney Meyers</span></td>
                                                        <td>Completed Step 1:4</td>
                                                    </tr>
                                                    <tr>
                                                        <td><span>Rooney Meyers</span></td>
                                                        <td>Completed Step 1:4</td>
                                                    </tr>
                                                    <tr>
                                                        <td><span>Rooney Meyers</span></td>
                                                        <td>Completed Step 1:4</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="item_dashboard pull-right">
                                    <div class="header_frame it_head clearfix">
                                        <h4>Shortcuts</h4>
                                    </div>
                                    <div class="wrap_das_sc">
                                        <div class="clearfix">
                                            <a href="#" class="button_add pull-left"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/Admin_add_icon.png" alt="" /><span>Add webinar</span></a>
                                            <a href="#" class="button_add pull-right"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/Admin_add_icon.png" alt="" /><span>Add Live Event</span></a>                                        
                                        </div>
                                        <div class="clearfix">
                                            <a href="#" class="button_add_sc pull-left">Entrepreneur</a>
                                            <a href="#" class="button_add_sc pull-right">Step</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                                              
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




