<!--View for page builder-->
<div class="wrap clearfix">
    <div class="back_to"><a href="#">&laquo; Back to Admin Tools</a></div>
    <div class="row">    
        <div class="span3">
            <div class="main_menu">
                <div class="page_com">
                    <div class="block_title"><h4>Available Blocks</h4></div>
                    <div class="page_body">
                        <div class="inner_page_bd">
                            <div class="block">
                                <div class="block_tt"><h5>Text Block</h5></div>
                                <div class="block_bd">
                                    <p>Enter text to be displayed here</p>
                                </div>
                            </div>

                            <div class="block">
                                <div class="block_tt"><h5>Video Block</h5></div>
                                <div class="block_bd">
                                    <div class="block_video">
                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/files/block/block_video.png" alt="" />
                                    </div>
                                </div>
                            </div>

                            <div class="block">
                                <div class="block_tt"><h5>Link List Block</h5></div>
                                <div class="block_bd">
                                    <a class="link_block" href="#">Link 1,</a>
                                    <a class="link_block" href="#">Link 2,</a>
                                    <a class="link_block" href="#">Link 3,</a>
                                    <a class="link_block" href="#">Link 4</a>
                                </div>
                            </div>

                            <div class="block">
                                <div class="block_tt"><h5>Image List Block</h5></div>
                                <div class="block_bd">
                                    <div class="img_list clearfix">
                                        <div class="img_item_list"></div>                                        
                                        <div class="img_item_list"></div>
                                        <div class="img_item_list"></div>
                                        <div class="img_item_list"></div>
                                        <div class="img_item_list"></div>
                                        <div class="img_item_list"></div>
                                        <div class="img_item_list"></div>
                                        <div class="img_item_list"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="block">
                                <div class="block_tt"><h5>HTMl Block</h5></div>
                                <div class="block_bd">
                                    <p>Enter Code HTML Here</p>
                                </div>
                            </div>

                            <div class="block">
                                <div class="block_tt"><h5>Image Block</h5></div>
                                <div class="block_bd">
                                    <div class="imag">
                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/files/block/img_block.png" alt="" />
                                    </div>
                                </div>
                            </div>

                            <!---->
                            <div class="block">
                                <div class="block_tt"><h5>Image List Block</h5></div>
                                <div class="block_bd">
                                    <div class="img_list clearfix">
                                        <div class="img_item_list"></div>                                        
                                        <div class="img_item_list"></div>
                                        <div class="img_item_list"></div>
                                        <div class="img_item_list"></div>
                                        <div class="img_item_list"></div>
                                        <div class="img_item_list"></div>
                                        <div class="img_item_list"></div>
                                        <div class="img_item_list"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="block">
                                <div class="block_tt"><h5>HTMl Block</h5></div>
                                <div class="block_bd">
                                    <p>
                                        Enter HTML Code Here
                                    </p>
                                </div>
                            </div>

                            <div class="block">
                                <div class="block_tt"><h5>Image Block</h5></div>
                                <div class="block_bd">
                                    <div class="imag">
                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/files/block/img_block.png" alt="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="span9">
            <div class="wrap_container">               
                <div class="frame">
                    <div class="header_frame clearfix">
                        <h4 class="pull-left">Create Page - Details</h4>
                        <a href="#" class="pull-right btn_ac">Next</a>
                    </div>
                    <div class="frame_body">
                        <div class="inner_body clearfix">     
                            <div class="wr_bd clearfix">
                                <div class="item_field">
                                    <label for="">Name</label>
                                    <input class="input-xxlarge ipt" type="text" placeholder="New Page">
                                </div>                            
                                <div class="item_field">
                                    <label for="">Layout</label>
                                    <div class="wrap_layout clearfix">
                                        <div class="item_layout pull-left">
                                            <div class="type_layout clearfix">
                                                <div class="single_lay"></div>
                                            </div>
                                            <div class="choice_layout">
                                                <input type="radio" name="layout" value="1" /><span> Single Column</span>
                                            </div>
                                        </div>
                                        <div class="item_layout pull-left">
                                            <div class="type_layout clearfix">
                                                <div class="col-left pull-left"></div>
                                                <div class="col-right pull-right"></div>
                                            </div>
                                            <div class="choice_layout">
                                                <input type="radio" name="layout" value="2" checked="checked" /><span> Multi Column</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item_field">
                                    <label for="">SEO Title</label>
                                    <input class="input-xxlarge ipt" type="text" placeholder="New Page - Title">
                                </div>
                                <div class="item_field">
                                    <label for="">SEO Description</label>
                                    <input class="input-xxlarge ipt" type="text" placeholder="Lorem ipsum dolor sit amet">
                                </div>
                                <div class="item_field">
                                    <label for="">SEO Keywords</label>
                                    <input class="input-xxlarge ipt" type="text" placeholder="Bussiness, Education, Launch, Growth">
                                </div>
                            </div>

                        </div>                                              
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


