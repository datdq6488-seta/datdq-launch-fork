<!--View for page builder-->
<div class="wrap clearfix">
    <div class="back_to"><a href="#">&laquo; Back to Page Details</a></div>
    <div class="row">    
        <div class="span3">
            <div class="main_menu">
                <div class="page_com">
                    <div class="block_title"><h4>Available Blocks</h4></div>
                    <div class="page_body">
                        <div class="inner_page_bd">
                            <div class="block">
                                <div class="block_tt"><h5>Text Block</h5></div>
                                <div class="block_bd">
                                    <p>Enter text to be displayed here</p>
                                </div>
                            </div>

                            <div class="block">
                                <div class="block_tt"><h5>Video Block</h5></div>
                                <div class="block_bd">
                                    <div class="block_video">
                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/files/block/block_video.png" alt="" />
                                    </div>
                                </div>
                            </div>

                            <div class="block">
                                <div class="block_tt"><h5>Link List Block</h5></div>
                                <div class="block_bd">
                                    <a class="link_block" href="#">Link 1,</a>
                                    <a class="link_block" href="#">Link 2,</a>
                                    <a class="link_block" href="#">Link 3,</a>
                                    <a class="link_block" href="#">Link 4</a>
                                </div>
                            </div>

                            <div class="block">
                                <div class="block_tt"><h5>Image List Block</h5></div>
                                <div class="block_bd">
                                    <div class="img_list clearfix">
                                        <div class="img_item_list"></div>                                        
                                        <div class="img_item_list"></div>
                                        <div class="img_item_list"></div>
                                        <div class="img_item_list"></div>
                                        <div class="img_item_list"></div>
                                        <div class="img_item_list"></div>
                                        <div class="img_item_list"></div>
                                        <div class="img_item_list"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="block">
                                <div class="block_tt"><h5>HTMl Block</h5></div>
                                <div class="block_bd">
                                    <p>Enter Code HTML Here</p>
                                </div>
                            </div>

                            <div class="block">
                                <div class="block_tt"><h5>Image Block</h5></div>
                                <div class="block_bd">
                                    <div class="imag">
                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/files/block/img_block.png" alt="" />
                                    </div>
                                </div>
                            </div>

                            <!---->
                            <div class="block">
                                <div class="block_tt"><h5>Image List Block</h5></div>
                                <div class="block_bd">
                                    <div class="img_list clearfix">
                                        <div class="img_item_list"></div>                                        
                                        <div class="img_item_list"></div>
                                        <div class="img_item_list"></div>
                                        <div class="img_item_list"></div>
                                        <div class="img_item_list"></div>
                                        <div class="img_item_list"></div>
                                        <div class="img_item_list"></div>
                                        <div class="img_item_list"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="block">
                                <div class="block_tt"><h5>HTMl Block</h5></div>
                                <div class="block_bd">
                                    <p>
                                        Enter HTML Code Here
                                    </p>
                                </div>
                            </div>

                            <div class="block">
                                <div class="block_tt"><h5>Image Block</h5></div>
                                <div class="block_bd">
                                    <div class="imag">
                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/files/block/img_block.png" alt="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="span9">
            <div class="wrap_container">               
                <div class="frame">
                    <div class="header_frame clearfix">
                        <h4 class="pull-left">Create Page - Page Builder</h4>
                        <a href="#" class="pull-right btn_ac">Publish</a>
                        <a href="#" class="pull-right btn_base">Save Draft</a>
                    </div>
                    <div class="frame_body">
                        <div class="inner_body clearfix">     
                            <div class="wr_bd clearfix">
                                <div class="multi_col_left pull-left">
                                    <div class="block">
                                        <div class="block_tt clearfix">
                                            <h5 class="pull-left">Header Block</h5>
                                            <ul class="action_block pull-right clearfix">
                                                <li><a href="">Delete</a></li>
                                                <li><a href="">Edit</a></li>
                                            </ul>
                                        </div>
                                        <div class="block_bd">
                                            <p class="leeft_text">Introduction Video</p>
                                        </div>
                                    </div>

                                    <div class="block">
                                        <div class="block_tt clearfix">
                                            <h5 class="pull-left">Video Block</h5>
                                            <ul class="action_block pull-right clearfix">
                                                <li><a href="">Delete</a></li>
                                                <li><a href="">Edit</a></li>
                                            </ul>
                                        </div>
                                        <div class="block_bd">
                                            <div class="block_video">
                                                <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/files/block/block_video.png" alt="" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="block">
                                        <div class="block_tt clearfix">
                                            <h5 class="pull-left">Text Block</h5>
                                            <ul class="action_block pull-right clearfix">
                                                <li><a href="">Delete</a></li>
                                                <li><a href="">Edit</a></li>
                                            </ul>
                                        </div>
                                        <div class="block_bd">
                                            <p class="leeft_text">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et 
                                                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip 
                                                ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="multi_col_right pull-right">
                                    <div class="block">
                                        <div class="block_tt clearfix">
                                            <h5 class="pull-left">Header Block</h5>
                                            <ul class="action_block pull-right clearfix">
                                                <li><a href="">Delete</a></li>
                                                <li><a href="">Edit</a></li>
                                            </ul>
                                        </div>
                                        <div class="block_bd">
                                            <p class="leeft_text">Video Details</p>
                                        </div>
                                    </div>
                                    <div class="block">
                                        <div class="block_tt clearfix">
                                            <h5 class="pull-left">HTMl Block</h5>
                                            <ul class="action_block pull-right clearfix">
                                                <li><a href="">Delete</a></li>
                                                <li><a href="">Edit</a></li>
                                            </ul>
                                        </div>
                                        <div class="block_bd">
                                            <p class="html_block_text">HTML Content will display here</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                                              
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


