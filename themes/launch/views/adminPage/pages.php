<!--View for managerpage-->
<div class="wrap clearfix">
    <div class="row">    
        <div class="span3">
            <div class="main_menu">
                <ul>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Dashboard</a></li>
                    <li class="m_active"><a href="<?php echo Yii::app()->request->baseUrl; ?>">Pages</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Steps</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Quotes</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Videos</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Special Offers</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Books</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Referrals</a></li>                    
                </ul>
                <h4 class="menu_title">Access</h4>
                <ul>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Entrepreneurs</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Content Providers</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Partners</a></li>                                      
                </ul>
                <h4 class="menu_title">Events</h4>
                <ul>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Webinars</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Live Events</a></li>                                                         
                </ul>
                <h4 class="menu_title">Admin</h4>
                <ul>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Email Manager</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Notifications Manager</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Template Design</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Menu Links</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>">Access Manager</a></li>                    
                </ul>
            </div>
        </div>

        <div class="span9">
            <div class="wrap_container">               
                <div class="frame">
                    <div class="header_frame clearfix">
                        <h4 class="pull-left">Page</h4>
                        <a href="#" class="pull-right btn_ac">Add</a>
                    </div>
                    <div class="frame_body">

                        <div class="inner_body clearfix">
                            <div class="wr_bd">
                                <div class="page_item pull-left">
                                    <div class="content_align">
                                        <h4>Home</h4>
                                        <span>(Default)</span>
                                    </div>
                                </div>
                                <div class="page_item pull-left">
                                    <div class="content_align">
                                        <h4>Ask The Expert</h4>                                    
                                    </div>
                                    <div class="action">
                                        <a href="#" class="btn pull-right">Edit</a>
                                        <a href="#" class="btn pull-right">Delete</a>                                    
                                    </div>
                                </div>
                                <div class="page_item pull-left">
                                    <div class="content_align">
                                        <h4>Live Events</h4>                                    
                                    </div>
                                    <div class="action">
                                        <a href="#" class="btn pull-right">Edit</a>
                                        <a href="#" class="btn pull-right">Delete</a>                                    
                                    </div>
                                </div>
                                <div class="page_item pull-left">
                                    <div class="content_align">
                                        <h4>Webinars</h4>                                    
                                    </div>
                                    <div class="action">
                                        <a href="#" class="btn pull-right">Edit</a>
                                        <a href="#" class="btn pull-right">Delete</a>                                    
                                    </div>
                                </div>
                                <div class="page_item pull-left">
                                    <div class="content_align">
                                        <h4>Bookstore</h4>                                    
                                    </div>
                                    <div class="action">
                                        <a href="#" class="btn pull-right">Edit</a>
                                        <a href="#" class="btn pull-right">Delete</a>                                    
                                    </div>
                                </div>
                                <div class="page_item pull-left">
                                    <div class="content_align">
                                        <h4>Special Offers</h4>                                    
                                    </div>
                                    <div class="action">
                                        <a href="#" class="btn pull-right">Edit</a>
                                        <a href="#" class="btn pull-right">Delete</a>                                    
                                    </div>
                                </div>
                            </div>

                        </div>                                              
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


