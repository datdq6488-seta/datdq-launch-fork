<!--View of activity-->
<div class="wrap clearfix">
    <div class="row">    
        <div class="span3">
            <div class="main_menu">
                <ul>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/user/dashboard">Dashboard</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/user/profile">Profile</a></li>
                    <li class="m_active"><a href="<?php echo Yii::app()->request->baseUrl; ?>/user/activity">Activity</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/user/steps">Steps</a></li>
                </ul>
            </div>
        </div>

        <div class="span9">
            <div class="wrap_container">               
                <div class="frame">
                    <div class="header_frame clearfix">
                        <h4 class="pull-left">Activity</h4>
                    </div>
                    <div class="frame_body">
                        <div class="inner_body clearfix">
                            <div class="left_day pull-left">
                                <h4>DAY</h4>
                                <div id="col_date" class="col_date"></div>
                            </div>                            
                            <div class="right_date pull-right">
                                <h4>FEED</h4>
                                <div id="wrap_feed" class="wrap_feed">
                                    <div class="feed_item">
                                        <div class="neo"><span>9</span></div>
                                        <ul class="wrap_it_feed">
                                            <li>
                                                <div class="media clearfix">
                                                    <a class="pull-left r_margin" href="#">
                                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/king-icon.png" alt="" />
                                                    </a>
                                                    <div class="media-body w_cont pull-right">
                                                        <h5 class="media-heading head_mess_feed">Media heading</h5>
                                                        <p class="body_messs_fedd">
                                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                                                        </p>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="media clearfix">
                                                    <a class="pull-left r_margin" href="#">
                                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/king-icon.png" alt="" />
                                                    </a>
                                                    <div class="media-body w_cont pull-right">
                                                        <h5 class="media-heading head_mess_feed">Media heading</h5>
                                                        <p class="body_messs_fedd">
                                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                                                        </p>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    
                                    <div class="week_feed">
                                        <p>- Week 1 -</p>
                                    </div>
                                    
                                    <div class="feed_item">
                                        <div class="neo"><span>7</span></div>
                                        <ul class="wrap_it_feed">
                                            <li>
                                                <div class="media clearfix">
                                                    <a class="pull-left r_margin" href="#">
                                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/king-icon.png" alt="" />
                                                    </a>
                                                    <div class="media-body w_cont pull-right">
                                                        <h5 class="media-heading head_mess_feed">Media heading</h5>
                                                        <p class="body_messs_fedd">
                                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                                                        </p>
                                                    </div>
                                                </div>
                                            </li>                                            
                                        </ul>
                                    </div>
                                    
                                    <div class="feed_item">
                                        <div class="neo"><span>6</span></div>
                                        <ul class="wrap_it_feed">
                                            <li>
                                                <div class="media clearfix">
                                                    <a class="pull-left r_margin" href="#">
                                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/king-icon.png" alt="" />
                                                    </a>
                                                    <div class="media-body w_cont pull-right">
                                                        <h5 class="media-heading head_mess_feed">Media heading</h5>
                                                        <p class="body_messs_fedd">
                                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                                                        </p>
                                                    </div>
                                                </div>
                                            </li>                                            
                                        </ul>
                                    </div>
                                    
                                    <div class="feed_item">
                                        <div class="neo"><span>5</span></div>
                                        <ul class="wrap_it_feed">
                                            <li>
                                                <div class="media clearfix">
                                                    <a class="pull-left r_margin" href="#">
                                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/king-icon.png" alt="" />
                                                    </a>
                                                    <div class="media-body w_cont pull-right">
                                                        <h5 class="media-heading head_mess_feed">Media heading</h5>
                                                        <p class="body_messs_fedd">
                                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                                                        </p>
                                                    </div>
                                                </div>
                                            </li>                                            
                                        </ul>
                                    </div>
                                    
                                    <div class="feed_item">
                                        <div class="neo"><span>2</span></div>
                                        <ul class="wrap_it_feed">
                                            <li>
                                                <div class="media clearfix">
                                                    <a class="pull-left r_margin" href="#">
                                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/king-icon.png" alt="" />
                                                    </a>
                                                    <div class="media-body w_cont pull-right">
                                                        <h5 class="media-heading head_mess_feed">Media heading</h5>
                                                        <p class="body_messs_fedd">
                                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                                                        </p>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="media clearfix">
                                                    <a class="pull-left r_margin" href="#">
                                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/king-icon.png" alt="" />
                                                    </a>
                                                    <div class="media-body w_cont pull-right">
                                                        <h5 class="media-heading head_mess_feed">Media heading</h5>
                                                        <p class="body_messs_fedd">
                                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                                                        </p>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    
                                    <div class="feed_item">
                                        <div class="neo"><span>1</span></div>
                                        <ul class="wrap_it_feed">
                                            <li>
                                                <div class="media clearfix">
                                                    <a class="pull-left r_margin" href="#">
                                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/king-icon.png" alt="" />
                                                    </a>
                                                    <div class="media-body w_cont pull-right">
                                                        <h5 class="media-heading head_mess_feed">Media heading</h5>
                                                        <p class="body_messs_fedd">
                                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                                                        </p>
                                                    </div>
                                                </div>
                                            </li>                                            
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>                                              
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/libs/jquery-1.9.1.min.js"></script>
<script type="text/javascript">
    $(document).ready(function (){
       var hContent;
       hContent = $("#wrap_feed").outerHeight(true);
       $("#col_date").height((hContent + 40) + "px");       
    });
</script>

