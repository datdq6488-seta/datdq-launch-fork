<!--view of dashboard-->
<div class="wrap clearfix">
    <div class="row">    
        <div class="span3">
            <div class="main_menu">
                <ul>
                    <li class="m_active"><a href="<?php echo Yii::app()->request->baseUrl; ?>/user/dashboard">Dashboard</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/user/profile">Profile</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/user/activity">Activity</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/user/steps">Steps</a></li>
                </ul>
            </div>
        </div>

        <div class="span9">
            <div class="wrap_container">
                <div class="frame">
                    <div class="header_frame clearfix">
                        <h4 class="pull-left">Orverview</h4>
                    </div>
                    <div class="frame_body">
                        <div class="inner_head_body clearfix">
                            <div class="over_col1 pull-left">
                                <div class="over_body">
                                    <p class="stastic">1,027</p>
                                </div>
                                <div class="over_foot">
                                    <p>POINT</p>
                                </div>
                            </div>
                            <div class="over_col2 pull-left">
                                <div class="over_body">
                                    <div class="for_pro">
                                        <div class="progress new_pro_size">
                                            <div class="bar new_bar_size" style="width: 20%;">20%</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="over_foot">
                                    <p>PROGRESS</p>
                                </div>
                            </div>
                            <div class="over_col3 pull-left">
                                <div class="over_body">
                                    <div class="king_rate clearfix">
                                        <ul class="k_rate clearfix">
                                            <li class="item_rate act"></li>
                                            <li class="item_rate act"></li>
                                            <li class="item_rate"></li>
                                            <li class="item_rate"></li>
                                            <li class="item_rate"></li>
                                            <li class="item_rate"></li>
                                            <li class="item_rate"></li>
                                            <li class="item_rate"></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="over_foot">
                                    <p>BADGES</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="frame">
                    <div class="header_frame clearfix">
                        <h4 class="pull-left">Progress</h4>
                    </div>
                    <div class="frame_body">
						<?php foreach ($steps as $parent): ?>
                        <div class="inner_body t_margin">		
                            <h4 class="step_title"><?php echo $parent['name'];?> - <?php echo $parent['full_name'];?></h4>
                            <div class="col1 f_margin clearfix">
                                <h5 class="pull-left"><?php echo $parent['intro']['name'];?></h5>
                                <a class="<?php echo $parent['intro']['status'] == 0 ? 'getstart' : 'btn' ;?> pull-right" href="<?php echo Yii::app()->getBaseUrl(true).'/landing/parent/step/'.$parent['id'];?>"><?php echo $parent['intro']['status'] == 0 ? 'Get Started' : 'Completed' ;?></a>
                            </div>
							<?php $i = 1;?>
							<?php foreach ($parent['child_step'] as $child): ?>
								<div class="row f_margin clearfix">
									<div class="span5 col1"><h5><?php echo $i++;?>.&nbsp;<?php echo $child['name'];?></h5></div>
									<div class="span2 col2"><span>&raquo;</span><h5> Quicken</h5></div>
									<div class="<?php echo $child['status'] == 0 ? 'getstart' : '' ;?> col3 pull-right"><a class="<?php echo $child['status'] == 0 ? '' : 'btn' ;?>" href="<?php echo Yii::app()->getBaseUrl(true).'/landing/child/step/'.$child['id'];?>"><?php echo $child['status'] == 0 ? 'Get Started' : 'Completed' ;?></a></div>
								</div>
							<?php endforeach; ?>
                        </div>
						<?php endforeach;?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>