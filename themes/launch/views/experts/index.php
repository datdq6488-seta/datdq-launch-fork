<?php ?>
<div class="row">
    <div class="col-md-12">                        
        <div class="ask_content content_expert">                       
            <div class="head_box_line clearfix">
                <div class="row">
                    <div class="col-md-5">
                        <span class="title_for_box"><?php echo Yii::t('_yii', 'Experts'); ?></span>
                    </div>
                    <div class="col-md-7">
                        <form class="form-inline pull-right m_top" role="form">
                            <div class="form-group group_m_right">
                                <label class="inline_filter_bar" for="exampleInputEmail2"><?php echo Yii::t('_yii', 'Sort by:'); ?></label>
                                <select name="ex_sort_by" id="ex_sort_by">
                                    <option value="" selected="selected">Rating</option>
                                    <option value="title-asc"><?php echo Yii::t('_yii', 'Title(A-Z)'); ?></option>
                                    <option value="title-desc"><?php echo Yii::t('_yii', 'Title(Z-A)'); ?></option>                                    
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="inline_filter_bar" for=""><?php echo Yii::t('_yii', 'Search:'); ?></label>
                            </div>
                            <div class="form-group">
                                <div class="input-group">                
                                    <input type="text" id="abc" class="form-control re_ipt_search">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default re_button" type="button"><span class="glyphicon glyphicon-search"></span></button>
                                    </span>
                                </div>
                            </div>                      
                        </form>
                    </div>
                </div>
            </div>

            <div class="list_item_expert bg_sponsored">
                <h3 class="tt_list_expert">Sponsored By:</h3>

                <div class="media expert_item clearfix">
                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/experts/detail" class="pull-left media-img-experts ask_shadow"><img class="img-responsive" src="<?php echo Yii::app()->request->baseUrl; ?>/images/files/avatar/medium/author01.jpg" alt="" /></a>
                    <div class="media-body">
                        <div class="it_expert_detail">
                            <h5 class="media-heading"><a href="<?php echo Yii::app()->request->baseUrl; ?>/experts/detail">Tony Robbins</a></h5>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et 
                                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip 
                                ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu 
                                fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                            </p>
                            <a class="meet_author" href="<?php echo Yii::app()->request->baseUrl; ?>/experts/detail">Meet Charlle</a>
                        </div>                        
                    </div>
                </div>

                <div class="media expert_item clearfix">
                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/experts/detail" class="pull-left media-img-experts ask_shadow"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/files/avatar/medium/author02.jpg" alt="" /></a>
                    <div class="media-body">
                        <div class="it_expert_detail">
                            <h5 class="media-heading"><a href="<?php echo Yii::app()->request->baseUrl; ?>/experts/detail">Charlie Hoffman</a></h5>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et 
                                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip 
                                ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu 
                                fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                            </p>
                            <a class="meet_author" href="<?php echo Yii::app()->request->baseUrl; ?>/experts/detail">Meet Charlle</a>
                        </div>                        
                    </div>
                </div>

                <div class="media expert_item clearfix">
                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/experts/detail" class="pull-left media-img-experts ask_shadow"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/files/avatar/medium/author03.jpg" alt="" /></a>
                    <div class="media-body">
                        <div class="it_expert_detail">
                            <h5 class="media-heading"><a href="<?php echo Yii::app()->request->baseUrl; ?>/experts/detail">Charlie Hoffman</a></h5>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et 
                                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip 
                                ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu 
                                fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                            </p>
                            <a class="meet_author" href="<?php echo Yii::app()->request->baseUrl; ?>/experts/detail">Meet Charlle</a>
                        </div>
                    </div>
                </div>                                                               
            </div>

            <div class="line_box_client"></div>

            <div class="list_item_expert">
                <h3 class="tt_list_expert">Additional Providers:</h3>

                <div class="media expert_item clearfix">
                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/experts/detail" class="pull-left media-img-experts ask_shadow"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/files/avatar/medium/author04.jpg" alt="" /></a>
                    <div class="media-body">
                        <div class="it_expert_detail">
                            <h5 class="media-heading"><a href="<?php echo Yii::app()->request->baseUrl; ?>/experts/detail">Charlie Hoffman</a></h5>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et 
                                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip 
                                ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu 
                                fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                            </p>
                            <a class="meet_author" href="<?php echo Yii::app()->request->baseUrl; ?>/experts/detail">Meet Charlle</a>
                        </div>
                    </div>
                </div>

                <div class="media expert_item clearfix">
                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/experts/detail" class="pull-left media-img-experts ask_shadow"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/files/avatar/medium/author05.jpg" alt="" /></a>
                    <div class="media-body">
                        <div class="it_expert_detail">
                            <h5 class="media-heading"><a href="<?php echo Yii::app()->request->baseUrl; ?>/experts/detail">Charlie Hoffman</a></h5>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et 
                                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip 
                                ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu 
                                fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                            </p>
                            <a class="meet_author" href="<?php echo Yii::app()->request->baseUrl; ?>/experts/detail">Meet Charlle</a>
                        </div>
                    </div>
                </div>

                <div class="media expert_item clearfix">
                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/experts/detail" class="pull-left media-img-experts ask_shadow"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/files/avatar/medium/author02.jpg" alt="" /></a>
                    <div class="media-body">
                        <div class="it_expert_detail">
                            <h5 class="media-heading"><a href="<?php echo Yii::app()->request->baseUrl; ?>/experts/detail">Charlie Hoffman</a></h5>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et 
                                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip 
                                ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu 
                                fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                            </p>
                            <a class="meet_author" href="<?php echo Yii::app()->request->baseUrl; ?>/experts/detail">Meet Charlle</a>
                        </div>
                    </div>
                </div>

                <div class="media expert_item clearfix">
                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/experts/detail" class="pull-left media-img-experts ask_shadow"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/files/avatar/medium/author03.jpg" alt="" /></a>
                    <div class="media-body">
                        <div class="it_expert_detail">
                            <h5 class="media-heading"><a href="<?php echo Yii::app()->request->baseUrl; ?>/experts/detail">Charlie Hoffman</a></h5>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et 
                                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip 
                                ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu 
                                fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                            </p>
                            <a class="meet_author" href="<?php echo Yii::app()->request->baseUrl; ?>/experts/detail">Meet Charlle</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="view_more">
                <a href="#">VIEW MORE</a>
            </div>
        </div>
    </div>
</div>

