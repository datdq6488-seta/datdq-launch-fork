<?php ?>

<div class="row">
    <div class="col-md-4">
        <div class="ask_avatar">
            <div class="inner_avatar">
                <img class="img-responsive" src="<?php echo Yii::app()->request->baseUrl; ?>/images/files/avatar/large/Header.jpg" alt="no-image" />
            </div>            

            <div class="infomation_avatar">
                <form action="" class="form-horizontal">
                    <div class="control-group mar_bottom clearfix">
                        <label class="control-label l_label" for="company">Company:</label>
                        <div class="controls l_control">
                            <img class="img-responsive img_company" src="<?php echo Yii::app()->request->baseUrl; ?>/images/files/company/company-img.jpg" alt="no-image" />
                            <span class="name_com">Robbins Research International</span>
                        </div>
                    </div>
                    <div class="control-group mar_bottom clearfix">
                        <label class="control-label l_label" for="founder">Title:</label>
                        <div class="controls l_control for_val_tt">
                            <span>Founder</span>
                        </div>
                    </div>
                </form>
            </div>

            <div class="l_btn_contacted">                
                <ul class="btn_ct">
                    <li><a class="btn btn-primary btn-block" href="#">Contact Tony</a></li>
                    <li><a class="btn btn-default btn-block" href="#">Book to Speak</a></li>
                </ul>
            </div>

            <div class="l_btn_viewmore">                
                <ul class="btn_ct">                    
                    <li><a class="btn btn-default btn-block" href="<?php echo Yii::app()->request->baseUrl; ?>/experts">View more experts...</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="col-md-8">
        <div class="box_expert_detai">
            <div class="box_content line clearfix">            
                <h4 class="head_title pull-left">Tony Robbins</h4>
                <div class="social_icon pull-right">
                    <ul class="social clearfix">
                        <li><a href="" class="social_item soci_item1"></a></li>
                        <li><a href="" class="social_item soci_item2"></a></li>
                        <li><a href="" class="social_item soci_item3"></a></li>
                        <li><a href="" class="social_item soci_item4"></a></li>
                        <li><a href="" class="social_item soci_item5"></a></li>
                        <li><a href="" class="social_item soci_item6"></a></li>
                        <li><a href="" class="social_item soci_item7"></a></li>
                        <li><a href="" class="social_item soci_item8"></a></li>
                    </ul>
                </div>                
            </div>
            <div style="padding: 20px">                           
                <div class="box_content clearfix">
                    <h4 class="box_title">Biography</h4>
                    <p class="graphy_content">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et 
                        dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip 
                        ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu 
                        fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et 
                        dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip 
                        ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu 
                        fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </p>            
                </div>

                <div class="box_content clearfix">
                    <h4 class="box_title">Contact detail</h4>
                    <ul class="info_for_contact f_margin">
                        <li><span>Email Address: </span>quangdv6466@gmail.com</li>
                        <li><span>Phone numbers: </span>619-777-7777</li>
                        <li><span>Website: </span>http://www.dantri.com.vn</li>
                    </ul>                
                </div>

                <div class="box_content clearfix">
                    <h4 class="box_title">Live Events</h4>
                    <div class="media f_margin">
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/events/detail" class="pull-left media-img"><img class="img-responsive" src="<?php echo Yii::app()->request->baseUrl; ?>/images/files/event/mask-licve-event.jpg" alt="" /></a>
                        <div class="media-body">
                            <ul class="detail_of_item">
                                <li><h5 class="media-heading"><a href="<?php echo Yii::app()->request->baseUrl; ?>/events/detail">Business mastery - Take your business the Next Level</a></h5></li>
                                <li><h5>June 25, 2014 - 10:00 AM EST</h5></li>
                                <li><span>Location: </span> Los Angeles Convention Center - <a href="<?php echo Yii::app()->request->baseUrl; ?>/events/map/mark/1">Map</a></li>
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit <a href="<?php echo Yii::app()->request->baseUrl; ?>/events/detail">more</a></li>                        
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="box_content clearfix">
                    <h4 class="box_title">Webinars</h4>
                    <div class="media f_margin">
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/webinars/detail" class="pull-left media-img"><img class="img-responsive" src="<?php echo Yii::app()->request->baseUrl; ?>/images/files/webinar/mask-webina.jpg" alt="" /></a>
                        <div class="media-body">
                            <ul class="detail_of_item">
                                <li><h5 class="media-heading"><a href="<?php echo Yii::app()->request->baseUrl; ?>/webinars/detail">Live webinars: Exentinal For Starting A Bussiness</a></h5></li>
                                <li><h5>June 25, 2014 - 10:00 AM EST</h5></li>                            
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et 
                                    dolore magna aliqua... <a href="<?php echo Yii::app()->request->baseUrl; ?>/webinars/detail">more</a></li>                        
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="box_content clearfix">
                    <h4 class="box_title">Books</h4>
                    <div class="media f_margin">
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/books/detail" class="pull-left media-img ask_shadow"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/files/book/mask-book.jpg" alt="" /></a>
                        <div class="media-body">
                            <ul class="detail_of_item">
                                <li><h5 class="media-heading"><a href="<?php echo Yii::app()->request->baseUrl; ?>/books/detail">Unlimited Power</a></h5></li>
                                <li><span>Author: </span><a href="<?php echo Yii::app()->request->baseUrl; ?>/authors/detail">Anthony Robbins</a></li>                                                        
                                <li style="margin-top: 18px;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et 
                                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip 
                                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit... <a href="<?php echo Yii::app()->request->baseUrl; ?>/books/detail">more</a></li>                        
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


