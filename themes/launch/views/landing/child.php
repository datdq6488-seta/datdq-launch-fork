<div id="child-content">
	
	<div id="child-video">
		<div class="child-name">
			<span class="child-order"><?php echo $data['sub_name'];?></span>
			<span>To Do: <?php echo $data['name'];?></span>
		</div><!--.child-name-->
		
		<div class="promoted-video">
			<div class="youtube-video">
				<iframe width="734" height="421" src="<?php echo $dataParent['intro']['video'];?>" frameborder="0" allowfullscreen></iframe>
				<span>Building Deeper Relationships Make Your Bussiness Better</span>
				<a href="#">Meet Tony</a>
			</div>
			
			<div class="featured-video">
				<ul>
					<?php foreach ( $dataParent['feature_video'] as  $video) :?>
						<li>
							<div class="thumbnail-video">
								<a href="<?php echo Yii::app()->request->baseUrl?>/videos/detail/1"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/files/videos/<?php echo $video['img'];?>" alt=""/></a>
							</div>
							<a href="<?php echo Yii::app()->request->baseUrl?>/videos/detail/1"><?php echo $video['name'];?></a>
							<span>by <?php echo $video['author'];?></span>
						</li>
					<?php endforeach;?>
				</ul>
				<a href="<?php echo Yii::app()->request->baseUrl.'/videos' ;?>" id="more-video">More Videos</a>
			</div><!--.featured-video-->
		</div><!--.promoted-video-->
	</div><!--#child-video-->
	
	<div id="child-content-wrap">
		<div class="child-title">
			<span>The Tool You Need</span>
		</div><!--.child-title-->
		<div class="child-content">
			<div class="advertising">
				<a href="#"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/advertising.png"/></a>
			</div>
			<div class="child-desc">
				<?php echo $data['full_desc'];?>
			</div><!--.child-desc-->
			<div class="child-completed">
				<span>I, <?php echo $user['full_name'];?>, have completed this task.</span>
				<div class="wrap-submit">
					<input type="text" class="input-key">
					<button class="submit-completed" type="submit">Search</button>
				</div>
			</div><!--.child-completed-->
		</div><!--.child-content-->
	</div><!--#child-content-wrap-->
</div>