<div id="step-content">
	
	<div id="step-intro">
		<div class="step-name">
			<span>Step <?php echo $currentStep;?>: Introduction</span>
			<a href="<?php echo Yii::app()->request->baseUrl;?>/preneurs/detail">Meet Scoot</a>
		</div><!--.step-name-->
		
		<div class="step-video">
			<div class="youtube-video">
				<iframe width="734" height="421" src="<?php echo $data['intro']['video'];?>" frameborder="0" allowfullscreen></iframe>
			</div>
		</div><!--.step-video-->
	</div><!--#step-intro-->
	
	<div id="step-menu">
		<div class="menu-desc">
			<span>Things You Need To Do</span>
		</div><!--.menu-desc-->
		<div class="sub-menu">
			<ul>
				<?php $i = 1;?>
				<?php foreach($data['child_step'] as $child) : ?>
				<li>
					<a href="<?php echo Yii::app()->getBaseUrl(true).'/landing/child/step/'.$child['id'];?>">
						<span><?php echo $i++;?>.&nbsp;<?php echo $child['name'];?></span>
					</a>
				</li>
				<?php endforeach; ?>
			</ul>
			<div class="advertising">
				<a href="<?php echo $data['partner']['link'];?>"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/<?php echo $data['partner']['img'];?>"/></a>
			</div>
		</div><!--.sub-menu-->
	</div><!--#step-menu-->
</div>