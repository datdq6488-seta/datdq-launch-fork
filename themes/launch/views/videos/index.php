<div id="video">
	<div class="wrap-left">
		<div class="category box">
			<div class="title">
				<span>Categories</span>
			</div>
			<div class="list">
				<a href="javascript:void(0)" class="check-all" for=".category-list">Check All</a>
				<a href="javascript:void(0)" class="uncheck-all" for=".category-list">Uncheck All</a>
				<ul class="category-list">
					<?php foreach ($categories as $cat):?>
						<li>
							<div>
								<input type="checkbox" id="<?php echo 'cat-'.$cat['id']?>">
								<label for="<?php echo 'cat-'.$cat['id']?>"><?php echo $cat['name']?></label>
							</div>
						</li>
					<?php endforeach;?>
				</ul>
			</div>
		</div>
		
		<div class="tag box">
			<div class="title">
				<span>Tags</span>
			</div>
			<div class="list">
				<a href="javascript:void(0)" class="check-all" for=".tag-list">Check All</a>
				<a href="javascript:void(0)" class="uncheck-all" for=".tag-list">Uncheck All</a>
				<ul class="tag-list">
					<?php foreach ($tags as $tag):?>
						<li>
							<div>
								<input type="checkbox" id="<?php echo 'tag-'.$tag['id']?>">
								<label for="<?php echo 'tag-'.$tag['id']?>"><?php echo $tag['name']?></label>
							</div>
						</li>
					<?php endforeach;?>
				</ul>
			</div>
		</div>
	</div>
	
	
	<div class="wrap-right">
		<div class="content">
			<div class="title">
				<div class="filter-type">
					<a href="javascript:void(0)" class="check-all">Grid View</a>
					<a href="javascript:void(0)" class="uncheck-all">List View</a>
				</div>
				<div class="info-search-filter">
					<form name="search-form-video" id="search-form-video" action="" method="get">
						<div class="filter-sort">
							<label><?php echo Yii::t('_yii','Sort by:');?></label>
							<select name="sort" id="sortvideo">
								<option value="most-recent"><?php echo Yii::t('_yii','Most Recent');?></option>
							</select>
						</div>
						<div class="filter-search">
							<div>
								<label><?php echo Yii::t('_yii','Search:');?></label>
								<input type="text" name="key" class="key-search-input" value="<?php echo isset($_GET['key'])?$_GET['key']:'';?>">
								<button type="submit" class="submit-search" id="submit-search"><?php echo Yii::t('_yii','Search');?></button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="must-watch video-list">
				<span class="sub-title">Must Watch</span>
				<div class="video-list-detail">
					<ul>
						<?php for ($i = 1; $i <=8; $i++) :?>
							<?php $video = $videos[array_rand($videos)];?>
							<li>
								<div class="thumbnail-video">
									<a href="<?php echo Yii::app()->request->baseUrl?>/videos/detail/id/<?php echo $video['id']?>"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/files/videos/<?php echo $video['img']?>" alt=""/></a>
								</div>
								<a href="<?php echo Yii::app()->request->baseUrl?>/videos/detail/id/<?php echo $video['id']?>"><?php echo $video['name']?></a>
								<span class="author">by <?php echo $video['author']?></span>
								<span class="length"><?php echo $video['length']?></span>
							</li>
						<?php endfor;?>
					</ul>
				</div>
			</div>
			<div class="most-popular video-list">
				<span class="sub-title">Most Popular</span>
				<div class="video-list-detail">
					<ul>
						<?php for ($i = 1; $i <=8; $i++) :?>
							<?php $video = $videos[array_rand($videos)];?>
							<li>
								<div class="thumbnail-video">
									<a href="<?php echo Yii::app()->request->baseUrl?>/videos/detail/id/<?php echo $video['id']?>"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/files/videos/<?php echo $video['img']?>" alt=""/></a>
								</div>
								<a href="<?php echo Yii::app()->request->baseUrl?>/videos/detail/id/<?php echo $video['id']?>"><?php echo $video['name']?></a>
								<span class="author">by <?php echo $video['author']?></span>
								<span class="length"><?php echo $video['length']?></span>
							</li>
						<?php endfor;?>
					</ul>
				</div>
			</div>
			<div class="most-viewed video-list">
				<span class="sub-title">Most Viewed</span>
				<div class="video-list-detail">
					<ul>
						<?php for ($i = 1; $i <=4; $i++) :?>
							<?php $video = $videos[array_rand($videos)];?>
							<li>
								<div class="thumbnail-video">
									<a href="<?php echo Yii::app()->request->baseUrl?>/videos/detail/id/<?php echo $video['id']?>"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/files/videos/<?php echo $video['img']?>" alt=""/></a>
								</div>
								<a href="<?php echo Yii::app()->request->baseUrl?>/videos/detail/id/<?php echo $video['id']?>"><?php echo $video['name']?></a>
								<span class="author">by <?php echo $video['author']?></span>
								<span class="length"><?php echo $video['length']?></span>
							</li>
						<?php endfor;?>
					</ul>
				</div>
			</div>
			<div class="bookstore-footer">
                <a href="#" class="book-load-more"><?php echo Yii::t('_yii','View more');?></a>
            </div>
		</div>
	</div>
</div>