<div id="video-detail">
	<div class="wrap-detail">
		<div id="video-content">
			<div class="head-content">
				<span><?php echo $videoData['name']?></span>
				<a href="#">Meet Tony</a>
			</div><!--.head-content-->

			<div class="main-content">
				<div class="youtube-video">
					<iframe width="734" height="421" src="<?php echo $videoData['link']; ?>" frameborder="0" allowfullscreen></iframe>
				</div>
			</div><!--.main-content-->
		</div><!--#video-content-->

		<div id="detail-video">
			<div class="detail-head">
				<span>Video Details</span>
			</div><!--.detail-head-->
			<div class="detail-main">
				<span class="title-desc"><?php echo $videoData['name'];?></span>
				<span class="length-desc"><?php echo $videoData['length'];?></span>
				<div class="full-desc">
					<?php echo $videoData['full_desc'];?>
				</div><!--.full-desc-->
				<div class="meta-category">
					<span class="meta-type">Category</span>
					<span class="meta-content">Most Watched</span>
				</div><!--.meta-category-->
				<div class="meta-tag">
					<span class="meta-type">Tag</span>
					<span class="meta-content">Business, Finance, Online, Relationships</span>
				</div><!--.meta-tag-->
			</div><!--.sub-menu-->
		</div><!--#detail-video-->
	
	</div><!--.wrap-detail-->
	
	<div class="wrap-related">
		<div class="related-title">
			 <span>Related Videos</span>
			 <a href="<?php echo Yii::app()->request->baseUrl?>/videos">See all</a>
		</div><!--.related-title-->
			
		<div class="video-list related-list">
			<div class="video-list-detail">
				<ul>
					<?php for ($i = 1; $i <=10; $i++) :?>
						<?php $video = $videos[array_rand($videos)];?>
						<li>
							<div class="thumbnail-video">
								<a href="<?php echo Yii::app()->request->baseUrl?>/videos/detail/id/<?php echo $video['id']?>"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/files/videos/<?php echo $video['img']?>" alt=""/></a>
							</div>
							<a href="<?php echo Yii::app()->request->baseUrl?>/videos/detail/id/<?php echo $video['id']?>"><?php echo $video['name']?></a>
							<span class="author">by <?php echo $video['author']?></span>
							<span class="length"><?php echo $video['length']?></span>
						</li>
					<?php endfor;?>
				</ul>
			</div>
		</div>
	</div><!--.wrap-related-->
</div>