<?php foreach ($modelSponsored as $row):?>
<div class="media expert_item clearfix">
	<a href="<?php echo BASE_URL; ?>/books/detail/<?php echo $row->id;?>" class="pull-left media-img-book ask_shadow">
		<img src="<?php echo BASE_URL; ?>/images/files/book/book-item.png" alt="" />
	</a>
	<div class="media-body">
		<div class="it_book_detail">
			<h5 class="media-heading"><a href="<?php echo BASE_URL; ?>/books/detail/<?php echo $row->id;?>"><?php echo $row->title; ?></a></h5>
			<span><?php echo Yii::t('_yii', 'Author: '); ?></span><a href="<?php echo BASE_URL;?>/authors/detail"><?php echo $row->author; ?></a>
			<p class="description_book">
				<?php echo StringUtil::cutText($row->description, 550); ?>
				<a href="<?php echo BASE_URL;?>/books/detail/<?php echo $row->id;?>" class="view-more-item"><?php echo Yii::t('_yii', 'more');?></a>
			</p>                            
		</div>                        
	</div>
</div>
<?php endforeach;?>