<?php
/* @var $this BookstoreController */

$this->breadcrumbs = array(
    'Bookstore Detail',
);
?>
<div class="row">
    <div class="col-md-4">
        <div class="ask_avatar">
            <div class="inner_avatar">
                <img class="img-responsive" src="<?php echo BASE_URL; ?>/images/files/book/Book_Item_Details.png" alt="no-image" />
            </div>            

            <div class="l_btn_contacted">                
                <ul class="btn_ct">
                    <li class="book_distribute"><a href="javascript:void(0)"><img src="<?php BASE_URL; ?>/images/files/company/com_amazon.png" alt="" /></a></li>
                    <li class="book_distribute"><a href="javascript:void(0)"><img src="<?php BASE_URL; ?>/images/files/company/com_barnes.png" alt="" /></a></li>
                    <li class="book_distribute"><a href="javascript:void(0)"><img src="<?php BASE_URL; ?>/images/files/company/com_amilion.png" alt="" /></a></li>
                    <li class="book_distribute"><a href="javascript:void(0)"><img src="<?php BASE_URL; ?>/images/files/company/com_buyat.png" alt="" /></a></li>
                </ul>
            </div>           
        </div>
    </div>

    <div class="col-md-8">
        <div class="box_expert_detai">
            <div class="box_content line clearfix">                
                <h4 class="head_title pull-left"><?php echo $model->title; ?></h4>

                <div class="social_icon pull-right" id="share">
                    <ul class="social clearfix" id="share">
                        <!--<li><a href="javascript:void(0)" class="social_item soci_item1"></a></li>-->
                        <li><span class='st_email_large' displayText='Email'></span></li>                                                
                        <li><a href="javascript:void(0)" class="share-social plusone social_item soci_item6" data-social='{"type":"plusone", "url":"<?php echo BASE_URL . '/books/detail/' . $model->id; ?>", "text": ""}'>Google Plus</a></li>
                        <li><a href="javascript:void(0)" class='share-social twitter social_item soci_item7'  data-social='{"type":"twitter", "url":"<?php echo BASE_URL . '/books/detail/' . $model->id; ?>", "text": ""}'>Twitter</a></li>
                        <li><a href="javascript:void(0)" class='share-social facebook social_item soci_item8' data-social='{"type":"facebook", "url":"<?php echo BASE_URL . '/books/detail/' . $model->id; ?>", "text": ""}'>Facebook</a></li> 
                    </ul>
                </div>                
            </div>
            <div style="padding: 20px">
                <div class="box_content clearfix">
                    <h4 class="box_title"><?php echo Yii::t('_yii', 'Book Details'); ?></h4>
                    <div class="author-detail"><strong><?php echo Yii::t('_yii', 'Author'); ?>:</strong> <a href="<?php echo Yii::app()->request->baseUrl; ?>/authors/detail"><?php echo $model->author; ?></a></div>
                    <div class="publisher-detail"><strong><?php echo Yii::t('_yii', 'Publisher'); ?>:</strong> <span><?php echo $model->publisher; ?>&nbsp; &#40;<?php echo $model->published_date; ?>&#41;</span></span></div>
                    <div class="category-detail"><strong><?php echo Yii::t('_yii', 'Category'); ?>:</strong> <span><?php echo $model->category_name; ?></span></div>                                  
                </div>

                <div class="box_content clearfix">
                    <h4 class="box_title"><?php echo Yii::t('_yii', 'Infomation'); ?></h4>
                    <p class="graphy_content">
                        <?php echo $model->description; ?>
                    </p>
                </div>                

            </div>
        </div>

    </div>
</div>
