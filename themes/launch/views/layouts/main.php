<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <?php
        global $language;
        Yii::app()->clientScript->scriptMap = array(
            'jquery.js' => false,
        );
        Yii::app()->getClientScript()->coreScriptPosition = CClientScript::POS_END;
        ?>
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width">        

        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style.css" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <div class="main-launch-site">            
            <div class="header-wrapper-main">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <a class="logo_site" href="<?php echo Yii::app()->request->baseUrl . $language . '/landing/parent'; ?>/">
                                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo.png" alt="" />
                            </a>
                        </div>
                        <div class="col-md-7">
                            <div class="line-info-login">
                                <div class="wrapper-line">
                                    <?php if (!Yii::app()->user->isGuest): ?>
                                        Hi <a href="<?php echo Yii::app()->getModule('user')->profileUrl[0]; ?>"><?php echo Yii::app()->user->name; ?></a>, welcome back, <a href="<?php echo Yii::app()->getModule('user')->logoutUrl[0]; ?>"><?php echo Yii::app()->getModule('user')->t("Logout"); ?></a>
                                    <?php else: ?>
                                        <a href="<?php echo Yii::app()->getModule('user')->loginUrl[0]; ?>" class="user-login-line"><?php echo Yii::app()->getModule('user')->t("Login"); ?></a>|
                                        <a href="<?php echo Yii::app()->getModule('user')->registrationUrl[0]; ?>" class="user-register-line"><?php echo Yii::app()->getModule('user')->t("Register"); ?></a>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <nav class="navbar navbar-default new_nav_bar" role="navigation">
                                <div class="frame_nav">
                                    <!-- Brand and toggle get grouped for better mobile display -->
                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>                                        
                                    </div>

                                    <!-- Collect the nav links, forms, and other content for toggling -->
                                    <div class="collapse navbar-collapse" id="navbar-collapse-1">
                                        <?php
                                        $this->widget('zii.widgets.CMenu', array(
                                            'items' => array(
                                                array('label' => Yii::t('_yii', 'Ask the Expert'), 'url' => array($language . '/experts')),
                                                array('label' => Yii::t('_yii', 'Live Events'), 'url' => array($language . '/events')),
                                                array('label' => Yii::t('_yii', 'Webinars'), 'url' => array($language . '/webinars')),
                                                array('label' => Yii::t('_yii', 'Bookstore'), 'url' => array($language . '/books')),
                                                array('label' => Yii::t('_yii', 'Special Offers'), 'url' => array($language . '/offers')),
                                                array('label' => Yii::t('_yii', 'Video'), 'url' => array($language . '/videos')),
                                            ),
                                            'htmlOptions' => array('class' => 'nav navbar-nav navbar-right navigation_bar'),
                                        ));
                                        ?>
                                    </div>
                                </div>
                            </nav>
                        </div>
                        <div class="col-md-1">
                            <a class="but_pansonic" href="#">
                                <img class="img-responsive" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/panic.png" alt="" />
                            </a>
                        </div>
                    </div>
                </div>
            </div><!--End Header-->

            <?php if (preg_match("/(\/bookstore)/", $_SERVER['REQUEST_URI'])): ?>
                <div class="line-head-book">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="line-head-book-content">
                                    <span>"<?php echo Yii::t('_yii', 'Someone is sitting in the shade today because someone planted a tree a long time ago.'); ?>"</span>&nbsp;<strong>- Warren Buffett</strong>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            
            <div class="main-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <?php echo $content; ?>
                        </div>
                    </div>
                </div>
            </div><!--Main Content Body -->
            
            <div class="footer-wrapper-site" style="margin-top: 50px;">
                <div class="container">
                    <div class="row">
                        <footer class="col-md-12">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="nav_foot">
                                        <ul class="clearfix">
                                            <li><a href="<?php echo Yii::app()->baseUrl; ?>/privacy-policy"><?php echo Yii::t('_yii', 'Privacy Policy'); ?></a></li>
                                            <li><a href="<?php echo Yii::app()->baseUrl; ?>/contact-us"><?php echo Yii::t('_yii', 'Contact Us'); ?></a></li>
                                            <li><a href="<?php echo Yii::app()->baseUrl; ?>/term-of-use"><?php echo Yii::t('_yii', 'Terms of Use'); ?></a></li>
                                            <li><a href="<?php echo Yii::app()->baseUrl; ?>/site-map"><?php echo Yii::t('_yii', 'Site Map'); ?></a></li>
                                        </ul>
                                    </div>
                                    <span class="coppy_right">&copy;&nbsp;<?php echo date('Y'); ?> <?php echo Yii::t('_yii', 'Launch Project, LLC. All rights reserved'); ?></span>
                                </div>
                                <div class="col-md-4"></div>
                            </div>
                        </footer>
                    </div><!-- footer -->
                </div><!-- container -->
            </div><!-- Footer Site -->
        </div><!-- main site-->
        <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/libs/jquery-1.9.1.min.js"><\/script>')</script>
    </body>
</html>