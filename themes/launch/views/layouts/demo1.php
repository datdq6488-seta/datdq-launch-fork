<?php $this->beginContent('//layouts/main'); ?>
<div class="main-content unit-wrapper">
    <div class="span2">
        <div id="sidebar-left">
            <div  id="login-selector" style="float:right; margin:5px;">
                <?php $this->widget('application.widgets.LoginForms');?>
            </div>
        </div><!-- sidebar -->
    </div>
    <div class="span7">
        <?php echo $content; ?>
    </div>
    <div class="span2">
        <div id="sidebar-right">
            This is layout demo sidebar right
        </div><!-- sidebar -->
    </div>
</div>
<?php $this->endContent(); ?>