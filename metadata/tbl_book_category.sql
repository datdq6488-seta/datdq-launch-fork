/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50532
Source Host           : localhost:3306
Source Database       : launch

Target Server Type    : MYSQL
Target Server Version : 50532
File Encoding         : 65001

Date: 2014-08-06 15:05:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_book_category
-- ----------------------------
DROP TABLE IF EXISTS `tbl_book_category`;
CREATE TABLE `tbl_book_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `state` tinyint(4) DEFAULT '0',
  `published` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_book_category
-- ----------------------------
INSERT INTO `tbl_book_category` VALUES ('1', 'Movie', '1', '2014-07-09 17:14:22');
INSERT INTO `tbl_book_category` VALUES ('2', 'Bussiness', '2', '2014-07-09 17:14:40');
INSERT INTO `tbl_book_category` VALUES ('3', 'Entertaiment', '3', '2014-07-09 17:14:47');
