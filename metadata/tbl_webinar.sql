/*
Navicat MySQL Data Transfer

Source Server         : local_wingnix
Source Server Version : 50616
Source Host           : localhost:3306
Source Database       : launch

Target Server Type    : MYSQL
Target Server Version : 50616
File Encoding         : 65001

Date: 2014-08-07 09:41:36
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_webinar
-- ----------------------------
DROP TABLE IF EXISTS `tbl_webinar`;
CREATE TABLE `tbl_webinar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `author` varchar(255) DEFAULT NULL,
  `approved_date` datetime DEFAULT NULL,
  `published` tinyint(1) DEFAULT NULL,
  `type` int(1) DEFAULT '0' COMMENT '0: default, 1: sponsored',
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_webinar
-- ----------------------------
