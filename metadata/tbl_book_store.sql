/*
Navicat MySQL Data Transfer

Source Server         : local_wingnix
Source Server Version : 50616
Source Host           : localhost:3306
Source Database       : launch

Target Server Type    : MYSQL
Target Server Version : 50616
File Encoding         : 65001

Date: 2014-08-06 14:23:26
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_book_store
-- ----------------------------
DROP TABLE IF EXISTS `tbl_book_store`;
CREATE TABLE `tbl_book_store` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text,
  `author` varchar(45) NOT NULL DEFAULT '0',
  `keyword` varchar(255) NOT NULL DEFAULT '',
  `publisher` varchar(45) NOT NULL,
  `published_date` date NOT NULL DEFAULT '0000-00-00',
  `published` tinyint(1) DEFAULT NULL,
  `type` int(1) DEFAULT '0' COMMENT '0: default, 1: sponsored',
  `book_category_id` int(11) NOT NULL,
  `tenant_id` int(11) NOT NULL,
  `state` tinyint(4) NOT NULL DEFAULT '0',
  `link` varchar(255) NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `alias` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_book_store_tbl_book_category_idx` (`book_category_id`) USING BTREE,
  KEY `fk_tbl_book_store_tbl_tenant1_idx` (`tenant_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_book_store
-- ----------------------------
INSERT INTO `tbl_book_store` VALUES ('1', 'Launch! The Critical 90 Days from Idea to Market', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Scort Duffy', '', 'Portfolio Abc', '2014-08-05', '1', '0', '1', '0', '0', '1', '2014-08-05 06:59:09', '2014-08-05 06:59:09', 'cate-3');
INSERT INTO `tbl_book_store` VALUES ('2', 'Another Book spon 1', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Williams Duffy', '', 'Portfolio Hardcover', '2014-08-26', '1', '1', '2', '0', '0', '1', '2014-08-05 06:59:09', '2014-08-05 06:59:09', 'cate-3');
INSERT INTO `tbl_book_store` VALUES ('3', 'Another One', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Williams Robert', '', 'Portfolio Abc', '2014-07-30', '1', '0', '3', '0', '0', '1', '2014-08-05 06:59:09', '2014-08-05 06:59:09', 'cate-3');
INSERT INTO `tbl_book_store` VALUES ('4', 'Gaza: Mapping the human cost', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Scort Duffy', '', 'Portfolio Hardcover', '2014-08-07', '1', '0', '1', '0', '0', '1', '2014-08-05 06:59:09', '2014-08-05 06:59:09', 'cate-3');
INSERT INTO `tbl_book_store` VALUES ('5', 'Another Book spon 2', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Williams Duffy', '', 'Portfolio Hardcover', '2014-08-26', '1', '1', '1', '0', '0', '1', '2014-08-05 06:59:09', '2014-08-05 06:59:09', 'cate-3');
INSERT INTO `tbl_book_store` VALUES ('6', 'Chinese authorities are investigating a ', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Williams Robert', '', 'Portfolio Def', '2014-08-07', '1', '0', '2', '0', '0', '1', '2014-08-05 06:59:09', '2014-08-05 06:59:09', 'cate-3');
INSERT INTO `tbl_book_store` VALUES ('7', 'HP accuses Autonomy founder of fraud', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Scort Duffy', '', 'Portfolio Hardcover', '2014-08-31', '1', '0', '1', '0', '0', '1', '2014-08-05 06:59:09', '2014-08-05 06:59:09', 'cate-3');
INSERT INTO `tbl_book_store` VALUES ('8', 'Another Book', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Williams Duffy', '', 'Portfolio Hardcover', '2014-08-26', '1', '0', '1', '0', '0', '1', '2014-08-05 06:59:09', '2014-08-05 06:59:09', 'cate-3');
INSERT INTO `tbl_book_store` VALUES ('9', 'Another One spon 3', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Williams Duffy', '', 'Portfolio Jqk', '2014-08-26', '1', '1', '2', '0', '0', '1', '2014-08-05 06:59:09', '2014-08-05 06:59:09', 'cate-3');
INSERT INTO `tbl_book_store` VALUES ('10', 'UK company in 2011.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Scort Robert', '', 'Portfolio Hardcover', '2014-08-27', '1', '0', '1', '0', '0', '1', '2014-08-05 06:59:09', '2014-08-05 06:59:09', 'cate-3');
INSERT INTO `tbl_book_store` VALUES ('11', 'Another Book spon 4', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Williams Duffy', '', 'Portfolio Hardcover', '2014-08-02', '1', '1', '1', '0', '0', '1', '2014-08-05 06:59:09', '2014-08-05 06:59:09', 'cate-3');
INSERT INTO `tbl_book_store` VALUES ('12', 'Guardians swoops to top box office', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Williams Robert', '', 'Portfolio Lkh', '2014-08-28', '0', '0', '3', '0', '0', '1', '2014-08-05 06:59:09', '2014-08-05 06:59:09', 'cate-3');
