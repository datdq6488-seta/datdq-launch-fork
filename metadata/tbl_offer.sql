/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50532
Source Host           : localhost:3306
Source Database       : launch

Target Server Type    : MYSQL
Target Server Version : 50532
File Encoding         : 65001

Date: 2014-08-01 14:11:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_offer
-- ----------------------------
DROP TABLE IF EXISTS `tbl_offer`;
CREATE TABLE `tbl_offer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text,
  `promoted` int(1) DEFAULT NULL COMMENT '1: Editor Favorites, 2: Popular',
  `offer_category_id` int(11) DEFAULT NULL,
  `tenant_id` int(11) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `added_user` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `published` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;
