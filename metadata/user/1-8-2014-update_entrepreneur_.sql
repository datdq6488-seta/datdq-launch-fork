﻿# Host: 127.0.0.1  (Version: 5.6.16)
# Date: 2014-08-01 15:22:01
# Generator: MySQL-Front 5.3  (Build 4.136)

/*!40101 SET NAMES utf8 */;

INSERT INTO `tbl_extensions` VALUES (NULL,'com_entrepreneur','component','com_entrepreneur','',1,1,0,0,'{\"name\":\"com_entrepreneur\",\"type\":\"component\",\"creationDate\":\"July 2014\",\"author\":\"Launch! Project\",\"copyright\":\"(C) 2005 - 2014 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"0.0.1\",\"description\":\"COM_TEMPLATE_DESIGNS_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0);
INSERT INTO `tbl_extensions` VALUES (NULL,'com_provider','component','com_provider','',1,1,0,0,'{\"name\":\"com_provider\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2014 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_TEMPLATE_DESIGNS_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0);
INSERT INTO `tbl_extensions` VALUES (NULL,'com_partner','component','com_partner','',1,1,0,0,'{\"name\":\"com_partner\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2014 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_TEMPLATE_DESIGNS_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0);

#
# Structure for table "tbl_usergroups"
#

DROP TABLE IF EXISTS `tbl_usergroups`;
CREATE TABLE `tbl_usergroups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Adjacency List Reference Id',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `title` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_usergroup_parent_title_lookup` (`parent_id`,`title`),
  KEY `idx_usergroup_title_lookup` (`title`),
  KEY `idx_usergroup_adjacency_lookup` (`parent_id`),
  KEY `idx_usergroup_nested_set_lookup` (`lft`,`rgt`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

#
# Data for table "tbl_usergroups"
#

INSERT INTO `tbl_usergroups` VALUES (1,0,1,18,'Public'),(2,1,8,15,'Registered'),(3,2,9,14,'Author'),(4,3,10,13,'Editor'),(5,4,11,12,'Publisher'),(6,1,4,7,'Manager'),(7,6,5,6,'Administrator'),(8,1,16,17,'Super Users'),(9,1,2,3,'Guest'),(10,1,1,2,'Entrepreneur'),(11,1,2,3,'Content Provider'),(12,1,3,4,'Partner');

#
# Structure for table "tbl_users"
#

DROP TABLE IF EXISTS `tbl_users`;
CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `username` varchar(150) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `block` tinyint(4) NOT NULL DEFAULT '0',
  `sendEmail` tinyint(4) DEFAULT '0',
  `registerDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastvisitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activation` varchar(100) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  `lastResetTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Date of last password reset',
  `resetCount` int(11) NOT NULL DEFAULT '0' COMMENT 'Count of password resets since lastResetTime',
  `otpKey` varchar(1000) NOT NULL DEFAULT '' COMMENT 'Two factor authentication encrypted keys',
  `otep` varchar(1000) NOT NULL DEFAULT '' COMMENT 'One time emergency passwords',
  `requireReset` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Require user to reset password on next login',
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_name` (`name`),
  KEY `idx_block` (`block`),
  KEY `username` (`username`),
  KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=utf8;

#
# Data for table "tbl_users"
#

INSERT INTO `tbl_users` VALUES (122,'Super User','launch','launch@gmail.com','$2y$10$fL/jRK/hGfD1F55Qeq5xwugxpwJgUextzQcWDeFVa18DKXX6ytczm',0,1,'2014-06-17 08:00:07','2014-08-01 06:58:31','0','','0000-00-00 00:00:00',0,'','',0,0,1,1),(123,'ENTRE 2 3','df','launc3h2@gmail.com','$2y$10$fL/jRK/hGfD1F55Qeq5xwugxpwJgUextzQcWDeFVa18DKXX6ytczm',0,1,'2014-08-01 03:37:25','0000-00-00 00:00:00','','a:4:{s:12:\"company_name\";s:9:\"ENTRE 2 3\";s:12:\"company_type\";s:9:\"ENTRE 2 3\";s:3:\"bio\";s:9:\"ENTRE 2 3\";s:7:\"private\";i:1;}','0000-00-00 00:00:00',0,'','',0,0,1,1),(124,'ENTRE 34','','','',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','','a:4:{s:12:\"company_name\";s:8:\"ENTRE 34\";s:12:\"company_type\";s:8:\"ENTRE 34\";s:3:\"bio\";s:8:\"ENTRE 34\";s:7:\"private\";i:0;}','0000-00-00 00:00:00',0,'','',0,0,1,-2),(125,'dgsg','','','',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','','a:4:{s:12:\"company_name\";s:9:\"gdsfgdsfg\";s:12:\"company_type\";s:4:\"dsfg\";s:3:\"bio\";s:7:\"dsfgsdg\";s:7:\"private\";i:1;}','0000-00-00 00:00:00',0,'','',0,0,1,1);
