﻿# Host: 127.0.0.1  (Version: 5.6.16)
# Date: 2014-08-07 11:08:15
# Generator: MySQL-Front 5.3  (Build 4.136)

/*!40101 SET NAMES utf8 */;

#
# Structure for table "tbl_provider_category"
#

DROP TABLE IF EXISTS `tbl_provider_category`;
CREATE TABLE `tbl_provider_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `published` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

#
# Data for table "tbl_provider_category"
#

INSERT INTO `tbl_provider_category` VALUES (4,'test',-2,'2014-08-06 08:07:47'),(5,'test 2 4',1,'2014-08-06 08:11:03'),(6,'test 4',-2,'2014-08-06 08:11:07'),(7,'ttt 6',-2,'2014-08-06 09:42:43');
