﻿# Host: 127.0.0.1  (Version: 5.6.16)
# Date: 2014-08-11 10:31:46
# Generator: MySQL-Front 5.3  (Build 4.136)

/*!40101 SET NAMES utf8 */;

#
# Structure for table "tbl_entrepreneur_category"
#

DROP TABLE IF EXISTS `tbl_entrepreneur_category`;
CREATE TABLE `tbl_entrepreneur_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `published` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

#
# Data for table "tbl_entrepreneur_category"
#

INSERT INTO `tbl_entrepreneur_category` VALUES (4,'test',-2,'2014-08-06 08:07:47'),(5,'test 2 4',-2,'2014-08-06 08:11:03'),(6,'test 4',-2,'2014-08-06 08:11:07'),(7,'ttt 6',-2,'2014-08-06 09:42:43'),(8,'ed gdg ',1,'2014-08-07 08:26:06'),(9,'456456546',-2,'2014-08-07 08:26:44'),(10,'2354 35 2345',1,'2014-08-07 09:07:31'),(11,' 245 234 523 5',1,'2014-08-07 09:07:36');
